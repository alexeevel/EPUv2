/*
 * stlm75.cpp
 *
 *  Created on: 8 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#include "stlm75.hpp"
#include "debug/debug.hpp"
using namespace debug;

bool STLM75::test() {
	uint8_t i2c_data[2] = {0};
	uint8_t i2c_tx_data[] = {0};
	HAL_I2C_Master_Receive(i2c, ADDR, i2c_data, 2, 1000);
	HAL_I2C_Master_Transmit(i2c, ADDR, i2c_tx_data, 1, 1000);
	//t_addr|=0b100000000;
	HAL_I2C_Master_Receive(i2c, ADDR, i2c_data, 2, 1000);
	//i2c_data[0] = 0;
	HAL_I2C_Mem_Read(i2c, ADDR, 0, 1, i2c_data, 2, 1000);
	int16_t temp16 = ((uint16_t)i2c_data[0]<<8) | (i2c_data[1]);
	float temp = (float)temp16/256;
	if ((temp>100.0)||(temp<-45.0)) {
		DBG::PRINT(LVL::EMCY, "STLM75", "", "TEST FAIL", "temp=%f", temp);
		return false;
	} else {
		DBG::PRINT(LVL::INFO, "STLM75", "", "TEST OK");
		return true;
	}

}

