/*
 * stlm75.h
 *
 *  Created on: 8 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef THERMO_STLM75_HPP_
#define THERMO_STLM75_HPP_

#include "hwtesting/hwtesting.hpp"
#include "stm32l4xx_hal.h"
#include "bsp/bsp.hpp"

class STLM75: public AbstractHWTest
{
	static constexpr uint16_t ADDR = 0b10010001;
	I2C_HandleTypeDef* i2c;
	gpio_out_pin power_pin;
public:
	STLM75(I2C_HandleTypeDef* i2c, gpio_out_pin &&pin):i2c(i2c), power_pin(std::move(pin))
{
		power_pin.set();
}
	bool test() override;
};



#endif /* THERMO_STLM75_HPP_ */
