//Reenterable function, based on switch statement
#include <type_traits>
extern int DO_NOT_WRITE_CODE_BETWEEN_STATES[];

#define STATEFUL_BEGIN_STATES enum {
#define STATEFUL_END_STATES  };
#define STATEFUL_DEFINE_STATE( THE_STATE_NAME ) THE_STATE_NAME=__LINE__,

#define STATEFUL_STATE_START 0
#define STATEFUL_STATE_END INT_MIN
#define STATEFUL_START(state_pointer)\
    auto &current_state = state_pointer;\
    switch(current_state){ case static_cast<std::remove_reference<decltype(current_state)>::type>(STATEFUL_STATE_START):

#define STATEFUL_STATE(THE_STATE) 0]=0; case THE_STATE:

#define STATEFUL_MOVETO( THE_STATE_NAME, THE_VAL ) \
    {current_state = THE_STATE_NAME; return THE_VAL;}



#define STATEFUL_END_STATE DO_NOT_WRITE_CODE_BETWEEN_STATES[
#define STATEFUL_END 0]=0; default: ; } current_state=static_cast<std::remove_reference<decltype(current_state)>::type>(STATEFUL_STATE_START);

#define STATEFUL_CYCLIC_START(condition) \
    while(condition) {

#define STATEFUL_CYCLIC_NEXT(THE_VAL) return THE_VAL;
#define STATEFUL_CYCLIC_END(CLEAR_COND)  } CLEAR_COND;
