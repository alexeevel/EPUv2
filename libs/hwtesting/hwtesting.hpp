/*
 * hwtesting.h
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef HWTESTING_HWTESTING_H_
#define HWTESTING_HWTESTING_H_


 class AbstractHWTest
 {
	 public:
	 virtual bool test()=0;
	 virtual ~AbstractHWTest()=default;

 };


#endif /* HWTESTING_HWTESTING_H_ */
