/*
 * gnss.hpp
 *
 *  Created on: 9 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef GNSS_L76_HPP_
#define GNSS_L76_HPP_
#include "hwtesting/hwtesting.hpp"
#include "hal/gpio.hpp"
#include "stm32l4xx_hal.h"
#include "vector"
#include "cmsis_os2.h"
#include "FreeRTOS.h"
#include "message_buffer.h"
#include <functional>
#include "isr_vector.h"
#include "cmsis_os2.h"
#include "containers/double_buffer.hpp"
#include "NMEAParser/NMEAParser.h"

class L76 : public AbstractHWTest
{
	UART_HandleTypeDef * uart;
	gpio_out_pin force_on, reset, stndby;
	containers::DoubleBuffer rx_buff;
	osThreadId_t thread;
	osSemaphoreId_t sem;
	MessageBufferHandle_t msg_buf;
	NMEAParser parser;

	static constexpr size_t BUFF_SIZE=64;
	static constexpr size_t STACK_SIZE=512;
	static constexpr size_t WAIT_FIRST_SENTENCE_S=3;
	static void ThreadFunc(void* );

public:
	L76(UART_HandleTypeDef * uart, gpio_out_pin&& on, gpio_out_pin&& rst, gpio_out_pin&& stnd);
	void interrupt_handler();
	void rx_cplt_callback(UART_HandleTypeDef *);
	void error_callback(UART_HandleTypeDef *);
	bool test()override;
	UART_HandleTypeDef * get_uart(){return uart;}
	void start();
	void stop();
};

#endif /* GNSS_L76_HPP_ */
