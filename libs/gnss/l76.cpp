/*
 * m66.cpp
 *
 *  Created on: Jun 12, 2021
 *      Author: ironcaterpillar
 */

#include <gnss/l76.hpp>
#include <functional>
#include "usart.h"
#include <cstring>
#include "debug/debug.hpp"
#include "message_buffer.h"
#include "containers/double_buffer.hpp"
using namespace debug;

bool L76::test() {
	size_t sec_passed=0;
	while(parser.GetActualGPSInfo().m_nSentences==0&&sec_passed<WAIT_FIRST_SENTENCE_S)
	{
		osDelay(1000);
		sec_passed++;
	}
	if(parser.GetActualGPSInfo().m_nSentences)
	{
		DBG::PRINT(LVL::INFO, "L76 GNSS", "" , "TEST OK");
		return true;
	}
	else
	{
		DBG::PRINT(LVL::EMCY, "L76 GNSS", "" , "TEST FAILED ", "No sentences received");
		return false;
	}
}
void L76::ThreadFunc(void *arg) {
	L76 *instance = reinterpret_cast<L76*>(arg);
	while(1)
	{
		osStatus_t result = osSemaphoreAcquire(instance->sem, osWaitForever);
		if(result==osOK)
		{
			//DBG::PRINT(LVL::INFO, "GNSS", " DATA IB", (char*) &(*instance->rx_buff.spare_buffer().begin()));
			instance->parser.Parse((char*)&(*instance->rx_buff.spare_buffer().begin()), instance->rx_buff.get_data_size());


		}

	}
}
L76::L76(UART_HandleTypeDef *uart, gpio_out_pin &&on, gpio_out_pin &&rst,
		gpio_out_pin &&stnd) :
		uart(uart), force_on(std::move(on)), stndby(std::move(stnd)),rx_buff(BUFF_SIZE) {
	HAL_UART_Abort(uart);



	osThreadAttr_t attr = { .stack_size = STACK_SIZE };
	sem = osSemaphoreNew(1,0,nullptr);
	osSemaphoreAcquire(sem,0);
	thread = osThreadNew(L76::ThreadFunc, this, &attr);


}

void L76::start() {
	HAL_UART_Receive_IT(uart, &(*rx_buff.data_iterator()), 1);
	force_on.set();
	reset.reset();
	stndby.reset();
}

void L76::stop() {
	HAL_UART_Abort(uart);
	force_on.reset();
	reset.set();
	stndby.set();
}

void L76::interrupt_handler() {
	HAL_UART_IRQHandler(uart);
}

void L76::rx_cplt_callback(UART_HandleTypeDef *uart) {
	if ((*rx_buff.data_iterator()) == '\n'
			|| rx_buff.data_iterator()==rx_buff.end()) {
		*(rx_buff.data_iterator()++) = 0;
		rx_buff.swap();
		HAL_UART_Receive_IT(uart, &(*rx_buff.data_iterator()), 1);
		osSemaphoreRelease(sem);



	} else
	{
		HAL_UART_Receive_IT(uart, &(*(++rx_buff.data_iterator())), 1);
	}


}

void L76::error_callback(UART_HandleTypeDef*) {
	HAL_UART_Receive_IT(uart, &(*rx_buff.data_iterator()), 1);

}
