/*
 * usbh_skzi.c
 *
 *  Created on: May 2, 2021
 *      Author: abutko
 */

#include "usbh_skzi.h"

static USBH_StatusTypeDef USBH_SKZI_InterfaceInit(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_SKZI_InterfaceDeInit(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_SKZI_Process(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_SKZI_ClassRequest(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_SKZI_SOFProcess(USBH_HandleTypeDef *phost);
//static USBH_StatusTypeDef USBH_SKZI_Events(USBH_HandleTypeDef *phost);

USBH_ClassTypeDef  SKZI_Class =
{
  "SKZI",
  USB_SKZI_CLASS,
  USBH_SKZI_InterfaceInit,
  USBH_SKZI_InterfaceDeInit,
  USBH_SKZI_ClassRequest,
  USBH_SKZI_Process,
  USBH_SKZI_SOFProcess,
  NULL,
};

static USBH_StatusTypeDef USBH_SKZI_InterfaceInit(USBH_HandleTypeDef *phost)
{
	USBH_StatusTypeDef status;
	uint8_t interface;
	SKZI_HandleTypeDef *SKZI_Handle;

	interface = USBH_FindInterface(phost, USB_SKZI_CLASS, 0x77, 0x01);

	if ((interface == 0xFFU) || (interface >= USBH_MAX_NUM_INTERFACES)) /* No Valid Interface */
	{
	   USBH_DbgLog("Cannot Find the interface for Communication Interface Class.", phost->pActiveClass->Name);
	   return USBH_FAIL;
	}

	status = USBH_SelectInterface(phost, interface);

	if (status != USBH_OK)
	{
	  return USBH_FAIL;
	}

	phost->pActiveClass->pData = (SKZI_HandleTypeDef *)USBH_malloc(sizeof(SKZI_HandleTypeDef));
	SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

	if (SKZI_Handle == NULL)
	{
	  USBH_DbgLog("Cannot allocate memory for CDC Handle");
	  return USBH_FAIL;
	}

	/* Initialize cdc handler */
	USBH_memset(SKZI_Handle, 0, sizeof(SKZI_HandleTypeDef));

	/*Collect the notification endpoint address and length*/
#if 0
	if (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].bEndpointAddress & 0x80U)
	{
	  SKZI_Handle->CommItf.NotifEp = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].bEndpointAddress;
	  SKZI_Handle->CommItf.NotifEpSize  = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].wMaxPacketSize;
	}

	/*Allocate the length for host channel number in*/
	SKZI_Handle->CommItf.NotifPipe = USBH_AllocPipe(phost, SKZI_Handle->CommItf.NotifEp);

	/* Open pipe for Notification endpoint */
	USBH_OpenPipe(phost, SKZI_Handle->CommItf.NotifPipe, SKZI_Handle->CommItf.NotifEp,
	              phost->device.address, phost->device.speed, USB_EP_TYPE_INTR,
	              SKZI_Handle->CommItf.NotifEpSize);

	USBH_LL_SetToggle(phost, SKZI_Handle->CommItf.NotifPipe, 0U);

	interface = USBH_FindInterface(phost, SKZI_DATA_INTERFACE_CLASS_CODE,
			SKZI_RESERVED, SKZI_NO_CLASS_SPECIFIC_PROTOCOL_CODE);

	if ((interface == 0xFFU) || (interface >= USBH_MAX_NUM_INTERFACES)) /* No Valid Interface */
	{
	  USBH_DbgLog("Cannot Find the interface for Data Interface Class.", phost->pActiveClass->Name);
	  return USBH_FAIL;
	}
#endif
	/*Collect the class specific endpoint address and length*/
	if (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].bEndpointAddress & 0x80U)
	{
	  SKZI_Handle->DataItf.InEp = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].bEndpointAddress;
	  SKZI_Handle->DataItf.InEpSize  = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].wMaxPacketSize;
	}
	else
	{
	  SKZI_Handle->DataItf.OutEp = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].bEndpointAddress;
	  SKZI_Handle->DataItf.OutEpSize  = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[0].wMaxPacketSize;
	}

	if (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[1].bEndpointAddress & 0x80U)
	{
	  SKZI_Handle->DataItf.InEp = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[1].bEndpointAddress;
	  SKZI_Handle->DataItf.InEpSize  = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[1].wMaxPacketSize;
	}
	else
	{
	  SKZI_Handle->DataItf.OutEp = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[1].bEndpointAddress;
	  SKZI_Handle->DataItf.OutEpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[1].wMaxPacketSize;
	}

	/*Allocate the length for host channel number out*/
	SKZI_Handle->DataItf.OutPipe = USBH_AllocPipe(phost, SKZI_Handle->DataItf.OutEp);

	/*Allocate the length for host channel number in*/
	SKZI_Handle->DataItf.InPipe = USBH_AllocPipe(phost, SKZI_Handle->DataItf.InEp);

	/* Open channel for OUT endpoint */
	USBH_OpenPipe(phost, SKZI_Handle->DataItf.OutPipe, SKZI_Handle->DataItf.OutEp,
	              phost->device.address, phost->device.speed, USB_EP_TYPE_BULK,
	              SKZI_Handle->DataItf.OutEpSize);

	/* Open channel for IN endpoint */
	USBH_OpenPipe(phost, SKZI_Handle->DataItf.InPipe, SKZI_Handle->DataItf.InEp,
	              phost->device.address, phost->device.speed, USB_EP_TYPE_BULK,
	              SKZI_Handle->DataItf.InEpSize);

	SKZI_Handle->state = SKZI_IDLE_STATE;

	USBH_LL_SetToggle(phost, SKZI_Handle->DataItf.OutPipe, 0U);
	USBH_LL_SetToggle(phost, SKZI_Handle->DataItf.InPipe, 0U);

	return USBH_OK;
}

static USBH_StatusTypeDef USBH_SKZI_InterfaceDeInit(USBH_HandleTypeDef *phost)
{
	  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

	  if (SKZI_Handle->CommItf.NotifPipe)
	  {
	    USBH_ClosePipe(phost, SKZI_Handle->CommItf.NotifPipe);
	    USBH_FreePipe(phost, SKZI_Handle->CommItf.NotifPipe);
	    SKZI_Handle->CommItf.NotifPipe = 0U;     /* Reset the Channel as Free */
	  }

	  if (SKZI_Handle->DataItf.InPipe)
	  {
	    USBH_ClosePipe(phost, SKZI_Handle->DataItf.InPipe);
	    USBH_FreePipe(phost, SKZI_Handle->DataItf.InPipe);
	    SKZI_Handle->DataItf.InPipe = 0U;     /* Reset the Channel as Free */
	  }

	  if (SKZI_Handle->DataItf.OutPipe)
	  {
	    USBH_ClosePipe(phost, SKZI_Handle->DataItf.OutPipe);
	    USBH_FreePipe(phost, SKZI_Handle->DataItf.OutPipe);
	    SKZI_Handle->DataItf.OutPipe = 0U;    /* Reset the Channel as Free */
	  }

	  if (phost->pActiveClass->pData)
	  {
	    USBH_free(phost->pActiveClass->pData);
	    phost->pActiveClass->pData = 0U;
	  }

	  return USBH_OK;
}

/**
  * @brief  This request allows the host to specify typical asynchronous
  * line-character formatting properties
  * This request applies to asynchronous byte stream data class interfaces
  * and endpoints
  * @param  pdev: Selected device
  * @retval USBH_StatusTypeDef : USB ctl xfer status
  */
static USBH_StatusTypeDef SetLineCoding(USBH_HandleTypeDef *phost,
                                        SKZI_LineCodingTypeDef *linecoding)
{
	 phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_CLASS |
                                         USB_REQ_RECIPIENT_INTERFACE;

	 phost->Control.setup.b.bRequest = SKZI_SET_LINE_CODING;
	 phost->Control.setup.b.wValue.w = 0U;

	 phost->Control.setup.b.wIndex.w = 0U;

	 phost->Control.setup.b.wLength.w = SKZI_LINE_CODING_STRUCTURE_SIZE;

	 return USBH_CtlReq(phost, linecoding->Array, SKZI_LINE_CODING_STRUCTURE_SIZE);
}

/**
  * @brief  This request allows the host to find out the currently
  *         configured line coding.
  * @param  pdev: Selected device
  * @retval USBH_StatusTypeDef : USB ctl xfer status
  */
static USBH_StatusTypeDef GetLineCoding(USBH_HandleTypeDef *phost, SKZI_LineCodingTypeDef *linecoding)
{
	phost->Control.setup.b.bmRequestType = USB_D2H | USB_REQ_TYPE_CLASS | \
                                         USB_REQ_RECIPIENT_INTERFACE;

	phost->Control.setup.b.bRequest = SKZI_GET_LINE_CODING;
	phost->Control.setup.b.wValue.w = 0U;
	phost->Control.setup.b.wIndex.w = 0U;
	phost->Control.setup.b.wLength.w = SKZI_LINE_CODING_STRUCTURE_SIZE;

	return USBH_CtlReq(phost, linecoding->Array, SKZI_LINE_CODING_STRUCTURE_SIZE);
}

/**
* @brief  The function is responsible for sending data to the device
*  @param  pdev: Selected device
* @retval None
*/
static void SKZI_ProcessTransmission(USBH_HandleTypeDef *phost)
{
#if 1
  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;

  switch (SKZI_Handle->data_tx_state)
  {
    case SKZI_SEND_DATA:
      if (SKZI_Handle->TxDataLength > SKZI_Handle->DataItf.OutEpSize)
      {
        USBH_BulkSendData(phost,
                          SKZI_Handle->pTxData,
                          SKZI_Handle->DataItf.OutEpSize,
                          SKZI_Handle->DataItf.OutPipe,
                          1U);
      }
      else
      {
        USBH_BulkSendData(phost,
                          SKZI_Handle->pTxData,
                          (uint16_t)SKZI_Handle->TxDataLength,
                          SKZI_Handle->DataItf.OutPipe,
                          1U);
      }

      SKZI_Handle->data_tx_state = SKZI_SEND_DATA_WAIT;
      break;

    case SKZI_SEND_DATA_WAIT:

      URB_Status = USBH_LL_GetURBState(phost, SKZI_Handle->DataItf.OutPipe);

      /* Check the status done for transmission */
      if (URB_Status == USBH_URB_DONE)
      {
        if (SKZI_Handle->TxDataLength > SKZI_Handle->DataItf.OutEpSize)
        {
          SKZI_Handle->TxDataLength -= SKZI_Handle->DataItf.OutEpSize;
          SKZI_Handle->pTxData += SKZI_Handle->DataItf.OutEpSize;
        }
        else
        {
          SKZI_Handle->TxDataLength = 0U;
        }

        if (SKZI_Handle->TxDataLength > 0U)
        {
          SKZI_Handle->data_tx_state = SKZI_SEND_DATA;
        }
        else
        {
          SKZI_Handle->data_tx_state = SKZI_IDLE;
          USBH_SKZI_TransmitCallback(phost);
        }

#if (USBH_USE_OS == 1U)
        phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
        (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
        (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
      }
      else
      {
        if (URB_Status == USBH_URB_NOTREADY)
        {
          SKZI_Handle->data_tx_state = SKZI_SEND_DATA;

#if (USBH_USE_OS == 1U)
          phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
          (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
          (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
        }
      }
      break;

    default:
      break;
  }
#endif
}

/**
* @brief  This function responsible for reception of data from the device
*  @param  pdev: Selected device
* @retval None
*/

static void SKZI_ProcessReception(USBH_HandleTypeDef *phost)
{
  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
  uint32_t length;

  switch (SKZI_Handle->data_rx_state)
  {

    case SKZI_RECEIVE_DATA:
    {
      volatile USBH_StatusTypeDef retuhval = USBH_BulkReceiveData(phost,
                           SKZI_Handle->pRxData,
                           SKZI_Handle->DataItf.InEpSize,
                           SKZI_Handle->DataItf.InPipe);
      if (retuhval == USBH_OK)
    	  SKZI_Handle->data_rx_state = SKZI_RECEIVE_DATA_WAIT;
      else
          SKZI_Handle->data_rx_state = SKZI_IDLE;
    }
      break;

    case SKZI_RECEIVE_DATA_WAIT:

      URB_Status = USBH_LL_GetURBState(phost, SKZI_Handle->DataItf.InPipe);

      /*Check the status done for reception*/
      if (URB_Status == USBH_URB_DONE)
      {
        length = USBH_LL_GetLastXferSize(phost, SKZI_Handle->DataItf.InPipe);

        if (((SKZI_Handle->RxDataLength - length) > 0U) && (length > SKZI_Handle->DataItf.InEpSize))
        {
          SKZI_Handle->RxDataLength -= length ;
          SKZI_Handle->pRxData += length;
          SKZI_Handle->data_rx_state = SKZI_RECEIVE_DATA;
        }
        else
        {
          SKZI_Handle->RxDataLength -= length;
          SKZI_Handle->data_rx_state = SKZI_IDLE;
          USBH_SKZI_ReceiveCallback(phost);
        }

#if (USBH_USE_OS == 1U)
        phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
        (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
        (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
      }
      /*
      else
          if (URB_Status == USBH_URB_NOTREADY)
          {
              SKZI_Handle->data_rx_state = SKZI_RECEIVE_DATA;
#if (USBH_USE_OS == 1U)
        phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
        (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
        (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
          }
          */
      break;

    default:
      break;
  }
}

static USBH_StatusTypeDef USBH_SKZI_Process(USBH_HandleTypeDef *phost)
{
	USBH_StatusTypeDef status = USBH_BUSY;
	USBH_StatusTypeDef req_status = USBH_OK;
	SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

	switch (SKZI_Handle->state)
	{
    case SKZI_IDLE_STATE:
    	status = USBH_OK;
    	break;


    case SKZI_SET_LINE_CODING_STATE:
    	req_status = SetLineCoding(phost, SKZI_Handle->pUserLineCoding);

    	if (req_status == USBH_OK)
    	{
    		SKZI_Handle->state = SKZI_GET_LAST_LINE_CODING_STATE;
    	}
    	else
    	{
    		if (req_status != USBH_BUSY)
    		{
    			SKZI_Handle->state = SKZI_ERROR_STATE;
    		}
    	}
    	break;

    case SKZI_GET_LAST_LINE_CODING_STATE:
    	req_status = GetLineCoding(phost, &(SKZI_Handle->LineCoding));

    	if (req_status == USBH_OK)
    	{
    		SKZI_Handle->state = SKZI_IDLE_STATE;

    		if ((SKZI_Handle->LineCoding.b.bCharFormat == SKZI_Handle->pUserLineCoding->b.bCharFormat) &&
    				(SKZI_Handle->LineCoding.b.bDataBits == SKZI_Handle->pUserLineCoding->b.bDataBits) &&
					(SKZI_Handle->LineCoding.b.bParityType == SKZI_Handle->pUserLineCoding->b.bParityType) &&
					(SKZI_Handle->LineCoding.b.dwDTERate == SKZI_Handle->pUserLineCoding->b.dwDTERate))
	        	{
    				USBH_SKZI_LineCodingChanged(phost);
	        	}
    	}
    	else
    	{
    		if (req_status != USBH_BUSY)
    		{
    			SKZI_Handle->state = SKZI_ERROR_STATE;
    		}
    	}
    	break;

    case SKZI_TRANSFER_DATA:
    	SKZI_ProcessTransmission(phost);
    	SKZI_ProcessReception(phost);
    	break;

    case SKZI_ERROR_STATE:
    	req_status = USBH_ClrFeature(phost, 0x00U);
    	if (req_status == USBH_OK)
    	{
    		/*Change the state to waiting*/
    		SKZI_Handle->state = SKZI_IDLE_STATE;
    	}
    	break;

    default:
    	break;

	}
	return status;
}

USBH_StatusTypeDef  USBH_SKZI_Transmit(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

  if ((SKZI_Handle->state == SKZI_IDLE_STATE) || (SKZI_Handle->state == SKZI_TRANSFER_DATA))
  {
    SKZI_Handle->pTxData = pbuff;
    SKZI_Handle->TxDataLength = length;
    SKZI_Handle->state = SKZI_TRANSFER_DATA;
    SKZI_Handle->data_tx_state = SKZI_SEND_DATA;
    Status = USBH_OK;

#if (USBH_USE_OS == 1U)
    phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
    (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
    (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
  }
  return Status;
}

USBH_StatusTypeDef  USBH_SKZI_Receive(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

  if ((SKZI_Handle->state == SKZI_IDLE_STATE) || (SKZI_Handle->state == SKZI_TRANSFER_DATA))
  {
    SKZI_Handle->pRxData = pbuff;
    SKZI_Handle->RxDataLength = length;
    SKZI_Handle->state = SKZI_TRANSFER_DATA;
    SKZI_Handle->data_rx_state = SKZI_RECEIVE_DATA;
    Status = USBH_OK;

#if (USBH_USE_OS == 1U)
    phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
    (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
    (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
  }
  return Status;
}

static USBH_StatusTypeDef USBH_SKZI_ClassRequest(USBH_HandleTypeDef *phost)
{
#if 0
	  USBH_StatusTypeDef status;
	  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

	  /* Issue the get line coding request */
	  status = GetLineCoding(phost, &SKZI_Handle->LineCoding);
	  if (status == USBH_OK)
	  {
	    phost->pUser(phost, HOST_USER_CLASS_ACTIVE);
	  }
	  else if (status == USBH_NOT_SUPPORTED)
	  {
	    USBH_ErrLog("Control error: SKZI: Device Get Line Coding configuration failed");
	  }
	  else
	  {
	    /* .. */
	  }

	  return status;
#else
	return USBH_OK;
#endif
}

static USBH_StatusTypeDef USBH_SKZI_SOFProcess(USBH_HandleTypeDef *phost)
{
	return USBH_OK;
}
/*
static USBH_StatusTypeDef USBH_SKZI_Events(USBH_HandleTypeDef *phost)
{
	return USBH_OK;
}
*/
/**
  * @brief  USBH_SKZI_Stop
  *         Stop current SKZI Transmission
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef  USBH_SKZI_Stop(USBH_HandleTypeDef *phost)
{
  SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) phost->pActiveClass->pData;

  if (phost->gState == HOST_CLASS)
  {
    SKZI_Handle->state = SKZI_IDLE_STATE;

    USBH_ClosePipe(phost, SKZI_Handle->CommItf.NotifPipe);
    USBH_ClosePipe(phost, SKZI_Handle->DataItf.InPipe);
    USBH_ClosePipe(phost, SKZI_Handle->DataItf.OutPipe);
  }
  return USBH_OK;
}

/**
* @brief  The function informs user that data have been received
*  @param  pdev: Selected device
* @retval None
*/
__weak void USBH_SKZI_TransmitCallback(USBH_HandleTypeDef *phost)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(phost);
}

/**
* @brief  The function informs user that data have been sent
*  @param  pdev: Selected device
* @retval None
*/
__weak void USBH_SKZI_ReceiveCallback(USBH_HandleTypeDef *phost)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(phost);
}

/**
* @brief  The function informs user that Settings have been changed
*  @param  pdev: Selected device
* @retval None
*/
__weak void USBH_SKZI_LineCodingChanged(USBH_HandleTypeDef *phost)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(phost);
}

