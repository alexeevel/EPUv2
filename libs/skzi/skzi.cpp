/*
 * skzi.cpp
 *
 *  Created on: 14 июн. 2021 г.
 *      Author: abutko
 */

#include "skzi/skzi.hpp"
#include "main.h"
#include "string.h"
#include "main.h"
#include "cmsis_os2.h"
#include "debug/debug.hpp"
#include "gps.h"

using namespace debug;

PLUMB_STATE plState;

uint8_t recvBufUartSKZI[RX_BUF_SKZI_SIZE];
uint8_t *skzi_RX_buf = (uint8_t *)&recvBufUartSKZI;
int skzi_RX_buf_cnt = 0;
char packet_buf[SC_PROTO_MAX_PACKET_SIZE];
uint8_t staticMasterCtxBuffer[STATIC_MASTER_CTX_BUF_SKZI_SIZE];
char skzi_EOT = 0;
int codeUSB = 0;
char tls_server_buf[4 * 1024];
int tls_server_max_size = 0;

void USBH_SKZI_TransmitCallback(USBH_HandleTypeDef *phost)
{
	codeUSB = 1;
}

void USBH_SKZI_ReceiveCallback(USBH_HandleTypeDef *phost)
{
	codeUSB = 2;
}

uint32_t stopwatch_getticks()
{
	return 0;
}

const char *sc_proto_cmd_name(int cmd)
{
	switch (cmd)
	{
	case SCM_CMD_CONTINUE:
		return "SCM_CMD_CONTINUE";
	case SCM_CMD_ABORT:
		return "SCM_CMD_ABORT";
	case SCM_CMD_PING:
		return "SCM_CMD_PING";
	case SCM_CMD_MODULE_ID:
		return "SCM_CMD_MODULE_ID";
	case SCM_CMD_MODULE_STATUS:
		return "SCM_CMD_MODULE_STATUS";
	case SCM_CMD_MODULE_GET_TIME:
		return "SCM_CMD_MODULE_GET_TIME";
	case SCM_CMD_MASTER_ID:
		return "SCM_CMD_MASTER_ID";
	case SCM_CMD_GET_ALG_LIST:
		return "SCM_CMD_GET_ALG_LIST";
	case SCM_CMD_GET_KEY_INFO:
		return "SCM_CMD_GET_KEY_INFO";
	case SCM_CMD_GET_CRL: //not implemented
		return "SCM_CMD_GET_CRL";
	case SCM_CMD_CERT_CHECK: //not implemented
		return "SCM_CMD_CERT_CHECK";
	case SCM_CMD_CRYPT_INIT:
		return "SCM_CMD_CRYPT_INIT";
	case SCM_CMD_CRYPT_UPDATE:
		return "SCM_CMD_CRYPT_UPDATE";
	case SCM_CMD_CRYPT_FINAL:
		return "SCM_CMD_CRYPT_FINAL";
	case SCM_CMD_HASH_INIT:
		return "SCM_CMD_HASH_INIT";
	case SCM_CMD_HASH_UPDATE:
		return "SCM_CMD_HASH_UPDATE";
	case SCM_CMD_HASH_FINAL:
		return "SCM_CMD_HASH_FINAL";
	case SCM_CMD_SIGN_INIT:
		return "SCM_CMD_SIGN_INIT";
	case SCM_CMD_SIGN_UPDATE:
		return "SCM_CMD_SIGN_UPDATE";
	case SCM_CMD_SIGN_FINAL:
		return "SCM_CMD_SIGN_FINAL";
	case SCM_CMD_VERIFY_INIT:
		return "SCM_CMD_VERIFY_INIT";
	case SCM_CMD_VERIFY_UPDATE:
		return "SCM_CMD_VERIFY_UPDATE";
	case SCM_CMD_VERIFY_FINAL:
		return "SCM_CMD_VERIFY_FINAL";
	case SCM_CMD_TLS_INIT:
		return "SCM_CMD_TLS_INIT";
	case SCM_CMD_TLS_SET_CERTS: //not used
		return "SCM_CMD_TLS_SET_CERTS";
	case SCM_CMD_TLS_SETUP:
		return "SCM_CMD_TLS_SETUP";
	case SCM_CMD_TLS_GET_REMOTE:
		return "SCM_CMD_TLS_GET_REMOTE";
	case SCM_CMD_TLS_ENCRYPT:
		return "SCM_CMD_TLS_ENCRYPT";
	case SCM_CMD_TLS_DECRYPT:
		return "SCM_CMD_TLS_DECRYPT";
	case SCM_CMD_TLS_CLOSE:
		return "SCM_CMD_TLS_CLOSE";
	case SCM_CMD_STORAGE_LIST:
		return "SCM_CMD_STORAGE_LIST";
	case SCM_CMD_STORAGE_NEW:
		return "SCM_CMD_STORAGE_NEW";
	case SCM_CMD_STORAGE_INFO:
		return "SCM_CMD_STORAGE_INFO";
	case SCM_CMD_STORAGE_OPEN:
		return "SCM_CMD_STORAGE_OPEN";
	case SCM_CMD_STORAGE_CLOSE:
		return "SCM_CMD_STORAGE_CLOSE";
	case SCM_CMD_STORAGE_READ:
		return "SCM_CMD_STORAGE_READ";
	case SCM_CMD_STORAGE_WRITE:
		return "SCM_CMD_STORAGE_WRITE";
	case SCM_CMD_STORAGE_CONFIRM:
		return "SCM_CMD_STORAGE_CONFIRM";
	case SCM_CMD_STORAGE_FIND_UNCONFIRMED:
		return "SCM_CMD_STORAGE_FIND_UNCONFIRMED";
	case SCM_CMD_CONNECTION_INIT:
		return "SCM_CMD_CONNECTION_INIT";
	case SCM_CMD_CONNECTION_SET_CERTS://not used
		return "SCM_CMD_CONNECTION_SET_CERTS";
	case SCM_CMD_CONNECTION_PUT:
		return "SCM_CMD_CONNECTION_PUT";
	case SCM_CMD_CONNECTION_GET:
		return "SCM_CMD_CONNECTION_GET";
	case SCM_CMD_CONNECTION_STATUS:
		return "SCM_CMD_CONNECTION_STATUS";
	case SCM_CMD_CONNECTION_CLOSE:
		return "SCM_CMD_CONNECTION_CLOSE";
	case SCM_CMD_CMS_ENCRYPT:
		return "SCM_CMD_CMS_ENCRYPT";
	case SCM_CMD_CMS_DECRYPT:
		return "SCM_CMD_CMS_DECRYPT";
	case SCM_CMD_GET_ALG_INFO:
		return "SCM_CMD_GET_ALG_INFO";
	case SCM_CMD_SET_ALG_INFO:
		return "SCM_CMD_SET_ALG_INFO";
	case SCM_CMD_ALARM_GET:
		return "SCM_CMD_ALARM_GET";
	case SCM_CMD_ALARM_SET:
		return "SCM_CMD_ALARM_SET";
	default:
		return "UNKONOWN";
	}
}


bool SKZI::initSKZI()
{
	bool on = SKZI_ON(0);
	if (on == false)
	{
		SKZI_OFF();
		DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "POWER ON TIMEOUT");
		return false;
	}
	else
	{
		DBG::PRINT(LVL::INFO, "SKZI", "" , "POWER ON OK");
		on = test_SKZI();
		if (on == false)
		{
			SKZI_OFF();
			DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "CONECTION TIMEOUT");
			return false;
		}
		else
		{
			DBG::PRINT(LVL::INFO, "SKZI", "" , "CONECTION OK");
			on = get_time_SKZI(&info.gm);
			if (on == false)
			{
				SKZI_OFF();
				DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "GET SKZI TIME FAILED");
				return false;
			}
			else
			{
				char strBuf[50];
				sprintf((char *)&strBuf, "%02d.%02d.%04d %02d:%02d:%02d", info.gm.tm_mday, info.gm.tm_mon, info.gm.tm_year, info.gm.tm_hour, info.gm.tm_min, info.gm.tm_sec);
				DBG::PRINT(LVL::INFO, "SKZI", "" , "SKZI TIME: ", (char *)&strBuf);
				return true;
			}
		}
	}
}

#if 0
void SKZI::SKZI_ON()
{
	HAL_GPIO_WritePin(SCM_PWR_ON_GPIO_Port, SCM_PWR_ON_Pin, GPIO_PIN_SET); // pwr on
	HAL_GPIO_WritePin(SCM_OFF_GPIO_Port, SCM_OFF_Pin, GPIO_PIN_SET);
}

SKZI::SKZI()
{
}

bool SKZI::test() {
	return 1;
}
#endif
