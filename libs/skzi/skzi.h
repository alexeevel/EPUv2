/*
 * skzi.h
 *
 *  Created on: Jan 17, 2021
 *      Author: abutko
 */

#ifndef INC_SKZI_H_
#define INC_SKZI_H_

int SKZI_start();
int power_On_SKZI();
int get_state_SKZI();
int send_SKZI(char *send_buf, int buf_size, int timeout);

#endif /* INC_SKZI_H_ */
