#ifndef __SCM_API_H__
#define __SCM_API_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define SCM_PROTO_TAG			0x47

#define SCM_PACKET_MAX			(16384 / 2)

// Module timeouts
#define SCM_TIMEOUT_REPLY		5000 //??? milliseconds
#define SCM_TIMEOUT_HANG		10000 //??? milliseconds, depends on application

#define SCM_UART_HELLO			"SCM READY\n"

// Data type IDs
//TODO: change to defines ???
enum {
	SCM_DATA_BYTE = 0,       // data size - 1 byte
	SCM_DATA_INTEGER,    // data size - 4 bytes (signed integer)
	SCM_DATA_BINARY,     // data size - variable
	SCM_DATA_TIME,       // data size - 4 bytes (Unix epoch - seconds from 01/01/1970 00:00)
	SCM_DATA_STRING,     // data size - variable
	SCM_DATA_ERROR,      // data size - 4 bytes
	SCM_DATA_ALG_ID,     // data size - 4 bytes
	SCM_DATA_KEY_ID,     // data size - sizeof(scm_data_key_id_t)
	SCM_DATA_HANDLE,     // data size - 4 bytes ???
	SCM_DATA_CERT,       // data size - variable
};

typedef uint8_t scm_data_byte_t;
typedef int32_t scm_data_integer_t;
typedef uint32_t scm_data_time_t;//too old dates (before 1970) not supported
typedef int32_t scm_data_error_t;
typedef int32_t scm_data_handle_t;
typedef uint32_t scm_data_alg_id_t;

// Module status type
#define SCM_MODULE_STATUS_TYPE_COMMON		0 //not required
#define SCM_MODULE_STATUS_TYPE_KEYINIT		1 //cryptoprovider type required (SCM_DATA_ALG_ID)

// Module states
#define SCM_MODULE_STATE_READY			0 // module is ready
#define SCM_MODULE_STATE_INITIALIZING		1 // module is in initializing stage, limited number of functions are available
#define SCM_MODULE_STATE_ERROR			2 // some error occurs, limited number of functions are available

// Packet flags (bit mask)
#define SCM_PACKET_FLAG_CMD			(1 << 0) // packet contains command
#define SCM_PACKET_FLAG_REPLY			(1 << 1) // packet contains reply to command
#define SCM_PACKET_FLAG_CONTINUE		(1 << 2) // more data expected

//TODO: change algorithm types to provider types
// Algorithm types
//#define SCM_ALG_TYPE_SYM			0 // symmetric ciphering
//#define SCM_ALG_TYPE_SIGN			1 // asymmetric ciphering (signature, key exchange)

// Algorithm state (bit mask)
#define SCM_ALG_STATE_DISABLED			(1 << 0)
#define SCM_ALG_STATE_NO_KEYS			(1 << 1)
#define SCM_ALG_STATE_ALARM			(1 << 2)
#define SCM_ALG_STATE_LICENSE			(1 << 3)

#define SCM_ALG_LICENSE_GENERIC			0

// Key usage
#define SCM_KEY_USAGE_FIRST			1
#define SCM_KEY_USAGE_CYPHER			1 // key for symmetric ciphering
#define SCM_KEY_USAGE_TLS			2 // key for TLS
#define SCM_KEY_USAGE_SIGN			3 // key for signature
#define SCM_KEY_USAGE_TLS_SERVICE		4 // key for service connection TLS
#define SCM_KEY_USAGE_LAST			4

// Cipher direction
#define SCM_CIPHER_ENCRYPT			0
#define SCM_CIPHER_DECRYPT			1

// Certificate check parameters (bit mask)
// General check parameters (what to check, at least one parameter must be provided)
#define SCM_CERT_CHECK_DATE			(1 << 0) // simple check of certificate date validity
#define SCM_CERT_CHECK_BASE			(1 << 1) // general certificate check again trusted certificates (for example, can be certificate used for signature)
#define SCM_CERT_CHECK_TLS_CLIENT		(1 << 2) // check if certificate can be used as client certificate for TLS
#define SCM_CERT_CHECK_TLS_SERVER		(1 << 3) // check if certificate can be used as server certificate for TLS
// Extra certificate check parameters (how to check, optional parameters)
#define SCM_CERT_CHECK_IGNORE_REVOCATION_CHAIN	(1 << 4) // ignore CRL while check a whole certificate's chain
#define SCM_CERT_CHECK_IGNORE_WRONG_USAGE	(1 << 5) // ignore wrong usages in certificate's chain (used for TLS only)

#define SCM_CONNECTION_TYPE_UNKNOWN		0
#define SCM_CONNECTION_TYPE_KEYADMIN		1
#define SCM_CONNECTION_TYPE_KEYINIT		2
#define SCM_CONNECTION_TYPE_CRL_UPDATE		3
//#define SCM_CONNECTION_TYPE_KEYTOKEN		4
#define SCM_CONNECTION_TYPE_SIGNCHAIN_SERVER	5

#define SCM_CONNECTION_TRANSPORT_UNKNOWN	0
#define SCM_CONNECTION_TRANSPORT_TCP_CLIENT	1
#define SCM_CONNECTION_TRANSPORT_TCP_SERVER	2

#define SCM_CONNECTION_PROTOCOL_TLS_1_0		(1 << 0)
#define SCM_CONNECTION_PROTOCOL_TLS_1_1		(1 << 1)
#define SCM_CONNECTION_PROTOCOL_TLS_1_2		(1 << 2)
#define SCM_CONNECTION_PROTOCOL_GMTLS		(1 << 4) //not required (cryptoprovider CN uses it automatically)

// Connection side
#define SCM_CONNECTION_SERVER			0 // module is server
#define SCM_CONNECTION_CLIENT			1 // module is client

#define SCM_CONNECTION_AUTH_NONE		0
#define SCM_CONNECTION_AUTH_STRICT		1

// Connection status (TLS session, service connection)
//#define SCM_CONNECTION_STATUS_CONNECTING	0 // connection/session is opening
#define SCM_CONNECTION_STATUS_CONNECTED		1 // connection/session is established (ready to transmit data)
#define SCM_CONNECTION_STATUS_CLOSED		2 // connection/session is closed
#define SCM_CONNECTION_STATUS_ERROR		3 // connection/session has encounter error, must be closed
#define SCM_CONNECTION_STATUS_NEED_MORE_DATA	4 // connection/session nned more data from remote side to be established

// Connection/session closing reason
#define SCM_SESSION_CLOSE_REASON_COMPLETED	0 // session is closed due to protocol logic, communication channel is still opened,
                                                  // session must be closed in a proper way
#define SCM_SESSION_CLOSE_REASON_DISCONNECTED	1 // communication channel is closed, closing of session in a proper way is impossible
#define SCM_SESSION_CLOSE_REASON_CLOSING	2 // session is prematurely closing, communication channel is still opened,
                                                  // session must be closed in a proper way (if possible)
#define SCM_SESSION_CLOSE_REASON_ABORTED	3 // session is forcibly closing, closing of session in a proper way is impossible

// Storage types
#define SCM_STORAGE_TYPE_STATE			0 // storage has fixed count of slots for data/records
#define SCM_STORAGE_TYPE_LOG			1 // growing storage, recorded items are added to the end (after the last record)
#define SCM_STORAGE_TYPE_DOCUMENT		2 // growing storage, recorded items are added to the end (after the last record)

#define SCM_STORAGE_NAME_MAX			32
#define SCM_STORAGE_ITEM_NAME_MAX		32

#define SCM_STORAGE_FLAG_INTERNAL		(1 << 0) //created by module itself, unavailable outside of module
#define SCM_STORAGE_FLAG_READ_ONLY		(1 << 1)
#define SCM_STORAGE_FLAG_CYCLIC			(1 << 2)
#define SCM_STORAGE_FLAG_ENCRYPT		(1 << 3) //ecnrypt items
#define SCM_STORAGE_FLAG_SIGN			(1 << 4) //sign written items
#define SCM_STORAGE_FLAG_SYSTEM			(1 << 5) //created by module itself, available outside of module
#define SCM_STORAGE_FLAGS_ALL			(SCM_STORAGE_FLAG_INTERNAL | SCM_STORAGE_FLAG_READ_ONLY | SCM_STORAGE_FLAG_CYCLIC | \
							SCM_STORAGE_FLAG_ENCRYPT | SCM_STORAGE_FLAG_SIGN | SCM_STORAGE_FLAG_SYSTEM)

#define SCM_STORAGE_DATA_TYPE_ATTRIBUTES	0
#define SCM_STORAGE_DATA_TYPE_CONTENT		1
#define SCM_STORAGE_DATA_TYPE_TICKET		2
#define SCM_STORAGE_DATA_TYPE_SIGNATURE		3

#define SCM_STORAGE_ITEM_FLAG_CONFIRMATION	(1 << 0)

#define SCM_ACCESS_READ				(1 << 0)
#define SCM_ACCESS_WRITE			(1 << 1)

#define SCM_CRYPTO_PROVIDER_TYPE_UNKNOWN	0
#define SCM_CRYPTO_PROVIDER_TYPE_GOST		1
#define SCM_CRYPTO_PROVIDER_TYPE_CN		2
#define SCM_CRYPTO_PROVIDER_TYPE_RSA		3
#define SCM_CRYPTO_PROVIDER_TYPE_BELR		4
#define SCM_CRYPTO_PROVIDER_TYPE_DEFAULT	SCM_CRYPTO_PROVIDER_TYPE_GOST

#define SCM_CRYPTO_HASH_TYPE_UNKNOWN		0 //use default hash type of current cryptoprovider
#define SCM_CRYPTO_HASH_TYPE_GOST2012_256	1
#define SCM_CRYPTO_HASH_TYPE_SM3		2
#define SCM_CRYPTO_HASH_TYPE_SHA_256		3
#define SCM_CRYPTO_HASH_TYPE_BELT		4

#define SCM_TLS_VERIFY_NONE			0
#define SCM_TLS_VERIFY_IF_PRESENT		1
#define SCM_TLS_VERIFY_ALWAYS			2

#define SCM_TLS_FLAG_MUTUAL_AUTH		(1 << 0) //mutual authorization of server and client (both sides request remote certificates)
#define SCM_TLS_FLAG_IGNORE_REVOCATION		(1 << 1) //flag to verify remote certificate
#define SCM_TLS_FLAG_IGNORE_UNKNOWN_CA		(1 << 2) //flag to verify remote certificate
#define SCM_TLS_FLAG_IGNORE_WRONG_USAGE		(1 << 3) //flag to verify remote certificate
#define SCM_TLS_FLAG_IGNORE_CERT_CN_INVALID	(1 << 4) //flag to verify remote certificate
#define SCM_TLS_FLAG_IGNORE_CERT_DATE_INVALID	(1 << 5) //flag to verify remote certificate

#define SCM_CMS_FLAG_SIGNING_TIME		(1 << 0) //include or check signing time
#define SCM_CMS_FLAG_CHECK_SIGNER		(1 << 1) //check signer certificate if present

#define SCM_CMS_RESULT_SIGNATURE_NOT_VERIFIED	0 //bad signature or certificate not found
#define SCM_CMS_RESULT_SIGNATURE_VERIFIED	1 //only CMS signature verified
#define SCM_CMS_RESULT_CERTIFICATE_VERIFIED	2 //CMS signature and signer certificate verified (using current time)
#define SCM_CMS_RESULT_TIME_VERIFIED		3 //CMS signature and signer certificate verified (using signing time)

#define SCM_ALARM_MAX_COUNT			16

// Alarm flags (bit mask)
#define SCM_ALARM_FLAG_UNRECOVERABLE		(1 << 0)
#define SCM_ALARM_FLAG_READ_ONLY		(1 << 1)
#define SCM_ALARM_FLAG_DELETE_KEYS		(1 << 2)

#define SCM_REBOOT_TYPE_NORMAL			0
#define SCM_REBOOT_TYPE_BOOTLOADER		1

// Key update flags (bit mask)
#define SCM_KEY_UPDATE_FLAG_UPDATE		(1 << 0)

// Command codes
//TODO: change to defines ???
enum {
	// control
	SCM_CMD_CONTINUE = 0,
	SCM_CMD_ABORT,

	// module id and status
	SCM_CMD_PING,
	SCM_CMD_MODULE_ID,
	SCM_CMD_MODULE_STATUS,
	SCM_CMD_MODULE_GET_TIME,
	SCM_CMD_MASTER_ID,

	// base crypto functions
	SCM_CMD_GET_ALG_LIST,
	SCM_CMD_GET_KEY_INFO,
	SCM_CMD_GET_CRL,
	SCM_CMD_CERT_CHECK,
	SCM_CMD_CRYPT_INIT,
	SCM_CMD_CRYPT_UPDATE,
	SCM_CMD_CRYPT_FINAL,
	SCM_CMD_HASH_INIT,
	SCM_CMD_HASH_UPDATE,
	SCM_CMD_HASH_FINAL,
	SCM_CMD_SIGN_INIT,
	SCM_CMD_SIGN_UPDATE,
	SCM_CMD_SIGN_FINAL,
	SCM_CMD_VERIFY_INIT,
	SCM_CMD_VERIFY_UPDATE,
	SCM_CMD_VERIFY_FINAL,

	// TLS
	SCM_CMD_TLS_INIT,
	SCM_CMD_TLS_SET_CERTS, //not implemented
	SCM_CMD_TLS_SETUP,
	SCM_CMD_TLS_GET_REMOTE,
	SCM_CMD_TLS_ENCRYPT,
	SCM_CMD_TLS_DECRYPT,
	SCM_CMD_TLS_CLOSE,

	// storages
	SCM_CMD_STORAGE_LIST,
	SCM_CMD_STORAGE_NEW,
	SCM_CMD_STORAGE_INFO,
	SCM_CMD_STORAGE_OPEN,
	SCM_CMD_STORAGE_CLOSE,
	SCM_CMD_STORAGE_READ,
	SCM_CMD_STORAGE_WRITE,
	SCM_CMD_STORAGE_CONFIRM,
	SCM_CMD_STORAGE_FIND_UNCONFIRMED,

	// service connection
	SCM_CMD_CONNECTION_INIT,
	SCM_CMD_CONNECTION_SET_CERTS,//not implemented
	SCM_CMD_CONNECTION_PUT,
	SCM_CMD_CONNECTION_GET,
	SCM_CMD_CONNECTION_STATUS,
	SCM_CMD_CONNECTION_CLOSE,

	//CMS
	SCM_CMD_CMS_ENCRYPT,
	SCM_CMD_CMS_DECRYPT,

	//for internal usage
	SCM_CMD_GET_ALG_INFO,
	SCM_CMD_SET_ALG_INFO,

	//alarm
	SCM_CMD_ALARM_GET,
	SCM_CMD_ALARM_SET,

	SCM_CMD_SET_CRL,
	SCM_CMD_REBOOT,

	SCM_CMD_STORAGE_WRITE_BEGIN,
	SCM_CMD_STORAGE_WRITE_APPEND,
	SCM_CMD_STORAGE_WRITE_END,

	SCM_CMD_KEY_UPDATE,


	//in debug version only
	SCM_CMD_SET_TIME = 250,
};

// Reply codes
//TODO: change to defines ???
enum {
	//common replies
	SCM_REPLY_WAIT = 0,
	SCM_REPLY_ERROR,

	// module id and status
	SCM_REPLY_PING,
	SCM_REPLY_MODULE_ID,
	SCM_REPLY_MODULE_STATUS,
	SCM_REPLY_MODULE_GET_TIME,
	SCM_REPLY_MASTER_ID,

	// base crypto functions
	SCM_REPLY_GET_ALG_LIST,
	SCM_REPLY_GET_KEY_INFO,
	SCM_REPLY_GET_CRL,
	SCM_REPLY_CERT_CHECK,
	SCM_REPLY_CRYPT_INIT,
	SCM_REPLY_CRYPT_UPDATE,
	SCM_REPLY_CRYPT_FINAL,
	SCM_REPLY_HASH_INIT,
	SCM_REPLY_HASH_UPDATE,
	SCM_REPLY_HASH_FINAL,
	SCM_REPLY_SIGN_INIT,
	SCM_REPLY_SIGN_UPDATE,
	SCM_REPLY_SIGN_FINAL,
	SCM_REPLY_VERIFY_INIT,
	SCM_REPLY_VERIFY_UPDATE,
	SCM_REPLY_VERIFY_FINAL,

	// TLS
	SCM_REPLY_TLS_INIT,
	SCM_REPLY_TLS_SET_CERTS,
	SCM_REPLY_TLS_SETUP,
	SCM_REPLY_TLS_GET_REMOTE,
	SCM_REPLY_TLS_ENCRYPT,
	SCM_REPLY_TLS_DECRYPT,
	SCM_REPLY_TLS_CLOSE,

	// storages
	SCM_REPLY_STORAGE_LIST,
	SCM_REPLY_STORAGE_NEW,
	SCM_REPLY_STORAGE_INFO,
	SCM_REPLY_STORAGE_OPEN,
	SCM_REPLY_STORAGE_CLOSE,
	SCM_REPLY_STORAGE_READ,
	SCM_REPLY_STORAGE_WRITE,
	SCM_REPLY_STORAGE_CONFIRM,
	SCM_REPLY_STORAGE_FIND_UNCONFIRMED,

	// service connection
	SCM_REPLY_CONNECTION_INIT,
	SCM_REPLY_CONNECTION_SET_CERTS,
	SCM_REPLY_CONNECTION_PUT,
	SCM_REPLY_CONNECTION_GET,
	SCM_REPLY_CONNECTION_STATUS,
	SCM_REPLY_CONNECTION_CLOSE,

	//CMS
	SCM_REPLY_CMS_ENCRYPT,
	SCM_REPLY_CMS_DECRYPT,

	//for internal usage
	SCM_REPLY_GET_ALG_INFO,
	SCM_REPLY_SET_ALG_INFO,

	//alarm
	SCM_REPLY_ALARM_GET,
	SCM_REPLY_ALARM_SET,

	SCM_REPLY_SET_CRL,
	SCM_REPLY_REBOOT,

	SCM_REPLY_STORAGE_WRITE_BEGIN,
	SCM_REPLY_STORAGE_WRITE_APPEND,
	SCM_REPLY_STORAGE_WRITE_END,

	SCM_REPLY_KEY_UPDATE,

	//in debug version only
	SCM_REPLY_SET_TIME = 250,
};

enum {
	SCM_ERROR_INVALID_CMD = 1,
	SCM_ERROR_INVALID_PARAM,
	SCM_ERROR_INVALID_PARAM_VALUE,
	SCM_ERROR_OUT_OF_RESOURCE,
	SCM_ERROR_NOT_READY,
	SCM_ERROR_NOT_SUPPORTED,
	SCM_ERROR_INVALID_STATE,
	SCM_ERROR_ITEM_NOT_FOUND,
	SCM_ERROR_CRYPT,
	SCM_ERROR_STORAGE,
	SCM_ERROR_ALREADY_EXISTS,
	SCM_ERROR_ABORTED,
	SCM_ERROR_ACCESS_DENIED,
	SCM_ERROR_SERVICE,
};

// At least MSVC and GCC packed structures are compatible.
#undef DECLARE_PACKED_STRUCT_MSC_BEGIN
#undef DECLARE_PACKED_STRUCT_MSC_END
#undef DECLARE_PACKED_STRUCT_GNUC
#ifdef __GNUC__
  #define DECLARE_PACKED_STRUCT_MSC_BEGIN
  #define DECLARE_PACKED_STRUCT_MSC_END
  #define DECLARE_PACKED_STRUCT_GNUC		__attribute__((packed, aligned(1)))
#endif
#ifdef _MSC_VER
  #define DECLARE_PACKED_STRUCT_MSC_BEGIN	#pragma pack(push, 1)
  #define DECLARE_PACKED_STRUCT_MSC_END		#pragma pack(pop)
  #define DECLARE_PACKED_STRUCT_GNUC
#endif
#if !defined(DECLARE_PACKED_STRUCT_MSC_BEGIN) || !defined(DECLARE_PACKED_STRUCT_MSC_END) \
	|| !defined(DECLARE_PACKED_STRUCT_GNUC)
  #error "Unsupported platform"
#endif

typedef uint16_t scm_packet_crc_t;

// Command packet header
DECLARE_PACKED_STRUCT_MSC_BEGIN
typedef struct scm_packet_cmd_header_t {
	uint8_t proto_tag; // SCM_PROTO_TAG
	uint8_t cmd;       // command code (see SCM_CMD_...)
	uint8_t flags;     // packet flags (see SCM_PACKET_FLAG_...)
	uint16_t size;     // command data size (data follows this header)
	scm_packet_crc_t header_crc;
	scm_packet_crc_t data_crc;
} DECLARE_PACKED_STRUCT_GNUC scm_packet_cmd_header_t;
DECLARE_PACKED_STRUCT_MSC_END

// Reply packet header
DECLARE_PACKED_STRUCT_MSC_BEGIN
typedef struct scm_packet_reply_header_t {
	uint8_t proto_tag; // SCM_PROTO_TAG
	uint8_t reply;     // reply code (see SCM_REPLY_...)
	uint8_t flags;     // packet flags (see SCM_PACKET_FLAG_...)
	uint16_t size;     // command data size (data follows this header)
	scm_packet_crc_t header_crc;
	scm_packet_crc_t data_crc;
} DECLARE_PACKED_STRUCT_GNUC scm_packet_reply_header_t;
DECLARE_PACKED_STRUCT_MSC_END

DECLARE_PACKED_STRUCT_MSC_BEGIN
typedef struct DECLARE_PACKED_STRUCT_GNUC scm_packet_param_header_t {
	uint8_t tag;       //param type
	uint32_t size;     //param size
} scm_packet_param_header_t;
DECLARE_PACKED_STRUCT_MSC_END

DECLARE_PACKED_STRUCT_MSC_BEGIN
typedef struct scm_data_key_id_t {
	uint8_t usage;     // key usage (see SCM_KEY_USAGE_...)
	uint8_t number;    // number among keys with the same key usage, default - 0
} DECLARE_PACKED_STRUCT_GNUC scm_data_key_id_t;
DECLARE_PACKED_STRUCT_MSC_END

//header for storage's signature data.
//signature follow immediately this header.
//storage can have several signatures for an item, they are
//follow one by one (header, signature, header, signature, ...).
#define SCM_STORAGE_ITEM_SIGNATURE_HEADER_MAGIC			0x44485349 //"ISHD"
DECLARE_PACKED_STRUCT_MSC_BEGIN
typedef struct scm_storage_item_signature_header_t {
	uint32_t magic;//= SCM_STORAGE_ITEM_SIGNATURE_HEADER_MAGIC
	uint32_t provider;
	uint32_t sign_begin_time;
	uint32_t sign_end_time;
	uint32_t key_usage;
	uint32_t key_number;
	uint32_t sign_size;
} DECLARE_PACKED_STRUCT_GNUC scm_storage_item_signature_header_t;
DECLARE_PACKED_STRUCT_MSC_END

#ifdef __cplusplus
}
#endif

#endif
