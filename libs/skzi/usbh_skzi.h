/*
 * usbh_skzi.h
 *
 *  Created on: May 2, 2021
 *      Author: abutko
 */

#ifndef INC_USBH_SKZI_H_
#define INC_USBH_SKZI_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "usbh_core.h"


#define USB_SKZI_CLASS                                           0xFFU /* Still Image Class)*/

#define SKZI_LINE_CODING_STRUCTURE_SIZE                              0x07U

#define SKZI_SET_LINE_CODING                                     0x20U
#define SKZI_GET_LINE_CODING                                     0x21U
#define SKZI_LINE_CODING_STRUCTURE_SIZE                              0x07U

#define SKZI_DATA_INTERFACE_CLASS_CODE                               0x0AU
#define SKZI_RESERVED                                                0x00U
#define SKZI_NO_CLASS_SPECIFIC_PROTOCOL_CODE                         0x00U

/* States for CDC State Machine */
typedef enum
{
  SKZI_IDLE = 0U,
  SKZI_SEND_DATA,
  SKZI_SEND_DATA_WAIT,
  SKZI_RECEIVE_DATA,
  SKZI_RECEIVE_DATA_WAIT,
}
SKZI_DataStateTypeDef;

typedef enum
{
  SKZI_IDLE_STATE = 0U,
  SKZI_SET_LINE_CODING_STATE,
  SKZI_GET_LAST_LINE_CODING_STATE,
  SKZI_TRANSFER_DATA,
  SKZI_ERROR_STATE,
}
SKZI_StateTypeDef;


/*Line coding structure*/
typedef union _SKZI_LineCodingStructure
{
  uint8_t Array[SKZI_LINE_CODING_STRUCTURE_SIZE];

  struct
  {

    uint32_t             dwDTERate;     /*Data terminal rate, in bits per second*/
    uint8_t              bCharFormat;   /*Stop bits
    0 - 1 Stop bit
    1 - 1.5 Stop bits
    2 - 2 Stop bits*/
    uint8_t              bParityType;   /* Parity
    0 - None
    1 - Odd
    2 - Even
    3 - Mark
    4 - Space*/
    uint8_t                bDataBits;     /* Data bits (5, 6, 7, 8 or 16). */
  } b;
}
SKZI_LineCodingTypeDef;

/* Header Functional Descriptor
--------------------------------------------------------------------------------
Offset|  field              | Size  |    Value   |   Description
------|---------------------|-------|------------|------------------------------
0     |  bFunctionLength    | 1     |   number   |  Size of this descriptor.
1     |  bDescriptorType    | 1     |   Constant |  CS_INTERFACE (0x24)
2     |  bDescriptorSubtype | 1     |   Constant |  Identifier (ID) of functional
      |                     |       |            | descriptor.
3     |  bcdCDC             | 2     |            |
      |                     |       |   Number   | USB Class Definitions for
      |                     |       |            | Communication Devices Specification
      |                     |       |            | release number in binary-coded
      |                     |       |            | decimal
------|---------------------|-------|------------|------------------------------
*/
typedef struct _SKZI_FunctionalDescriptorHeader
{
  uint8_t     bLength;            /*Size of this descriptor.*/
  uint8_t     bDescriptorType;    /*CS_INTERFACE (0x24)*/
  uint8_t     bDescriptorSubType; /* Header functional descriptor subtype as*/
  uint16_t    bcdCDC;             /* USB Class Definitions for Communication
                                    Devices Specification release number in
                                  binary-coded decimal. */
}
SKZI_HeaderFuncDesc_TypeDef;
/* Call Management Functional Descriptor
--------------------------------------------------------------------------------
Offset|  field              | Size  |    Value   |   Description
------|---------------------|-------|------------|------------------------------
0     |  bFunctionLength    | 1     |   number   |  Size of this descriptor.
1     |  bDescriptorType    | 1     |   Constant |  CS_INTERFACE (0x24)
2     |  bDescriptorSubtype | 1     |   Constant |  Call Management functional
      |                     |       |            |  descriptor subtype.
3     |  bmCapabilities     | 1     |   Bitmap   | The capabilities that this configuration
      |                     |       |            | supports:
      |                     |       |            | D7..D2: RESERVED (Reset to zero)
      |                     |       |            | D1: 0 - Device sends/receives call
      |                     |       |            | management information only over
      |                     |       |            | the Communication Class
      |                     |       |            | interface.
      |                     |       |            | 1 - Device can send/receive call
      |                     \       |            | management information over a
      |                     |       |            | Data Class interface.
      |                     |       |            | D0: 0 - Device does not handle call
      |                     |       |            | management itself.
      |                     |       |            | 1 - Device handles call
      |                     |       |            | management itself.
      |                     |       |            | The previous bits, in combination, identify
      |                     |       |            | which call management scenario is used. If bit
      |                     |       |            | D0 is reset to 0, then the value of bit D1 is
      |                     |       |            | ignored. In this case, bit D1 is reset to zero for
      |                     |       |            | future compatibility.
4     | bDataInterface      | 1     | Number     | Interface number of Data Class interface
      |                     |       |            | optionally used for call management.
------|---------------------|-------|------------|------------------------------
*/
typedef struct _SKZI_CallMgmtFunctionalDescriptor
{
  uint8_t    bLength;            /*Size of this functional descriptor, in bytes.*/
  uint8_t    bDescriptorType;    /*CS_INTERFACE (0x24)*/
  uint8_t    bDescriptorSubType; /* Call Management functional descriptor subtype*/
  uint8_t    bmCapabilities;      /* bmCapabilities: D0+D1 */
  uint8_t    bDataInterface;      /*bDataInterface: 1*/
}
SKZI_CallMgmtFuncDesc_TypeDef;
/* Abstract Control Management Functional Descriptor
--------------------------------------------------------------------------------
Offset|  field              | Size  |    Value   |   Description
------|---------------------|-------|------------|------------------------------
0     |  bFunctionLength    | 1     |   number   |  Size of functional descriptor, in bytes.
1     |  bDescriptorType    | 1     |   Constant |  CS_INTERFACE (0x24)
2     |  bDescriptorSubtype | 1     |   Constant |  Abstract Control Management
      |                     |       |            |  functional  descriptor subtype.
3     |  bmCapabilities     | 1     |   Bitmap   | The capabilities that this configuration
      |                     |       |            | supports ((A bit value of zero means that the
      |                     |       |            | request is not supported.) )
                                                   D7..D4: RESERVED (Reset to zero)
      |                     |       |            | D3: 1 - Device supports the notification
      |                     |       |            | Network_Connection.
      |                     |       |            | D2: 1 - Device supports the request
      |                     |       |            | Send_Break
      |                     |       |            | D1: 1 - Device supports the request
      |                     \       |            | combination of Set_Line_Coding,
      |                     |       |            | Set_Control_Line_State, Get_Line_Coding, and the
                                                   notification Serial_State.
      |                     |       |            | D0: 1 - Device supports the request
      |                     |       |            | combination of Set_Comm_Feature,
      |                     |       |            | Clear_Comm_Feature, and Get_Comm_Feature.
      |                     |       |            | The previous bits, in combination, identify
      |                     |       |            | which requests/notifications are supported by
      |                     |       |            | a Communication Class interface with the
      |                     |       |            |   SubClass code of Abstract Control Model.
------|---------------------|-------|------------|------------------------------
*/
typedef struct _SKZI_AbstractCntrlMgmtFunctionalDescriptor
{
  uint8_t    bLength;            /*Size of this functional descriptor, in bytes.*/
  uint8_t    bDescriptorType;    /*CS_INTERFACE (0x24)*/
  uint8_t    bDescriptorSubType; /* Abstract Control Management functional
                                  descriptor subtype*/
  uint8_t    bmCapabilities;      /* The capabilities that this configuration supports */
}
SKZI_AbstCntrlMgmtFuncDesc_TypeDef;
/* Union Functional Descriptor
--------------------------------------------------------------------------------
Offset|  field              | Size  |    Value   |   Description
------|---------------------|-------|------------|------------------------------
0     |  bFunctionLength    | 1     |   number   |  Size of this descriptor.
1     |  bDescriptorType    | 1     |   Constant |  CS_INTERFACE (0x24)
2     |  bDescriptorSubtype | 1     |   Constant |  Union functional
      |                     |       |            |  descriptor subtype.
3     |  bMasterInterface   | 1     |   Constant | The interface number of the
      |                     |       |            | Communication or Data Class interface
4     | bSlaveInterface0    | 1     | Number     | nterface number of first slave or associated
      |                     |       |            | interface in the union.
------|---------------------|-------|------------|------------------------------
*/
typedef struct _SKZI_UnionFunctionalDescriptor
{
  uint8_t    bLength;            /*Size of this functional descriptor, in bytes*/
  uint8_t    bDescriptorType;    /*CS_INTERFACE (0x24)*/
  uint8_t    bDescriptorSubType; /* Union functional descriptor SubType*/
  uint8_t    bMasterInterface;   /* The interface number of the Communication or
                                 Data Class interface,*/
  uint8_t    bSlaveInterface0;   /*Interface number of first slave*/
}
SKZI_UnionFuncDesc_TypeDef;

typedef struct _USBH_SKZIInterfaceDesc
{
  SKZI_HeaderFuncDesc_TypeDef           SKZI_HeaderFuncDesc;
  SKZI_CallMgmtFuncDesc_TypeDef         SKZI_CallMgmtFuncDesc;
  SKZI_AbstCntrlMgmtFuncDesc_TypeDef    SKZI_AbstCntrlMgmtFuncDesc;
  SKZI_UnionFuncDesc_TypeDef            SKZI_UnionFuncDesc;
}
SKZI_InterfaceDesc_Typedef;


/* Structure for CDC process */
typedef struct
{
  uint8_t              NotifPipe;
  uint8_t              NotifEp;
  uint8_t              buff[8];
  uint16_t             NotifEpSize;
}
SKZI_CommItfTypedef ;

typedef struct
{
  uint8_t              InPipe;
  uint8_t              OutPipe;
  uint8_t              OutEp;
  uint8_t              InEp;
  uint8_t              buff[8];
  uint16_t             OutEpSize;
  uint16_t             InEpSize;
}
SKZI_DataItfTypedef ;

/* Structure for CDC process */
typedef struct _SKZI_Process
{
  SKZI_CommItfTypedef                CommItf;
  SKZI_DataItfTypedef                DataItf;
  uint8_t                           *pTxData;
  uint8_t                           *pRxData;
  uint32_t                           TxDataLength;
  uint32_t                           RxDataLength;
  SKZI_InterfaceDesc_Typedef         SKZI_Desc;
  SKZI_LineCodingTypeDef             LineCoding;
  SKZI_LineCodingTypeDef             *pUserLineCoding;
  SKZI_StateTypeDef                  state;
  SKZI_DataStateTypeDef              data_tx_state;
  SKZI_DataStateTypeDef              data_rx_state;
  uint8_t                           Rx_Poll;
}
SKZI_HandleTypeDef;

extern USBH_ClassTypeDef  SKZI_Class;
#define USBH_SKZI_CLASS    &SKZI_Class

USBH_StatusTypeDef  USBH_SKZI_SetLineCoding(USBH_HandleTypeDef *phost, SKZI_LineCodingTypeDef *linecoding);

USBH_StatusTypeDef  USBH_SKZI_GetLineCoding(USBH_HandleTypeDef *phost, SKZI_LineCodingTypeDef *linecoding);

USBH_StatusTypeDef  USBH_SKZI_Transmit(USBH_HandleTypeDef *phost,
                                      uint8_t *pbuff,
                                      uint32_t length);

USBH_StatusTypeDef  USBH_SKZI_Receive(USBH_HandleTypeDef *phost,
                                     uint8_t *pbuff,
                                     uint32_t length);


uint16_t            USBH_SKZI_GetLastReceivedDataSize(USBH_HandleTypeDef *phost);

USBH_StatusTypeDef  USBH_SKZI_Stop(USBH_HandleTypeDef *phost);

void USBH_SKZI_LineCodingChanged(USBH_HandleTypeDef *phost);

void USBH_SKZI_TransmitCallback(USBH_HandleTypeDef *phost);

void USBH_SKZI_ReceiveCallback(USBH_HandleTypeDef *phost);


#ifdef __cplusplus
}
#endif

#endif /* INC_USBH_SKZI_H_ */
