/*
 * skzi.hpp
 *
 *  Created on: 14 июн. 2021 г.
 *      Author: abutko
 */

#ifndef SKZI_SKZI_HPP_
#define SKZI_SKZI_HPP_

#include "hwtesting/hwtesting.hpp"
#include "string.h"
#include "main.h"
#include "cmsis_os2.h"
#include "debug/debug.hpp"
#include "usbh_def.h"
#include "usbh_skzi.h"
#include "scm_api.h"
#include "times.h"
#include "gps.h"

uint32_t stopwatch_getticks();

using namespace debug;

#define MIN_START_TIME_MS					(140000)
#define MAX_START_TIME_MS					(150000)
#define STARTING_QUANT_TIME_MS				(500)
#define SKZI_POWER_OFF_TIME_MS				(500)

#define MAX_DEBUG_HEX_LEN					(150)
#define RX_BUF_SKZI_SIZE					(1000 * 4)
#define STATIC_MASTER_CTX_BUF_SKZI_SIZE		(1024 * 3)

//*********************************************************************************
//*************************** Interface to libtest_proto.so *******************************

#define SC_PROTO_MAX_PACKET_SIZE		SCM_PACKET_MAX
#define SC_PROTO_MAX_CMD_PAYLOAD_SIZE		(SC_PROTO_MAX_PACKET_SIZE - sizeof(scm_packet_cmd_header_t))
#define SC_PROTO_MAX_RESPONCE_PAYLOAD_SIZE	(SC_PROTO_MAX_PACKET_SIZE - sizeof(scm_packet_reply_header_t))

//opaque library context
struct sc_proto_ctx_t;
extern struct sc_proto_ctx_t *sc_proto_test_new_ctx(void);
extern void sc_proto_test_clear_ctx(struct sc_proto_ctx_t *ctx);

//call sc_proto_test_start before command sending
//storage_file is used for local test only (directory to be used as storage),
//transport_type, transport_path, wait_hello are used for transport test only.
extern int sc_proto_test_start(struct sc_proto_ctx_t *ctx, const char *storage_file, int transport_type, char *transport_path, int wait_hello);
extern void sc_proto_test_stop(struct sc_proto_ctx_t *ctx);

//receive from slave
//return size or error < 0
//delay_ms is used for local test only
//if delay_ms != NULL then function will emulate long command processing time, function
//  can return SCM_REPLY_WAIT responce packet, value of *delay_ms will be decreased by elapsed interval
extern int sc_proto_test_receive_responce_packet(struct sc_proto_ctx_t *ctx, char *buf, int buf_size, int *delay_ms);

//parse parameter header in packet (or do it yourself)
//expected_param_type can be < 0 (means - do not check),
//expected_param_size must be exact size or can be < 0 (do not check),
//max_param_size can be < 0 (do not check),
//real_param_size can be NULL (for example, if expected_param_size > 0),
//special cases:
//  for SCM_DATA_BYTE val must be unsigned integer,
//  for SCM_DATA_ERROR val must be integer,
//  for SCM_DATA_KEY_ID val must be scm_key_id_t,
//  for SCM_DATA_STRING sizeof(val) must be > max_param_size (at least max_param_size+1)
//extern int sc_proto_packet_param_get_value(int param_type, char *buf, int param_size,
//	int expected_param_type, int expected_param_size, int max_param_size, void *val, int *real_param_size);

//just return command name (cmd must be of SCM_CMD_... in scm_api.h)
extern const char *sc_proto_cmd_name(int cmd);
//*********************************************************************************

typedef struct cmd_params_t {
	int param_type;
	unsigned int param_size;
	char *buf;
	int buf_size;//for cmd - current buffer size, for responce - real param size in current packet !
	unsigned int param_parsed;//parsed size of param value (used for responce only)
} cmd_params_t;

typedef struct master_ctx_t {
	int delay_ms;//delay command execution (to test timeout processing)

	int cmd;//current command
	int expected_responce;
	cmd_params_t params[32];
	unsigned int param_count;

//	char *send_buf;
//	int send_max_size;
//	int send_data_size;

	char *receive_buf;
	int receive_real_size;
	int receive_max_size;

	int error;

	cmd_params_t responce_params[32];
	unsigned int responce_param_count;

	int timeout_ms;//hard timeout
	int responce_interval_ms;//soft timeout

} master_ctx_t;

struct sc_proto_ctx_t;

struct list_head {
	struct list_head *next;
	struct list_head *prev;
};

typedef struct sc_global_ctx_
{
	int state;
	struct sc_storage_io_ctx_t *storage_ctx;
	struct sc_proto_ctx_t *proto_ctx;
	struct sc_service_ctx_t *service_ctx;
	struct sc_transport_t *transport;
	int server;
	char *master_id;
	struct list_head handles;
	void (*log_critical)(struct sc_global_ctx_t *, const char *);
	void (*log_common)(struct sc_global_ctx_t *, const char *);
} sc_global_ctx;

typedef int (*cmd_callback_t)(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, intptr_t arg, char *packet_buf, int sent, int *need_continue);
typedef int (*responce_callback_t)(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, intptr_t arg,
	int error, int error_responce, char *data, int size);


extern uint8_t recvBufUartSKZI[RX_BUF_SKZI_SIZE];
extern uint8_t *skzi_RX_buf;
extern int skzi_RX_buf_cnt;
extern char packet_buf[SC_PROTO_MAX_PACKET_SIZE];
extern uint8_t staticMasterCtxBuffer[STATIC_MASTER_CTX_BUF_SKZI_SIZE];
extern char skzi_EOT;
extern char tls_server_buf[4 * 1024];
extern int tls_server_max_size;

extern USBH_HandleTypeDef hUsbHostFS;
extern int codeUSB;

__weak void usb_debug_cpp(const char *x, ...)
{

}


#define PRINT_LINE(fmt, ...)			(usb_debug_cpp(fmt "\r\n", ##__VA_ARGS__))
#define PRINT_INFO(fmt, ...)			PRINT_LINE("INFO:  " fmt, ##__VA_ARGS__);
#define PRINT_WARN(fmt, ...)			PRINT_LINE("WARN:  " fmt, ##__VA_ARGS__);
#define PRINT_DEBUG_FORCE(fmt, ...)		PRINT_LINE("DEBUG: " fmt, ##__VA_ARGS__);
#define PRINT_DEBUG(fmt, ...)			PRINT_LINE("DEBUG: " fmt, ##__VA_ARGS__);
#define PRINT_ERROR(fmt, ...)			PRINT_LINE("ERROR: " fmt, ##__VA_ARGS__);
#define PRINT_ERROR_CODE(code, fmt, ...)	PRINT_ERROR(fmt " - error %d", ##__VA_ARGS__, code);

#define DO_ERROR(code, fmt, ...) \
	do { \
		if (code) { \
			ret = code; \
			PRINT_ERROR_CODE(ret, fmt, ##__VA_ARGS__); \
		} else { \
			PRINT_ERROR(fmt, ##__VA_ARGS__); /*asm("  bkpt");*/\
		} \
		goto on_done; \
	} while (0)

#ifndef MIN
  #define MIN(x, y)				((x) < (y) ? (x) : (y))
#endif

#ifndef ARRAY_SIZE
  #define ARRAY_SIZE(x)				(sizeof(x) / sizeof(x[0]))
#endif


typedef enum
{
	EPLUMBSTATE_OPENED = 1,
	EPLUMBSTATE_ARMED = 20,
	EPLUMBSTATE_ALARMED = 33
} EPLUMBSTATES;

typedef enum
{
	EPLUMBCMD_NOTHING_TO_SEND = 0,
	EPLUMBCMD_LOCATE_COORDINATES = 1,
	EPLUMBCMD_SEND_COORDINATES = 10,
	EPLUMBCMD_STATUS_CHANGED = 2,
	EPLUMBCMD_ALARM_OPTO = 3,
	EPLUMBCMD_ALARM_BUTTON = 4,
	EPLUMBCMD_ALARM_OPTO_BUTTON = 5,
	EPLUMBCMD_GO_OPENED = 6,
} EPLUMBCMD;

typedef struct
{
	EPLUMBSTATES nowState;
	unsigned char sendOK;
	uint64_t transmitOK;
	uint64_t transmitFAILED;
	EPLUMBCMD cmd;
	char lastError;
} PLUMB_STATE;

extern PLUMB_STATE plState;


static uint16_t crc16_table[] = {
	0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
	0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
	0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
	0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
	0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
	0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
	0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
	0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
	0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
	0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
	0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
	0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
	0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
	0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
	0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
	0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
	0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
	0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
	0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
	0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
	0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
	0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
	0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
	0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
	0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
	0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
	0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
	0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
	0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
	0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
	0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
	0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78,
};

static uint16_t crc16(unsigned char *buf, unsigned int lenin)
{
	unsigned int len = lenin;
	uint16_t crc = 0xffff;

	while (len--) crc = (crc >> 8) ^ crc16_table[(crc & 0xFF) ^ *buf++];

	//reverse byte order
	crc = (crc << 8) | ((crc >> 8) & 0xff);
	return ~crc;//xor 0xffff
}

class SKZI: public AbstractHWTest
{
	master_ctx_t master_ctx;
	struct sc_proto_ctx_t *slave_ctx = NULL;

/*
	static uint8_t form_command(uint8_t command, uint8_t read, uint8_t continios,
			uint8_t addr_command);
	uint8_t GetReg(uint8_t reg);
	uint64_t Get8Regs();
	void SetReg(uint8_t reg, uint8_t val);
	void SendCommand(uint8_t command);
*/
	bool SKZI_ON(int timeout)
	{
		int timeWait = timeout;
		HAL_GPIO_WritePin(SCM_PWR_ON_GPIO_Port, SCM_PWR_ON_Pin, GPIO_PIN_SET);
		osDelay(500);
		HAL_GPIO_WritePin(SCM_OFF_GPIO_Port, SCM_OFF_Pin, GPIO_PIN_SET);
		if (timeWait < MIN_START_TIME_MS)
			timeWait = MIN_START_TIME_MS;
		if (timeWait > MAX_START_TIME_MS)
			timeWait = MAX_START_TIME_MS;

		while (timeWait > 0)
		{
			if (hUsbHostFS.pActiveClass->pData != NULL)
			{
				SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) hUsbHostFS.pActiveClass->pData;
				if (SKZI_Handle->state == SKZI_IDLE_STATE)
					break;
			}
			osDelay(STARTING_QUANT_TIME_MS);
			timeWait -= STARTING_QUANT_TIME_MS;
		}
		if (hUsbHostFS.pActiveClass->pData == NULL)
			return false;
		else
			return true;
	}
	void SKZI_OFF()
	{
		HAL_GPIO_WritePin(SCM_OFF_GPIO_Port, SCM_OFF_Pin, GPIO_PIN_RESET);
		osDelay(SKZI_POWER_OFF_TIME_MS);
		HAL_GPIO_WritePin(SCM_PWR_ON_GPIO_Port, SCM_PWR_ON_Pin, GPIO_PIN_RESET);
	}



	//**********************************************************************************
	//******************************  USB realization  *********************************
	//**********************************************************************************

	int usb_sc_proto_test_send_cmd_packet(struct sc_proto_ctx_t *ctx, char *buf, int size)
	{
		memset(&recvBufUartSKZI, 0, RX_BUF_SKZI_SIZE);
		skzi_RX_buf = (uint8_t *)&recvBufUartSKZI;
		skzi_RX_buf_cnt = 0;
		skzi_EOT = 0;
		//&huartSKZIPort, buf, size
		codeUSB = 0;
		volatile char finishUSB = 0;
		volatile int sendTryCnt = 0;
		volatile int ctrUSB = 0;
		/*
		SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) hUsbHostFS.pActiveClass->pData;
		if ((SKZI_Handle->data_tx_state != SKZI_IDLE) || (SKZI_Handle->data_rx_state != SKZI_IDLE))
		{
			//asm("  bkpt");
		}
		*/

		USBH_StatusTypeDef res;
		sendTryCnt = 0;
		while ((codeUSB != 1) && (sendTryCnt < 5))
		{
			ctrUSB = 0;
			res = USBH_SKZI_Transmit(&hUsbHostFS, (uint8_t *)buf, size);
			//if (res == USBH_OK)
			{
				while ((codeUSB != 1) && (ctrUSB < 1000))
				{
					ctrUSB++;
					osDelay(1);
				}
			}
			sendTryCnt++;
		}

		if (codeUSB == 1)
		{
			osDelay(1000);
			finishUSB = 0;
			sendTryCnt = 0;
			while ((!finishUSB) && (sendTryCnt < 62))
			{
				ctrUSB = 0;
				codeUSB = 1;
				res = USBH_SKZI_Receive(&hUsbHostFS, skzi_RX_buf, 64/*RX_BUF_SKZI_SIZE*/);
				if (res == USBH_OK)
				{
					while ((codeUSB != 2) && (ctrUSB < 1000))
					{
						ctrUSB++;
						osDelay(1);
					}
					if (codeUSB == 2)
					{
						SKZI_HandleTypeDef *SKZI_Handle = (SKZI_HandleTypeDef *) hUsbHostFS.pActiveClass->pData;
						if (SKZI_Handle->RxDataLength < 64)
						{
							skzi_RX_buf_cnt += (64 - SKZI_Handle->RxDataLength);
							skzi_RX_buf += (64 - SKZI_Handle->RxDataLength);
							if (SKZI_Handle->RxDataLength != 0)
								finishUSB = 1;
						}
						else
							finishUSB = 1;
					}
					else
						finishUSB = 1;
				}
				else
					finishUSB = 1;
				//osDelay(500);
				sendTryCnt++;
			}
		}

		if (skzi_RX_buf_cnt > 0)
			return size;
		else
		{
			PRINT_ERROR("-=SKZI=- USB Receive error\r\n");
			plState.lastError = 1;
		}
		return -1;
	}

	int sc_proto_test_receive_responce_packet(struct sc_proto_ctx_t *ctx, char *buf, int buf_size, int *delay_ms)
	{
		int bfsize = 0;
		bfsize = skzi_RX_buf_cnt;
		if (bfsize > 0)
		{
			memcpy(buf, &recvBufUartSKZI, bfsize);
			return bfsize;
		}
		return SCM_REPLY_WAIT;
	}

	int sc_proto_packet_param_get_value(int param_type, char *buf, int param_size,
		int expected_param_type, int expected_param_size, int max_param_size, void *val, int *real_param_size)
	{
		//if (param_type == 2)
		{
			memcpy(val, buf, param_size);
			*real_param_size = param_size;
			return 0;
		}
		return -1;
	}

	int simple_responce_callback(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, intptr_t arg,
		int error, int error_responce, char *data, int size)
	{
		int ret = 0;
		if (error) {
			master_ctx->error = 1;
		} else {
			//We assume here that responce is always small enough, so we just
			//keep reponce data in master_ctx. Otherwise we must provide special
			//callbacks for commands that can return large data amount (see receive_file_callback below) !

			if (master_ctx->receive_real_size + size > master_ctx->receive_max_size) {
				master_ctx->receive_max_size = master_ctx->receive_real_size + size;
				//master_ctx->receive_buf = realloc(master_ctx->receive_buf, master_ctx->receive_max_size);//WARNING: unsafe
				if (master_ctx->receive_buf == NULL)
					master_ctx->receive_buf = (char *)&staticMasterCtxBuffer;
				if (master_ctx->receive_max_size > STATIC_MASTER_CTX_BUF_SKZI_SIZE)
					master_ctx->receive_buf = NULL;
				if (master_ctx->receive_buf == NULL)
				{
					asm("  bkpt");
				}
			}
			memcpy(master_ctx->receive_buf + master_ctx->receive_real_size, data, size);
			master_ctx->receive_real_size += size;
		}
		return ret;
	}

	//**********************************************************************************
	//*******************************  Building Cmd  ***********************************
	//**********************************************************************************

	void print_hex(const char *prefix, const char *suffix, unsigned char *buf, int size)
	{
		if (prefix) usb_debug_cpp("%s", prefix);
		while (size-- > 0) usb_debug_cpp("%.02X ", (unsigned)*buf++);//optimization required
		if (suffix) usb_debug_cpp("%s", suffix);
	}

	void clear_cmd(master_ctx_t *master_ctx)
	{
		master_ctx->cmd = -1;
		master_ctx->expected_responce = -1;
		master_ctx->param_count = 0;
		master_ctx->receive_real_size = 0;
		master_ctx->error = 0;
		master_ctx->responce_param_count = 0;
	}

	int add_cmd_param(master_ctx_t *master_ctx, int param_type, int param_size, const void *buf)
	{
		cmd_params_t *param;

		if (master_ctx->param_count >= ARRAY_SIZE(master_ctx->params)) return -1;

		param = &master_ctx->params[master_ctx->param_count];
		param->param_type = param_type;
		param->param_size = param_size;
		if (param->buf_size < param_size) {
			if (param->buf != NULL)
				vPortFree(param->buf);
			param->buf = (char *)pvPortMalloc(param_size);
			param->buf_size = param_size;
		}
		if (param_size > 0) memcpy(param->buf, buf, param_size);

		master_ctx->param_count++;

		return 0;
	}

	void clear_cmd_params(master_ctx_t *master_ctx)
	{
		cmd_params_t *param = NULL;
		if (master_ctx->param_count > 0)
		{
			for (unsigned int i=0; i<master_ctx->param_count; i++)
			{
				param = &master_ctx->params[i];
				if (param->buf != NULL)
				{
					vPortFree(param->buf);
					param->buf = NULL;
					param->buf_size = 0;
				}
			}
		}
	}

	//buf - is packet payload (without header)
	int extract_responce_params(master_ctx_t *master_ctx, char *buf, unsigned int size)
	{
		cmd_params_t *param;
		scm_packet_param_header_t *header;
		int cur_size;

		while (size > 0) {
			if (master_ctx->responce_param_count > 0) { //check is last param processed
				param = &master_ctx->responce_params[master_ctx->responce_param_count - 1];
				if (param->param_parsed < param->param_size) { //next part of param data
					cur_size = MIN(param->param_size - param->param_parsed, size);
					param->buf = buf;
					param->buf_size = cur_size;
					param->param_parsed += cur_size;

					buf += cur_size;
					size -= cur_size;
					continue;
				}
			}
			//new param
			if (master_ctx->responce_param_count >= ARRAY_SIZE(master_ctx->responce_params)) return -1;
			if (size < sizeof(scm_packet_param_header_t)) return -1;//we always expect whole header in packet
			header = (scm_packet_param_header_t *)buf;
			buf += sizeof(*header);
			size -= sizeof(*header);
			param = &master_ctx->responce_params[master_ctx->responce_param_count];
			memset(param, 0, sizeof(*param));
			param->param_type = header->tag;
			param->param_size = header->size;
			param->buf = buf;
			cur_size = MIN(param->param_size, size);
			param->buf_size = cur_size;
			param->param_parsed = cur_size;

			master_ctx->responce_param_count++;

			buf += cur_size;
			size -= cur_size;
		}
		return 0;
	}

	//packet_offset - in payload !
	//rest_size must be >= sizeof(scm_packet_param_header_t)
	//return size of written data
	int append_cmd_param_to_packet_raw(int param_type, int param_size, char *param_buf, int param_offset,
		char *packet_buf, int packet_offset, int rest_size, int need_header)
	{
		int ret;
		packet_buf += (sizeof(scm_packet_cmd_header_t) + packet_offset);//current packet buffer offset
		if (need_header) {
			scm_packet_param_header_t *header = (scm_packet_param_header_t *)packet_buf;
			header->tag = param_type;
			header->size = param_size;
			packet_buf += sizeof(scm_packet_param_header_t);
			rest_size -= sizeof(scm_packet_param_header_t);
		}
		ret = MIN(param_size - param_offset, rest_size);
		if (ret > 0) memcpy(packet_buf, param_buf + param_offset, ret);
			else ret = 0;
		return ret + (need_header ? sizeof(scm_packet_param_header_t) : 0);
	}

	//packet_offset - in payload !
	//rest_size must be >= sizeof(scm_packet_param_header_t)
	//return size of written data
	int append_cmd_param_to_packet(cmd_params_t *param, int param_offset, char *packet_buf, int packet_offset, int rest_size, int need_header)
	{
		return append_cmd_param_to_packet_raw(param->param_type, param->param_size, param->buf, param_offset,
			packet_buf, packet_offset, rest_size, need_header);
	}

	void finish_cmd_packet(char *packet_buf, int packet_size, int cmd, int first_packet, int need_continue)
	{
		scm_packet_cmd_header_t *cmd_header = (scm_packet_cmd_header_t *)packet_buf;

		memset(cmd_header, 0, sizeof(*cmd_header));
		cmd_header->proto_tag = SCM_PROTO_TAG;
		if (first_packet) cmd_header->cmd = cmd;
			else cmd_header->cmd = SCM_CMD_CONTINUE;
		cmd_header->flags = SCM_PACKET_FLAG_CMD;
		if (need_continue) cmd_header->flags |= SCM_PACKET_FLAG_CONTINUE;
		cmd_header->size = packet_size - sizeof(scm_packet_cmd_header_t);
		cmd_header->data_crc = crc16((unsigned char *)packet_buf + sizeof(scm_packet_cmd_header_t), cmd_header->size);
		cmd_header->header_crc = crc16((unsigned char *)packet_buf, sizeof(scm_packet_cmd_header_t));
	}

	//return 0 if OK
	int check_responce_packet(char *packet_buf, unsigned int packet_size)
	{
		scm_packet_reply_header_t *responce_header = (scm_packet_reply_header_t *)packet_buf;
		int param_size = packet_size - sizeof(scm_packet_reply_header_t);
		scm_packet_crc_t crc, real_crc;

		if (packet_size < sizeof(scm_packet_reply_header_t)) return 1;

		if (responce_header->proto_tag != SCM_PROTO_TAG) return 1;

		if (!(responce_header->flags & SCM_PACKET_FLAG_REPLY)) return 1;
		if (responce_header->flags & SCM_PACKET_FLAG_CMD) return 1;

		if (responce_header->size != param_size) return 1;

		crc = crc16((unsigned char *)packet_buf + sizeof(scm_packet_reply_header_t), param_size);
		if (crc != responce_header->data_crc) return 1;

		crc = responce_header->header_crc;
		responce_header->header_crc = 0;
		real_crc = crc16((unsigned char *)responce_header, sizeof(*responce_header));
		if (crc != real_crc) return 1;

		return 0;
	}

	//error responce is usually parsed for debug/logging purposes
	void process_error_responce(master_ctx_t *master_ctx, char *buf, int size)
	{
		int ret;
		master_ctx->responce_param_count = 0;//drop all other params if any ?
		ret = extract_responce_params(master_ctx, buf, size);
		if (ret) {
			PRINT_ERROR("invalid error responce");
		} else { //expected error code and (optionally) error message
			if (0 == master_ctx->responce_param_count || 2 < master_ctx->responce_param_count) {
				PRINT_ERROR("invalid error responce");
			} else {
				int error = 0;
				char msg[128];
				cmd_params_t *param;
				param = &master_ctx->responce_params[0];
				ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
					SCM_DATA_ERROR, -1, -1, &error, NULL);
				if (ret) {
					PRINT_ERROR("invalid error responce");
				} else {
					msg[0] = 0;
					if (2 == master_ctx->responce_param_count) {
						param = &master_ctx->responce_params[1];
						ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
							SCM_DATA_STRING, -1, sizeof(msg) - 1, msg, NULL);
						if (ret) {
							PRINT_ERROR("invalid error responce");
						}
					}
					if (msg[0]) {
						PRINT_INFO("error responce received, error code %d, message \"%s\"", error, msg);
					} else {
						PRINT_INFO("error responce received, error code %d", error);
					}
				}
			}
		}
	}

	void build_empty_cmd(char *packet_buf, int cmd)
	{
		finish_cmd_packet(packet_buf, sizeof(scm_packet_cmd_header_t), cmd, 1, 0);
	}

	//return nonzero if timeout occured
	int check_timeout(uint32_t start_time, uint32_t timeout_ms)
	{
		uint32_t cur_time = stopwatch_getticks();
		uint32_t delimter = HAL_RCC_GetSysClockFreq();
		uint32_t diferenc = ((uint64_t)(cur_time - start_time) * 1000 / delimter);
		if (diferenc < timeout_ms)
			return 0;
		else
			return -1;
	}

	//**********************************************************************************
	//*********************************  Send cmd  *************************************
	//**********************************************************************************

	int send_packet(struct sc_proto_ctx_t *slave_ctx, char *packet_buf)
	{
		int ret;
		scm_packet_cmd_header_t *cmd_header = (scm_packet_cmd_header_t *)packet_buf;

		ret = usb_sc_proto_test_send_cmd_packet(slave_ctx, packet_buf, sizeof(scm_packet_cmd_header_t) + cmd_header->size);
		if (ret < 0) return ret;
		PRINT_DEBUG("sent packet size %d (payload size %d)", (int)sizeof(scm_packet_cmd_header_t) + cmd_header->size, cmd_header->size);
	//	print_hex("******* SENT PACKET:     ", "\n", (unsigned char *)packet_buf, MIN(ret, 32));
		print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)packet_buf, MIN(ret, MAX_DEBUG_HEX_LEN));
		return ret;
	}

	//Current command and it's parameters already must be in master_ctx. If some commands
	//send large (or undefined) amount of data, then we can use special cmd_callback
	//to fill packet payload (see send_file_callback).
	//responce_callback is used to process data received from slave. We can either
	//store data in some buffer (and parse them after all data received) or process
	//data on the fly (see receive_file_callback).
	int send_cmd(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx/*,
		cmd_callback_t cmd_callback, intptr_t cmd_callback_arg,
		responce_callback_t responce_callback, intptr_t responce_callback_arg*/)
	{
		int ret = 0;
		int first_packet = 1;
		unsigned int cur_size;
		unsigned int rest_size;
		int received_size;
	//	scm_packet_cmd_header_t *cmd_header = (scm_packet_cmd_header_t *)packet_buf;
		scm_packet_reply_header_t *responce_header = (scm_packet_reply_header_t *)packet_buf;
		int error = 0;
		int is_first_param = 1;
		unsigned int cur_param = 0;
		unsigned int cur_param_used = 0;
		int need_continue = 0;
		int need_header;
		int error_responce = 0;
		/*struct timeval */ uint32_t cmd_time, interval_time;
		int delay_ms = master_ctx->delay_ms;
		int total_payload_sent = 0;

		master_ctx->receive_real_size = 0;
		//split command data (if required) and send packets
		do {
			need_continue = 0;
			cur_size = 0;

			/*if (cmd_callback) { //use callback to fill packet payload
				cur_size = cmd_callback(master_ctx, slave_ctx, cmd_callback_arg, packet_buf, total_payload_sent, &need_continue);
				if (cur_size < 0) DO_ERROR(cur_size, "cannot fill packet payload");
			} else*/ { //use parameters from master_ctx->params
				if (master_ctx->param_count > 0) { //pack parameters to packet
					rest_size = SC_PROTO_MAX_CMD_PAYLOAD_SIZE;
					need_continue = 1;
					need_header = 0;
					while (cur_size < SC_PROTO_MAX_CMD_PAYLOAD_SIZE) {
						if (master_ctx->params[cur_param].param_size <= cur_param_used) { //next param
							cur_param++;
							cur_param_used = 0;
							need_header = 1;
							if (master_ctx->param_count <= cur_param) {
								need_continue = 0;//no more params
								break;
							}
							if (rest_size < sizeof(scm_packet_param_header_t)) { //put new param to the next packet
								need_continue = 1;
								break;
							}
						} else {
							if (is_first_param) need_header = 1;
						}
						ret = append_cmd_param_to_packet(&master_ctx->params[cur_param], cur_param_used, packet_buf,
							cur_size, rest_size, need_header);
						if (need_header) cur_param_used += (ret - sizeof(scm_packet_param_header_t));
							else cur_param_used += ret;
						cur_size += ret;
						rest_size -= ret;
						need_header = 0;
						is_first_param = 0;
					}
					//special case: last param fits exactly to packet size (no more data)
					if (SC_PROTO_MAX_CMD_PAYLOAD_SIZE == cur_size && (master_ctx->param_count - 1) == cur_param &&
						master_ctx->params[cur_param].param_size <= cur_param_used) {
						need_continue = 0;
					}
				}
			}

			//fill cmd_header
			finish_cmd_packet(packet_buf, sizeof(scm_packet_cmd_header_t) + cur_size, master_ctx->cmd, first_packet, need_continue);

			first_packet = 0;
			//send command packet
			ret = send_packet(slave_ctx, packet_buf);
			if (ret < 0) DO_ERROR(ret, "cannot send packet");
			total_payload_sent += cur_size;

			cmd_time = stopwatch_getticks();
			//gettimeofday(&cmd_time, NULL);//keep start time (to detect timeout)
			interval_time = cmd_time;
			while (1) { //receive responce packet
				ret = sc_proto_test_receive_responce_packet(slave_ctx, packet_buf, sizeof(packet_buf), &delay_ms);
				if (ret < 0) DO_ERROR(ret, "cannot receive packet");
				received_size = ret;
	//			print_hex("******* RECEIVED PACKET: ", "\n", (unsigned char *)packet_buf, MIN(ret, 32));
				print_hex("DEBUG: RECEIVED PACKET: ", "\r\n", (unsigned char *)packet_buf, MIN(ret, MAX_DEBUG_HEX_LEN));
				PRINT_DEBUG("received packet size %d (payload size %d)", ret, ret - (int)sizeof(*responce_header));

				//process responce
				error = check_responce_packet(packet_buf, received_size);
				//first process special responces (ERROR, WAIT), also check timeouts
				if (!error && SCM_REPLY_ERROR == responce_header->reply) { //responce with error code received
					error_responce = 1;
					master_ctx->receive_real_size = 0;//drop previous data in receiving buffer ???
				} else {
					//check hard timeout
					//NOTE: timeout detecting/processing here is just a simple emulation,
					//      real code must use transport timeouts, interrupts, signals ...
					ret = check_timeout(cmd_time, master_ctx->timeout_ms);
					if (ret) { //fatal error ?
						DO_ERROR(ret, "hard timeout (%d), slave hangs ?", master_ctx->timeout_ms);
					}

					//check soft timeout
					ret = check_timeout(interval_time, master_ctx->responce_interval_ms);
					if (ret) { //just example: send ABORT (is it really usefull ? can depends on command ?)
						PRINT_WARN("soft timeout (%d), sending ABORT", master_ctx->responce_interval_ms);
						build_empty_cmd(packet_buf, SCM_CMD_ABORT);
						ret = send_packet(slave_ctx, packet_buf);
						if (ret < 0) DO_ERROR(ret, "cannot send packet");
						continue;
					}

					if (SCM_REPLY_WAIT == responce_header->reply) { //no timeout, send CONTINUE
						PRINT_INFO("WAIT responce received, sending CONTINUE");
						build_empty_cmd(packet_buf, SCM_CMD_CONTINUE);
						ret = send_packet(slave_ctx, packet_buf);
						if (ret < 0) DO_ERROR(ret, "cannot send packet");
						interval_time = stopwatch_getticks();
						//gettimeofday(&interval_time, NULL);//update soft timeout
						continue;
					}

					//check responce code (must correspond command code)
					if (master_ctx->expected_responce != responce_header->reply) {
						error = 1;
						PRINT_ERROR("unexpected responce code: %d != %d", master_ctx->expected_responce, (int)responce_header->reply);
					}
					cmd_time = stopwatch_getticks();
					//gettimeofday(&cmd_time, NULL);//update soft timeout
					interval_time = cmd_time;
				}
#if 0
				ret = responce_callback(master_ctx, slave_ctx, responce_callback_arg, error, error_responce,
					packet_buf + sizeof(*responce_header), received_size - sizeof(*responce_header));
#else
				ret = simple_responce_callback(master_ctx, slave_ctx, 0, error, error_responce,
						packet_buf + sizeof(*responce_header), received_size - sizeof(*responce_header));
#endif
				if (ret < 0) goto on_done;
				if (error) goto on_done;//???
				if (error_responce) goto on_done;
				if (SCM_PACKET_FLAG_CONTINUE & responce_header->flags) { //slave has more data, send CONTINUE
					build_empty_cmd(packet_buf, SCM_CMD_CONTINUE);
					ret = send_packet(slave_ctx, packet_buf);
					if (ret < 0) DO_ERROR(ret, "cannot send packet");
					continue;
				}
				break;
			}
	#if 0
			if (error) {
				ret = -1;
				break;//????????????????
			}
	#endif
	//	} while (master_ctx->param_count > cur_param);
		} while (need_continue);

	on_done:
		if (error) {
			PRINT_ERROR("stop by error");
			return -1;
		}
		if (error_responce) {
			process_error_responce(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
			PRINT_ERROR("stop by error responce");
			return -1;
		}

		return ret;
	}

	//**********************************************************************************
	//***********************************  Ping  ***************************************
	//**********************************************************************************

	int build_cmd_PING(master_ctx_t *master_ctx, uint32_t ping_data)
	{
		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_PING;
		master_ctx->expected_responce = SCM_REPLY_PING;
		return add_cmd_param(master_ctx, SCM_DATA_BINARY, sizeof(ping_data), &ping_data);
	}

	int do_ping(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx)
	{
		int ret;
		uint32_t ping_data = 0x01020304;
		uint32_t expected_responce = 0x04030201;//we must receive reverse byte order
		uint32_t responce_data;
		cmd_params_t *param;

		//prepare command
		build_cmd_PING(master_ctx, ping_data);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters (1 parameter expected)
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 != master_ctx->responce_param_count) DO_ERROR(-1, "invalid responce parameters count - %d", master_ctx->responce_param_count);
		//get returned value
		param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BINARY, sizeof(responce_data), -1, &responce_data, NULL);
		if (ret) DO_ERROR(-1, "invalid responce parameter");

		if (expected_responce != responce_data) {
			DO_ERROR(-1, "invalid responce data: %X != %X", (unsigned)responce_data, (unsigned)expected_responce);
		}
		PRINT_INFO("ping OK");
	on_done:
		return ret;
	}

	//**********************************************************************************
	//********************************  GEt Set Time ***********************************
	//**********************************************************************************

	int build_cmd_MODULE_GET_TIME(master_ctx_t *master_ctx)
	{
		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_MODULE_GET_TIME;
		master_ctx->expected_responce = SCM_REPLY_MODULE_GET_TIME;
		return 0;
	}

	int do_module_get_time(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, unitime *gm)
	{
		int ret;
		cmd_params_t *param;
		int tmp_int = 0;
		time_t tm;

		//prepare command
		build_cmd_MODULE_GET_TIME(master_ctx);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters (1 parameter expected)
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 != master_ctx->responce_param_count) DO_ERROR(-1, "invalid responce parameters count - %d", master_ctx->responce_param_count);
		//get returned value
		param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_TIME, -1, -1, &tmp_int, NULL);
		if (ret) DO_ERROR(-1, "invalid responce parameter");

		tm = tmp_int;
		convertTime(tm, +5, gm);//+5
		//volatile struct tm *gmt = gmtime(&tm);
		PRINT_INFO("module time (gm): %d.%.02d.%.04d  %.02d:%.02d:%.02d",	gm->tm_mday, gm->tm_mon, gm->tm_year, gm->tm_hour, gm->tm_min, gm->tm_sec);
	on_done:
		return ret;
	}

	int build_cmd_SET_TIME(master_ctx_t *master_ctx, time_t tm)
	{
		scm_data_time_t time = tm;
		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_SET_TIME;
		master_ctx->expected_responce = SCM_REPLY_SET_TIME;
		return add_cmd_param(master_ctx, SCM_DATA_TIME, sizeof(time), &time);
	}

	int do_set_time(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, time_t tm)
	{
		int ret;
		struct tm *gm;

		//prepare command
		build_cmd_SET_TIME(master_ctx, tm);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters (1 parameter expected)
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (0 != master_ctx->responce_param_count) DO_ERROR(-1, "invalid responce parameters count - %d", master_ctx->responce_param_count);

		gm = gmtime(&tm);
		PRINT_INFO("set module time to: %d.%.02d.%.04d  %.02d:%.02d:%.02d",
			gm->tm_mday, gm->tm_mon + 1, gm->tm_year + 1900, gm->tm_hour, gm->tm_min, gm->tm_sec);
	on_done:
		return ret;
	}

	bool get_time_SKZI(unitime *utime)
	{
		memset(&master_ctx, 0, sizeof(master_ctx));
		master_ctx.timeout_ms = SCM_TIMEOUT_HANG;
		master_ctx.responce_interval_ms = SCM_TIMEOUT_REPLY;

		int ret = do_module_get_time(&master_ctx, slave_ctx, utime);
		if (ret < 0) DO_ERROR(0, "exit");

		if (utime->tm_year < 2021)
		{
			time_t tm = 0;
			utime->tm_year = 2021;
			utime->tm_mon = 4;
			utime->tm_mday = 25;
			utime->tm_hour = 15;
			utime->tm_min = 20;
			utime->tm_sec = 21;
			convertTimeBack(utime, +5, &tm);//+5

			ret = do_set_time(&master_ctx, slave_ctx, tm);
			//if (ret < 0) DO_ERROR(0, "exit");

			int ret = do_module_get_time(&master_ctx, slave_ctx, utime);
			if (ret < 0) DO_ERROR(0, "exit");
		}

		return true;
		on_done:
		return false;
	}

	//**********************************************************************************
	//********************************  Module info  ***********************************
	//**********************************************************************************

	int build_cmd_MODULE_ID(master_ctx_t *master_ctx)
	{
		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_MODULE_ID;
		master_ctx->expected_responce = SCM_REPLY_MODULE_ID;
		return 0;
	}

	int do_module_id(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx)
	{
		int ret;
		cmd_params_t *param;
		char responce_data[64];
		int real_size = 0;

		//prepare command
		build_cmd_MODULE_ID(master_ctx);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters (1 parameter expected)
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 != master_ctx->responce_param_count) DO_ERROR(-1, "invalid responce parameters count - %d", master_ctx->responce_param_count);
		//get returned value
		param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BINARY, -1, sizeof(responce_data) - 1, responce_data, &real_size);
		if (ret) DO_ERROR(-1, "invalid responce parameter");

		responce_data[real_size] = 0;

		PRINT_INFO("module ID: %s", responce_data);
	on_done:
		return ret;
	}

	//**********************************************************************************
	//**********************************************************************************
	//**********************************************************************************

	bool test_SKZI()
	{

		int ret = do_module_id(&master_ctx, slave_ctx);
		osDelay(50);
		if (ret < 0) DO_ERROR(0, "exit");

		ret = do_ping(&master_ctx, slave_ctx);
		osDelay(50);
		if (ret < 0) DO_ERROR(0, "exit");

		return true;
		on_done:
		return false;
	}


	//**********************************************************************************

	//**********************************************************************************
	//************************************  TLS  ***************************************
	//**********************************************************************************

	int build_cmd_TLS_INIT(master_ctx_t *master_ctx, int provider, int server, const char *server_address)
	{
		int ret;
		scm_data_byte_t tmp_byte;
		scm_data_alg_id_t tmp_alg_id;
		scm_data_key_id_t tmp_key_id;

		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_TLS_INIT;
		master_ctx->expected_responce = SCM_REPLY_TLS_INIT;

		//transport
		tmp_byte = SCM_CONNECTION_TRANSPORT_UNKNOWN;
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//protocol
	//	tmp_byte = SCM_CONNECTION_PROTOCOL_TLS_1_0 | SCM_CONNECTION_PROTOCOL_TLS_1_1 | SCM_CONNECTION_PROTOCOL_TLS_1_2;
		tmp_byte = SCM_CONNECTION_PROTOCOL_TLS_1_2;
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//server or client
		tmp_byte = server ? SCM_CONNECTION_SERVER : SCM_CONNECTION_CLIENT;
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//auth type and auth params define policy while TLS negotiating.
		//  if we select SCM_TLS_VERIFY_ALWAYS then we always request certificate of remote side and verify it,
		//  if SCM_TLS_VERIFY_NONE then we do not verify remote certificate,
		//  if SCM_TLS_VERIFY_IF_PRESENT then we will verify remote certificate if remote side provides it.
		//if we verify certificate then the whole trusted certificate chain must be installed to module
		//  (intermediate CA to CA store, root CA to ROOT store).

		//auth type
	#ifdef USE_TLS_SERVER
		tmp_byte = SCM_TLS_VERIFY_NONE;//change to ALWAYS if sure that server's certificate is trusted
	#else
		tmp_byte = SCM_TLS_VERIFY_ALWAYS;
	#endif
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//auth params
		tmp_byte = SCM_TLS_FLAG_MUTUAL_AUTH;//check certificates whether we are client or server (often only client verifies server certificate)
		tmp_byte |= SCM_TLS_FLAG_IGNORE_REVOCATION;//do not use CRL (if no CRL is installed to module)
		//tmp_byte |= SCM_TLS_FLAG_IGNORE_UNKNOWN_CA;//allow unknown CA (for tests only)
		tmp_byte |= SCM_TLS_FLAG_IGNORE_CERT_CN_INVALID;//do not check that remote address matches remote certitifcate name.
								//we disable this check for test purpose only (usually server certificate names
								//must match server domain address).
		if (!server) tmp_byte |= SCM_TLS_FLAG_IGNORE_WRONG_USAGE;//if server's certificate has wrong usages
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//provider
		tmp_alg_id = provider;
		ret = add_cmd_param(master_ctx, SCM_DATA_ALG_ID, sizeof(tmp_alg_id), &tmp_alg_id);
		if (ret) return ret;

		//key id (optional)
		tmp_key_id.usage = SCM_KEY_USAGE_TLS;
		tmp_key_id.number = 0;
		ret = add_cmd_param(master_ctx, SCM_DATA_KEY_ID, sizeof(tmp_key_id), &tmp_key_id);
		if (ret) return ret;

	#ifdef USE_TLS_SERVER
		//remote address (if server requires), usually it is the server domain name (see also about SCM_TLS_FLAG_IGNORE_CERT_CN_INVALID above)
		if (server_address) {
			ret = add_cmd_param(master_ctx, SCM_DATA_STRING, strlen(server_address), server_address);
			if (ret) return ret;
		}
	#endif

		return ret;
	}

	int do_tls_init(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, int provider, int server, const char *server_address,
		int *tls_handle, char **out_buf, int *out_size)
	{
		int ret;
		cmd_params_t *param;
		unsigned int i;

		//prepare command
		build_cmd_TLS_INIT(master_ctx, provider, server, server_address);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 > master_ctx->responce_param_count || master_ctx->responce_param_count > 2) DO_ERROR(-1, "invalid responce parameter");

		i = 0; param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_HANDLE, -1, -1, tls_handle, NULL);
		if (ret) DO_ERROR(ret, "invalid responce parameter");

		*out_buf = NULL;
		*out_size = 0;

		i++; param++;
		if (i < master_ctx->responce_param_count && param->param_size > 0) {//data
			*out_buf = (char *)pvPortMalloc(param->param_size);
			if (!*out_buf) DO_ERROR(-1, "cannot allocate buffer");
			ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
				SCM_DATA_BINARY, -1, param->param_size, *out_buf, NULL);
			if (ret) {
				vPortFree(*out_buf);
				*out_buf = NULL;
				DO_ERROR(ret, "invalid responce parameter");
			}
			*out_size = param->param_size;
		}
		clear_cmd_params(master_ctx);

	on_done:
		clear_cmd_params(master_ctx);
		return ret;
	}

	int build_cmd_TLS_SETUP(master_ctx_t *master_ctx, int tls_handle, char *data, int data_size)
	{
		int ret;
		scm_data_handle_t tmp_handle;

		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_TLS_SETUP;
		master_ctx->expected_responce = SCM_REPLY_TLS_SETUP;

		//handle
		tmp_handle = tls_handle;
		ret = add_cmd_param(master_ctx, SCM_DATA_HANDLE, sizeof(tmp_handle), &tmp_handle);
		if (ret) return ret;

		if (data && data_size > 0) {
			//data
			ret = add_cmd_param(master_ctx, SCM_DATA_BINARY, data_size, data);
			if (ret) return ret;
		}

		return ret;
	}

	int do_tls_setup(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, int tls_handle, char *data, int data_size,
		char **out_buf, int *out_size, int *tls_state)
	{
		int ret;
		cmd_params_t *param;
		unsigned int i;

		//prepare command
		build_cmd_TLS_SETUP(master_ctx, tls_handle, data, data_size);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 > master_ctx->responce_param_count || master_ctx->responce_param_count > 2) DO_ERROR(-1, "invalid responce parameter");

		i = 0; param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BYTE, -1, -1, tls_state, NULL);
		if (ret) DO_ERROR(ret, "invalid responce parameter");

		*out_buf = NULL;
		*out_size = 0;

		i++; param++;
		if (i < master_ctx->responce_param_count && param->param_size > 0) {//data
			*out_buf = (char *)pvPortMalloc(param->param_size);
			if (!*out_buf) DO_ERROR(-1, "cannot allocate buffer");
			ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
				SCM_DATA_BINARY, -1, param->param_size, *out_buf, NULL);
			if (ret) {
				vPortFree(*out_buf);
				*out_buf = NULL;
				DO_ERROR(ret, "invalid responce parameter");
			}
			*out_size = param->param_size;
		}
		clear_cmd_params(master_ctx);

	on_done:
		clear_cmd_params(master_ctx);
		return ret;
	}

	int build_cmd_TLS_GET_REMOTE(master_ctx_t *master_ctx, int tls_handle)
	{
		int ret;
		scm_data_handle_t tmp_handle;

		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_TLS_GET_REMOTE;
		master_ctx->expected_responce = SCM_REPLY_TLS_GET_REMOTE;

		//handle
		tmp_handle = tls_handle;
		ret = add_cmd_param(master_ctx, SCM_DATA_HANDLE, sizeof(tmp_handle), &tmp_handle);
		if (ret) return ret;

		return ret;
	}

	int do_tls_get_remote(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, int tls_handle, char **cert_buf, int *cert_size, int *tls_state)
	{
		int ret;
		cmd_params_t *param;
		unsigned int i;

		//prepare command
		build_cmd_TLS_GET_REMOTE(master_ctx, tls_handle);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 > master_ctx->responce_param_count || master_ctx->responce_param_count > 3) DO_ERROR(-1, "invalid responce parameter");

		i = 0; param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BYTE, -1, -1, tls_state, NULL);
		if (ret) DO_ERROR(ret, "invalid responce parameter");

		*cert_buf = NULL;
		*cert_size = 0;

		i++; param++;
		if (i < master_ctx->responce_param_count && param->param_size > 0) {//cert
			*cert_buf = (char *)pvPortMalloc(param->param_size);
			if (!*cert_buf) DO_ERROR(-1, "cannot allocate buffer");
			ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
				SCM_DATA_CERT, -1, param->param_size, *cert_buf, NULL);
			if (ret) {
				vPortFree(*cert_buf);
				*cert_buf = NULL;
				DO_ERROR(ret, "invalid responce parameter");
			}
			*cert_size = param->param_size;
		}

		//TODO: role support ?
		clear_cmd_params(master_ctx);

	on_done:
		clear_cmd_params(master_ctx);
		return ret;
	}

	int build_cmd_TLS_CRYPT(master_ctx_t *master_ctx, int tls_handle, int encrypt, char *data, int data_size)
	{
		int ret;
		scm_data_handle_t tmp_handle;

		clear_cmd(master_ctx);
		if (encrypt) {
			master_ctx->cmd = SCM_CMD_TLS_ENCRYPT;
			master_ctx->expected_responce = SCM_REPLY_TLS_ENCRYPT;
		} else {
			master_ctx->cmd = SCM_CMD_TLS_DECRYPT;
			master_ctx->expected_responce = SCM_REPLY_TLS_DECRYPT;
		}

		//handle
		tmp_handle = tls_handle;
		ret = add_cmd_param(master_ctx, SCM_DATA_HANDLE, sizeof(tmp_handle), &tmp_handle);
		if (ret) return ret;

		//data
		ret = add_cmd_param(master_ctx, SCM_DATA_BINARY, data_size, data);
		if (ret) return ret;

		return ret;
	}

	int do_tls_crypt(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, int tls_handle, int encrypt, char *data, int data_size,
		char **out_buf, int *out_size, int *tls_state)
	{
		int ret;
		cmd_params_t *param;
		unsigned int i;

		//prepare command
		build_cmd_TLS_CRYPT(master_ctx, tls_handle, encrypt, data, data_size);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 > master_ctx->responce_param_count || master_ctx->responce_param_count > 2) DO_ERROR(-1, "invalid responce parameter");

		i = 0; param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BYTE, -1, -1, tls_state, NULL);
		if (ret) DO_ERROR(ret, "invalid responce parameter");

		*out_buf = NULL;
		*out_size = 0;

		i++; param++;
		if (i < master_ctx->responce_param_count && param->param_size > 0) {//data
			*out_buf = (char *)pvPortMalloc(param->param_size);
			if (!*out_buf) DO_ERROR(-1, "cannot allocate buffer");
			ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
				SCM_DATA_BINARY, -1, param->param_size, *out_buf, NULL);
			if (ret) {
				vPortFree(*out_buf);
				*out_buf = NULL;
				DO_ERROR(ret, "invalid responce parameter");
			}
			*out_size = param->param_size;
		}
		clear_cmd_params(master_ctx);

	on_done:
		clear_cmd_params(master_ctx);
		return ret;
	}

	int build_cmd_TLS_CLOSE(master_ctx_t *master_ctx, int tls_handle, int reason, char *data, int data_size)
	{
		int ret;
		scm_data_handle_t tmp_handle;
		scm_data_byte_t tmp_byte;

		clear_cmd(master_ctx);
		master_ctx->cmd = SCM_CMD_TLS_CLOSE;
		master_ctx->expected_responce = SCM_REPLY_TLS_CLOSE;

		//handle
		tmp_handle = tls_handle;
		ret = add_cmd_param(master_ctx, SCM_DATA_HANDLE, sizeof(tmp_handle), &tmp_handle);
		if (ret) return ret;

		//reason
		tmp_byte = reason;
		ret = add_cmd_param(master_ctx, SCM_DATA_BYTE, sizeof(tmp_byte), &tmp_byte);
		if (ret) return ret;

		//reason
		ret = add_cmd_param(master_ctx, SCM_DATA_BINARY, data_size, data);
		if (ret) return ret;

		return ret;
	}

	int do_tls_close(master_ctx_t *master_ctx, struct sc_proto_ctx_t *slave_ctx, int tls_handle, int reason, char *data, int data_size,
		char **out_buf, int *out_size, int *tls_state)
	{
		int ret;
		cmd_params_t *param;
		unsigned int i;

		//prepare command
		build_cmd_TLS_CLOSE(master_ctx, tls_handle, reason, data, data_size);

		//send command, receive responce
		PRINT_INFO("send command \"%s\"", sc_proto_cmd_name(master_ctx->cmd));
		ret = send_cmd(master_ctx, slave_ctx/*, NULL, 0, simple_responce_callback, 0*/);
		if (ret < 0) DO_ERROR(ret, "command \"%s\" failed", sc_proto_cmd_name(master_ctx->cmd));

		//process responce parameters
		ret = extract_responce_params(master_ctx, master_ctx->receive_buf, master_ctx->receive_real_size);
		if (ret < 0) DO_ERROR(ret, "invalid responce parameters");
		if (1 > master_ctx->responce_param_count || master_ctx->responce_param_count > 2) DO_ERROR(-1, "invalid responce parameter");

		i = 0; param = &master_ctx->responce_params[0];
		ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
			SCM_DATA_BYTE, -1, -1, tls_state, NULL);
		if (ret) DO_ERROR(ret, "invalid responce parameter");

		*out_buf = NULL;
		*out_size = 0;

		i++; param++;
		if (i < master_ctx->responce_param_count && param->param_size > 0) {//data
			*out_buf = (char *)pvPortMalloc(param->param_size);
			if (!*out_buf) DO_ERROR(-1, "cannot allocate buffer");
			ret = sc_proto_packet_param_get_value(param->param_type, param->buf, param->param_size,
				SCM_DATA_BINARY, -1, param->param_size, *out_buf, NULL);
			if (ret) {
				vPortFree(*out_buf);
				*out_buf = NULL;
				DO_ERROR(ret, "invalid responce parameter");
			}
			*out_size = param->param_size;
		}
		clear_cmd_params(master_ctx);

	on_done:
		clear_cmd_params(master_ctx);
		return ret;
	}


	//**********************************************************************************




#if 0
	int connect_SKZI()
	{
		int ret;
		memset(&master_ctx, 0, sizeof(master_ctx));
		master_ctx.timeout_ms = SCM_TIMEOUT_HANG;
		master_ctx.responce_interval_ms = SCM_TIMEOUT_REPLY;

		int transfer_count;
		clear_buf();
		PRINT_INFO("\r\nTLS");
		PRINT_INFO("------------ init TLS context");

		ret = GSM_modem_connect(&tls_server_socket, adr_to_connect, port_to_connect, timeout_to_connect);
		if (ret < 0) DO_ERROR(-1, "cannot connect to server");
	//while(1)osDelay(1000);
		tls_server_max_size = sizeof(tls_server_buf);

		ret = do_tls_init(&master_ctx, slave_ctx, provider, 0, tls_server_name, &tls_client_handle, &tls_client_buf, &tls_client_size);
		if (ret < 0) DO_ERROR(0, "exit");

		PRINT_INFO("------------ start TLS handshake");
		while (
			SCM_CONNECTION_STATUS_CONNECTED != tls_client_state) {
			transfer_count = 0;
			if (tls_client_size > 0) { //send data to server
				PRINT_INFO("send %d bytes to server", tls_client_size);
				print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)tls_client_buf, MIN(tls_client_size, MAX_DEBUG_HEX_LEN));
	#if 1
				ret = GSM_modem_send(tls_server_socket, tls_client_buf, tls_client_size, 60000);
				if (ret < 0) DO_ERROR(-1, "cannot send data to server");
				tls_server_size = GSM_modem_recv(tls_server_socket, tls_server_buf, tls_server_max_size, 0);
				if (tls_server_size <= 0) DO_ERROR(-1, "cannot receive data from server");
	#else
				tls_server_size = sizeof(bfsendback);
	#endif
				PRINT_INFO("received %d bytes from server", tls_server_size);
				transfer_count++;
				tls_client_size = 0;
				vPortFree(tls_client_buf);
				tls_client_buf = NULL;
			} else {
				//check if any data from server
				tls_server_size = GSM_modem_recv(tls_server_socket, tls_server_buf, tls_server_max_size, GSM_MODEM_WAIT_DATA_MS);
				if (tls_server_size <= 0) {
					if (tls_server_size < 0) {
						tls_server_size = 0;
					} else {
						DO_ERROR(-1, "cannot receive data from server");
					}
				} else {
					PRINT_INFO("received %d bytes from server", tls_server_size);
					transfer_count++;
				}
			}

			if (tls_server_size > 0) { //send data to client
	#if 0
				memcpy(&tls_server_buf, &bfsendback, sizeof(bfsendback));
				tls_server_size = sizeof(bfsendback);
				//tls_client_handle = 23;
	#endif
				//int tryesCount = 0;
				//ret = -1;
				//while ((ret < 0) && (tryesCount < 5))
				//{
					ret = do_tls_setup(&master_ctx, slave_ctx, tls_client_handle, tls_server_buf, tls_server_size,
							&tls_client_buf, &tls_client_size, &tls_client_state);
					//if (ret < 0)
					//	tryesCount++;
					//else
					//	break;
				//}
				if (ret < 0) DO_ERROR(0, "exit");
				if (SCM_CONNECTION_STATUS_ERROR == tls_client_state) DO_ERROR(-1, "TLS setup (client) failed");
				transfer_count++;
				tls_server_size = 0;
			}

			if (!transfer_count) DO_ERROR(-1, "no transfer");
		}
		PRINT_INFO("------------ TLS session established");

	#if 0
		//get server certificate (if no TLS server used then it's the same as client certificate)
		ret = do_tls_get_remote(&master_ctx, slave_ctx, tls_client_handle, &cert_buf, &cert_size, &tls_client_state);
		if (ret < 0) DO_ERROR(0, "exit");
		PRINT_INFO("------------ save server certificate to file: server.cer");
		//ret = save_file("server.cer", cert_buf, cert_size);
		//if (ret < 0) DO_ERROR(ret, "cannot save file");
		vPortFree(cert_buf);
		cert_buf = NULL;
	#endif
		return 1;
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		return -1;
	}

#endif

	const char *tls_server_name = "gost.infotecs.ru";
	const char *adr_to_connect = "10.77.60.14";
	int port_to_connect = 30197;
	int tls_client_size;
	int tls_client_handle = -1;
	char *tls_client_buf = NULL;
	int provider = SCM_CRYPTO_PROVIDER_TYPE_DEFAULT;
	int tls_server_size;//real data size in tls_server_buf
	int tls_client_state = SCM_CONNECTION_STATUS_NEED_MORE_DATA;

public:
	bool test()override
	{
		bool on = SKZI_ON(0);
		if (on == false)
		{
			SKZI_OFF();
			DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "POWER ON TIMEOUT");
			return false;
		}
		else
		{
			DBG::PRINT(LVL::INFO, "SKZI", "" , "POWER ON OK");
			on = test_SKZI();
			if (on == false)
			{
				SKZI_OFF();
				DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "CONECTION TIMEOUT");
				return false;
			}
			else
			{
				DBG::PRINT(LVL::INFO, "SKZI", "" , "CONECTION OK");
				on = get_time_SKZI(&info.gm);
				if (on == false)
				{
					SKZI_OFF();
					DBG::PRINT(LVL::EMCY, "SKZI", "" , "TEST FAIL", "GET SKZI TIME FAILED");
					return false;
				}
				else
				{
					SKZI_OFF();
					char strBuf[50];
					sprintf((char *)&strBuf, "%02d.%02d.%04d %02d:%02d:%02d", info.gm.tm_mday, info.gm.tm_mon, info.gm.tm_year, info.gm.tm_hour, info.gm.tm_min, info.gm.tm_sec);
					DBG::PRINT(LVL::INFO, "SKZI", "" , "SKZI TIME: ", (char *)&strBuf);
					return true;
				}
			}
		}
	}

	bool initSKZI();

	bool connectStage1(char **outBuf, unsigned int *outSize)
	{
		int ret = 0;
		PRINT_INFO("\r\nTLS");
		PRINT_INFO("------------ init TLS context");

		tls_server_max_size = sizeof(tls_server_buf);

		ret = do_tls_init(&master_ctx, slave_ctx, provider, 0, tls_server_name, &tls_client_handle, &tls_client_buf, &tls_client_size);
		if (ret < 0) DO_ERROR(0, "exit");

		PRINT_INFO("------------ start TLS handshake");
		if (tls_client_size > 0)
		{
			PRINT_INFO("send %d bytes to server", tls_client_size);
			print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)tls_client_buf, MIN(tls_client_size, MAX_DEBUG_HEX_LEN));
			*outBuf = tls_client_buf;
			*outSize = tls_client_size;
			return true;
		}
		else
		{
			*outBuf = NULL;
			*outSize = 0;
			return true;
		}
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		return false;

	}

	bool connectStage2(const char *inBuf, const unsigned int inSize, char **outBuf, unsigned int *outSize)
	{
		int ret = 0;
		tls_client_size = 0;
		if (tls_client_buf != NULL)
			vPortFree(tls_client_buf);
		tls_client_buf = NULL;

		tls_server_size = inSize;
		PRINT_INFO("received %d bytes from server", tls_server_size);
		if (tls_server_size <= 0)
		{
			if (tls_server_size < 0)
			{
				tls_server_size = 0;
			}
			else
			{
				DO_ERROR(-1, "cannot receive data from server");
			}
		}
		else
		{
			memcpy(tls_server_buf, inBuf, tls_server_size);
			PRINT_INFO("received %d bytes from server", tls_server_size);
		}

		if (tls_server_size > 0)
		{ //send data to client
			ret = do_tls_setup(&master_ctx, slave_ctx, tls_client_handle, tls_server_buf, tls_server_size,
						&tls_client_buf, &tls_client_size, &tls_client_state);
			if (ret < 0) DO_ERROR(0, "exit");
			if (SCM_CONNECTION_STATUS_ERROR == tls_client_state)
				DO_ERROR(-1, "TLS setup (client) failed");
			tls_server_size = 0;
		}

		if (tls_client_size > 0)
		{
			PRINT_INFO("send %d bytes to server", tls_client_size);
			print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)tls_client_buf, MIN(tls_client_size, MAX_DEBUG_HEX_LEN));
			*outBuf = tls_client_buf;
			*outSize = tls_client_size;
			return true;
		}
		else
		{
			*outBuf = NULL;
			*outSize = 0;
			return false;
		}
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		return false;

	}

	bool connectStage3(const char *inBuf, const unsigned int inSize)
	{
		int ret = 0;
		tls_client_size = 0;
		if (tls_client_buf != NULL)
			vPortFree(tls_client_buf);
		tls_client_buf = NULL;

		tls_server_size = inSize;
		PRINT_INFO("received %d bytes from server", tls_server_size);
		if (tls_server_size <= 0)
		{
			if (tls_server_size < 0)
			{
				tls_server_size = 0;
			}
			else
			{
				DO_ERROR(-1, "cannot receive data from server");
			}
		}
		else
		{
			memcpy(tls_server_buf, inBuf, tls_server_size);
			PRINT_INFO("received %d bytes from server", tls_server_size);
		}

		if (tls_server_size > 0)
		{ //send data to client
			ret = do_tls_setup(&master_ctx, slave_ctx, tls_client_handle, tls_server_buf, tls_server_size,
						&tls_client_buf, &tls_client_size, &tls_client_state);
			if (ret < 0) DO_ERROR(0, "exit");
			if (SCM_CONNECTION_STATUS_ERROR == tls_client_state)
				DO_ERROR(-1, "TLS setup (client) failed");
			tls_server_size = 0;
		}
		else
			return false;

		if (SCM_CONNECTION_STATUS_CONNECTED == tls_client_state)
		{
			return true;
		}
		else
		{
			return false;
		}
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		return false;

	}

	bool encrypt(const char *client_data, const unsigned int client_data_size, char **outBuf, unsigned int *outSize)
	{
		int ret;

		print_hex("+++++++ clent raw sent %d bytes: \"%s\"", "\n", (unsigned char *)client_data, MIN(client_data_size, MAX_DEBUG_HEX_LEN));
		//prepare data to be sent from client to server
		ret = do_tls_crypt(&master_ctx, slave_ctx, tls_client_handle, 1, (char *)client_data, client_data_size,
			&tls_client_buf, &tls_client_size, &tls_client_state);
		if (ret < 0) DO_ERROR(0, "exit");
		PRINT_INFO("------------ clent sent %d bytes (encrypted size %d): \"%s\"", client_data_size, tls_client_size, client_data);
		if (SCM_CONNECTION_STATUS_CONNECTED != tls_client_state) DO_ERROR(-1, "client closed TLS session");

		if (tls_client_size > 0)
		{
			PRINT_INFO("send %d bytes to server", tls_client_size);
			print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)tls_client_buf, MIN(tls_client_size, MAX_DEBUG_HEX_LEN));
			*outBuf = tls_client_buf;
			*outSize = tls_client_size;
			return true;
		}
		else
		{
			*outBuf = NULL;
			*outSize = 0;
			return false;
		}

		return true;
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		tls_client_size = 0;
		tls_server_size = 0;
		return false;
	}

	bool decrypt(const char *client_data, const unsigned int client_data_size, char *outBuf, unsigned int max_out_data_size, unsigned int *out_data_size)
	{
		int ret;
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		//if ((client_data != NULL) && (client_data_size > 0)/* && (client_data_size < sizeof(tls_server_buf))*/)
		{
			memcpy(&tls_server_buf, client_data, client_data_size);
			tls_server_size = client_data_size;
		}
		ret = do_tls_crypt(&master_ctx, slave_ctx, tls_client_handle, 0, tls_server_buf, tls_server_size,
			&tls_client_buf, &tls_client_size, &tls_client_state);
		if (ret < 0) DO_ERROR(0, "exit");
		PRINT_INFO("------------ client received %d bytes (decrypted size %d): \"%s\"", tls_server_size, tls_client_size, tls_client_buf);
		if (SCM_CONNECTION_STATUS_CONNECTED != tls_client_state)
			DO_ERROR(-1, "client closed TLS session");

		if (tls_client_size > 0)
		{
			PRINT_INFO("send %d bytes to server", tls_client_size);
			print_hex("DEBUG: SENT PACKET:     ", "\n", (unsigned char *)tls_client_buf, MIN(tls_client_size, MAX_DEBUG_HEX_LEN));
			*out_data_size = ((unsigned int)tls_client_size > max_out_data_size) ? max_out_data_size : (unsigned int)tls_client_size;
			memcpy(outBuf, tls_client_buf, tls_client_size);
			if (tls_client_buf != NULL)
			{
				vPortFree(tls_client_buf);
				tls_client_buf = NULL;
			}
		}

		return true;
		on_done:
		if (tls_client_buf != NULL)
		{
			vPortFree(tls_client_buf);
			tls_client_buf = NULL;
		}
		tls_client_size = 0;
		tls_server_size = 0;
		return false;
	}

	bool disconnect()
	{
		int ret;
		PRINT_INFO("------------ disconnecting \r\n");

		ret = do_tls_close(&master_ctx, slave_ctx, tls_client_handle, SCM_SESSION_CLOSE_REASON_COMPLETED, tls_server_buf, tls_server_size,
			&tls_client_buf, &tls_client_size, &tls_client_state);
		tls_client_size = 0;
		tls_server_size = 0;
		SKZI_OFF();
		if (ret < 0) DO_ERROR(-1, "cannot send data to disconnect server");
		return true;
	on_done:
		tls_client_size = 0;
		tls_server_size = 0;
		return false;
	}

	SKZI()
	{
		memset(&master_ctx, 0, sizeof(master_ctx));
		master_ctx.timeout_ms = SCM_TIMEOUT_HANG;
		master_ctx.responce_interval_ms = SCM_TIMEOUT_REPLY;

	}
};



#endif /* SKZI_SKZI_HPP_ */
