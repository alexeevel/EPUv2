/*
 * m66_test.hpp
 *
 *  Created on: Jun 12, 2021
 *      Author: abutko
 */

#ifndef M66_TEST_HPP_
#define M66_TEST_HPP_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <cmsis_os2.h>

#include "hwtesting/hwtesting.hpp"
#include "bsp/bsp.hpp"
#include "main.h"
#include "usart.h"
#include "debug/debug.hpp"
using namespace debug;

//#define USE_M66_DEBUG

#define huartM66Port huart3
#define huartM66Instance USART3

#define M66_RX_BUF_SIZE (1024 * 3)
#define M66_RX_MAX_BUF_SIZE (M66_RX_BUF_SIZE * 4)
#define MAX_QIRDI_CNT		10

extern uint8_t recvBufUartM66[M66_RX_BUF_SIZE];
extern uint8_t *m66_RX_buf_ptr;
extern int m66_RX_buf_cnt;
extern unsigned char rcvCharM66;
extern uint8_t bigRecvBufUartM66[M66_RX_MAX_BUF_SIZE];
extern int bigRecvBufUartM66size;

class M66Test : public AbstractHWTest
{
	typedef enum
	{
		M66_NOT_INITED = 0,
		M66_INIT_OK = 1,
		M66_GOT_ERROR = 2,
		M66_CONNECTED_OK = 3,
		M66_DISCONNECTED = 4
	} M66_STATE;

	typedef enum
	{
		M66_FUNCTIONALITY_MINIMAL = 0,
		M66_FUNCTIONALITY_FULL = 1,
		M66_FUNCTIONALITY_DISABLE_RF = 4
	} M66_FUNCTIONALITY;

	typedef enum
	{
		M66_NETWORK_REGISTRATION_NOT_REGISTERED_STOP_FINDING = 0,
		M66_NETWORK_REGISTRATION_REGISTERED_HOME_NETWORK = 1,
		M66_NETWORK_REGISTRATION_NOT_REGISTERED_FINDING = 2,
		M66_NETWORK_REGISTRATION_REGISTRATION_DENIED = 3,
		M66_NETWORK_REGISTRATION_UNKNOWN = 4,
		M66_NETWORK_REGISTRATION_REGISTERED_ROAMING = 5
	} M66_NETWORK_REGISTRATION;

	typedef enum
	{
		M66_NETWORK_REGISTRATION_CMD_DISABLE = 0,
		M66_NETWORK_REGISTRATION_CMD_ENABLE = 1,
		M66_NETWORK_REGISTRATION_CMD_ENABLE_WITH_LOCAL_INFO = 2,
	} M66_NETWORK_REGISTRATION_CMD;

	typedef enum
	{
		M66_GPRS_STACK_DETACHED = 0,
		M66_GPRS_STACK_ATTACHED = 1
	} M66_GPRS_STACK;

	typedef enum
	{
		M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA_NO_INDICATION = 0,
		M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA_SMALL_INDICATION = 1,
		M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA_FULL_INDICATION = 2
	} M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA;

	typedef enum
	{
		M66_SHOW_PROTOCOL_TYPE_DONT_SHOW = 0,
		M66_SHOW_PROTOCOL_TYPE_SHOW_TCP_OR_UDP = 1
	} M66_SHOW_PROTOCOL_TYPE;

	typedef enum
	{
		M66_TCP_IP_TRANSFER_MODE_NORMAL = 0,
		M66_TCP_IP_TRANSFER_MODE_TRANSPARENT = 1
	} M66_TCP_IP_TRANSFER_MODE;

	M66_STATE currState = M66_NOT_INITED;

	uint8_t check(const char *cRxBuffer, const char *findstr)
	{
		char *pc = strstr(cRxBuffer, findstr);
		if (pc)
			return 1;
		else
			return 0;
	}

	void deb_M66_HAL_UART_Transmit_IT(uint8_t *pData, uint16_t size, int timeout, char nodebug)
	{
		if ((size > 0) && (pData != NULL))
		{
#if 1
			if ((nodebug & 0x80) == 0)
			{
#if 0
				memset(debug_buf_m66, 0, sizeof(debug_buf_m66));
				my_strncpy( debug_buf_m66, "\r\nM66 >>", DEBUG_BUF_SIZE_M66 );
				memcpy( &debug_buf_m66[strlen( debug_buf_m66 )], pData, size );
				usb_debug_fix(&debug_buf_m66, size + strlen("\r\nM66 >>"));
#endif
#ifdef USE_M66_DEBUG
				if (size > 0)
					DBG::PRINT(LVL::INFO, "M66 >>", "" , "", (const char *)pData);
#endif
			}
#endif
			memset(&recvBufUartM66, 0, M66_RX_BUF_SIZE);
			m66_RX_buf_ptr = (uint8_t *)&recvBufUartM66;
			m66_RX_buf_cnt = 0;
			HAL_UART_Transmit_IT(&huartM66Port, pData, size);
			osDelay(timeout);
#if 1
			if ((nodebug & 1) == 0)
			{
#if 0
				memset(debug_buf_m66, 0, sizeof(debug_buf_m66));
				my_strncpy( debug_buf_m66, "\r\nM66 <<", DEBUG_BUF_SIZE_M66 );
				memcpy( &debug_buf_m66[strlen( debug_buf_m66 )], &recvBufUartM66, m66_RX_buf_cnt );
				usb_debug_fix(&debug_buf_m66, m66_RX_buf_cnt + strlen("\r\nM66 <<"));
#endif
#ifdef USE_M66_DEBUG
				//if (m66_RX_buf_cnt > 0)
					DBG::PRINT(LVL::INFO, "M66 <<", "" , "", (const char *)&recvBufUartM66);
#endif
			}
#endif
		}
	}

	bool GSM_POWER_ON()
	{
		powerControl->on();
		osDelay(500);
		HAL_GPIO_WritePin(SIM_SEL_GPIO_Port, SIM_SEL_Pin, GPIO_PIN_RESET);
		memset(&recvBufUartM66, 0, M66_RX_BUF_SIZE);
		m66_RX_buf_ptr = (uint8_t *)&recvBufUartM66;
		m66_RX_buf_cnt = 0;
		HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port, GSM_PWR_ON_Pin, GPIO_PIN_SET);
		HAL_UART_Receive(&huartM66Port, m66_RX_buf_ptr, M66_RX_BUF_SIZE, 3000);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT\r\n", 4, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"ATE0\r\n", 6, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	void GSM_POWER_OFF()
	{
		return;
		HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port, GSM_PWR_ON_Pin, GPIO_PIN_RESET);
		osDelay(850);
		HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port, GSM_PWR_ON_Pin, GPIO_PIN_SET);

		memset(&recvBufUartM66, 0, M66_RX_BUF_SIZE);
		m66_RX_buf_ptr = (uint8_t *)&recvBufUartM66;
		m66_RX_buf_cnt = 0;
		for (int i=0; i<6; i++)
		{
			if (m66_RX_buf_cnt > 0)
				DBG::PRINT(LVL::INFO, "M66 <<", "" , "", (const char *)&recvBufUartM66);
			osDelay(2000);
		}
		powerControl->off();
		osDelay(100);
		HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port, GSM_PWR_ON_Pin, GPIO_PIN_RESET);
//		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		osDelay(10000);
	}

	bool xCheckParseIMEI(char *cRxBuffer, char *outStr)
	{
		char *pc = cRxBuffer;
		int ctr = 0;
		while (pc)
		{
			if ((*pc >= '0') && (*pc <= '9'))
			{
				ctr++;
				if (ctr == 15)
				{
					pc -= 15 - 1;
					memcpy(outStr, pc, 15);
					break;
				}
			}
			else
			{
				ctr = 0;
			}
			pc++;
		}
		if (ctr == 15)
			return true;
		else
			return false;
	}

	bool GSM_GET_IMEI(char *outIMEI)
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+GSN\r\n", 8, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (!xCheckParseIMEI((char *)&recvBufUartM66, outIMEI))
		{
			return false;
		}
		return true;
	}

	bool CHECK_SIM1_CARD()
	{
		if (HAL_GPIO_ReadPin(SIM_DET_GPIO_Port, SIM_DET_Pin) == GPIO_PIN_SET)
			return true;
		else
			return false;
	}

	bool GSM_GET_IMSI(char *outIMSI)
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CIMI\r\n", 9, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (!xCheckParseIMEI((char *)&recvBufUartM66, outIMSI))
		{
			return false;
		}
		return true;
	}

	bool GSM_Set_Functionality(M66_FUNCTIONALITY func)
	{
		char cmdToSend[15] = {0};
		snprintf((char *)&cmdToSend, sizeof(cmdToSend), "AT+CFUN=%01d\r\n", func);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)&cmdToSend, strlen(cmdToSend), 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool xCheckCFUN(char *cRxBuffer, M66_FUNCTIONALITY *cfun)
	{
		char *pc = strstr(cRxBuffer, "+CFUN: ");
		if (pc)
		{
			pc += strlen("+CFUN: ");
			if (strlen(pc) >= 1)
			{
				char code = *pc;
				if ((code >= '0') && (code <= '9'))
				{
					code -= 0x30;
					if ((code == M66_FUNCTIONALITY_MINIMAL) || (code == M66_FUNCTIONALITY_FULL) || (code == M66_FUNCTIONALITY_DISABLE_RF))
					{
						*cfun = (M66_FUNCTIONALITY)code;
						return true;
					}
					else
						return false;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	bool GSM_Get_Functionality(M66_FUNCTIONALITY *outFunc)
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CFUN?\r\n", 10, 500, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (!xCheckCFUN((char *)&recvBufUartM66, outFunc))
		{
			return false;
		}
		return true;
	}

	bool GSM_Set_Network_Registration()
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CREG=1\r\n", 11, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool xCheckCREG(char *cRxBuffer, M66_NETWORK_REGISTRATION_CMD *outCmd, M66_NETWORK_REGISTRATION *outStatus)
	{
		char *pc = strstr(cRxBuffer, "+CREG: ");
		if (pc)
		{
			pc += strlen("+CREG: ");
			if (strlen(pc) >= 3)
			{
				char code = *pc;
				char delim = *(pc + 1);
				char code2 = *(pc + 2);
				if ((code >= '0') && (code <= '2') && (delim == ',') && (code2 >= '0') && (code2 <= '5'))
				{
					*outCmd = (M66_NETWORK_REGISTRATION_CMD)((uint8_t)code - 0x30);
					*outStatus = (M66_NETWORK_REGISTRATION)((uint8_t)code2 - 0x30);
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	bool GSM_Get_Network_Registration(M66_NETWORK_REGISTRATION_CMD *outCmd, M66_NETWORK_REGISTRATION *outReg)
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CREG?\r\n", 10, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (!xCheckCREG((char *)&recvBufUartM66, outCmd, outReg))
		{
			return false;
		}
		return true;
	}

	bool GSM_Set_GPRS_Stack()
	{
		int ctr = 0;
		bool retValue = false;
		osDelay(200);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CGATT=1\r\n", 12, 3000, 0);
		retValue = check((char *)&recvBufUartM66, (char *)"OK\r");
		while ((!retValue) && (ctr < 35))
		{
			retValue = check((char *)&recvBufUartM66, (char *)"OK\r");
			osDelay(200);
			ctr++;
		}
		return retValue;
	}

	bool xCheckCGATT(char *cRxBuffer, M66_GPRS_STACK *cfun)
	{
		char *pc = strstr(cRxBuffer, "+CGATT: ");
		if (pc)
		{
			pc += strlen("+CGATT: ");
			if (strlen(pc) >= 1)
			{
				char code = *pc;
				if ((code >= '0') && (code <= '1'))
				{
					code -= 0x30;
					if ((code == M66_GPRS_STACK_DETACHED) || (code == M66_GPRS_STACK_ATTACHED))
					{
						*cfun = (M66_GPRS_STACK)code;
						return true;
					}
					else
						return false;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	bool GSM_Get_GPRS_Stack(M66_GPRS_STACK *outGPRSstate)
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CGATT?\r\n", 11, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (!xCheckCGATT((char *)&recvBufUartM66, outGPRSstate))
		{
			return false;
		}
		return true;
	}
#if 0
	bool xCheckCREG(char *cRxBuffer, M66_NETWORK_REGISTRATION_CMD *outCmd, M66_NETWORK_REGISTRATION *outStatus)
	{
		char *pc = strstr(cRxBuffer, "+CREG: ");
		if (pc)
		{
			pc += strlen("+CREG: ");
			if (strlen(pc) >= 3)
			{
				char code = *pc;
				char delim = *(pc + 1);
				char code2 = *(pc + 2);
				if ((code >= '0') && (code <= '2') && (delim == ',') && (code2 >= '0') && (code2 <= '5'))
				{
					*outCmd = (M66_NETWORK_REGISTRATION_CMD)((uint8_t)code - 0x30);
					*outStatus = (M66_NETWORK_REGISTRATION)((uint8_t)code2 - 0x30);
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}
#endif
	bool GSM_Get_Signal_Quality()
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+CSQ\r\n", 8, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
#if 0
		if (!xCheckCREG((char *)&recvBufUartM66, outCmd, outReg))
		{
			return false;
		}
#endif
		return true;
	}

	bool GSM_Set_Method_Handle_Received_TCP_IP_Data(M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA indi)
	{
		char cmdToSend[15] = {0};
		snprintf((char *)&cmdToSend, sizeof(cmdToSend), "AT+QINDI=%01d\r\n", indi);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)&cmdToSend, strlen(cmdToSend), 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool GSM_Set_Show_Protocol_Type(M66_SHOW_PROTOCOL_TYPE show)
	{
		char cmdToSend[20] = {0};
		snprintf((char *)&cmdToSend, sizeof(cmdToSend), "AT+QISHOWPT=%01d\r\n", show);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)&cmdToSend, strlen(cmdToSend), 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool GSM_Set_TCP_IP_Transfer_Mode(M66_TCP_IP_TRANSFER_MODE mode)
	{
		char cmdToSend[20] = {0};
		snprintf((char *)&cmdToSend, sizeof(cmdToSend), "AT+QIMODE=%01d\r\n", mode);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)&cmdToSend, strlen(cmdToSend), 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool GSM_TCP_Close_Connection()
	{
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)"AT+QICLOSE\r\n", 12, 300, 0);
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		return true;
	}

	bool GSM_TCP_Open_Connection(const char *serverName, unsigned short port)
	{
		char cmdToSend[50] = {0};
		snprintf((char *)&cmdToSend, sizeof(cmdToSend), "AT+QIOPEN=\"TCP\",\"%s\",%d\r\n", serverName, port);
		deb_M66_HAL_UART_Transmit_IT((uint8_t *)&cmdToSend, strlen(cmdToSend), 1000, 0);

		int loc_ctr = 0;
		if (check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			while (loc_ctr < 29)
			{
				char *pc = strstr((char *)&recvBufUartM66, "ERROR");
				if (pc)
					break;
				pc = strstr((char *)&recvBufUartM66, "CONNECT OK");
				if (pc)
					break;
				pc = strstr((char *)&recvBufUartM66, "ALREADY CONNECT");
				if (pc)
					break;
				pc = strstr((char *)&recvBufUartM66, "CONNECT FAIL");
				if (pc)
					break;
				pc = strstr((char *)&recvBufUartM66, "CLOSED");
				if (pc)
					break;
				loc_ctr++;
				osDelay(1000);
			}
		}
#ifdef USE_M66_DEBUG
		if (m66_RX_buf_cnt > 0)
			DBG::PRINT(LVL::INFO, "M66 <<", "" , "", (const char *)&recvBufUartM66);
#endif
		if (!check((char *)&recvBufUartM66, (char *)"OK\r"))
		{
			return false;
		}
		if (check((char *)&recvBufUartM66, (char *)"CONNECT OK"))
			return true;
		return false;
	}

	int xCheckQIRDIint(char *cRxBuffer, int *p1, int *p2, int *p3, int *p4, int *p5, int *p6, char **endfind)
	{
		*p1 = 0;
		*p2 = 0;
		*p3 = 0;
		*p4 = 0;
		*p5 = 0;
		*p6 = 0;
		int *arr[6] = {p1, p2, p3, p4, p5, p6};
		char *pc = strstr(cRxBuffer, "+QIRDI: ");
		if (pc)
		{
			pc += strlen("+QIRDI: ");
			int commas = 0;
			char *pcfind = pc;
			while (*pcfind != '\r')
			{
				if ((commas >= 0) && (commas <= 5))
				{
					if ((*pcfind >= '0') && (*pcfind <= '9'))
						*arr[commas] = *arr[commas] * 10 + (int)(*pcfind - '0');
					else
						if (*pcfind != ',')
							return -1;
				}
				if (*pcfind == ',')
					commas++;
				pcfind++;
			}
			pcfind++;
			if ((commas == 5) && (*pcfind == '\n'))
			{
				*endfind = pcfind;
				return 1;
			}
		}
		return -1;
	}

	char req[30] = {0};
	char xCheckQIRDI(char *cRxBuffer)
	{
		memset(&req, 0, sizeof(req));
		char *pc = strstr(cRxBuffer, "+QIRDI: ");
		if (pc)
		{
			pc += strlen("+QINDI: ");
			memcpy(&req, "AT+QIRD=", strlen("AT+QIRD="));
			char *reqStr = (char*)&req + strlen("AT+QIRD=");
			int commas = 0;
			char *pcfind = pc;
			while (*pcfind != 0)
			{
				if (((commas >= 0) && (commas <= 2)) || (commas == 4))
					*reqStr++ = *pcfind;
				if (*pcfind == ',')
					commas++;
				pcfind++;
			}
			reqStr--;
			*reqStr++ = '\r';
			*reqStr = '\n';
			return 1;
			if (commas != 5)
			{
				memset(&req, 0, sizeof(req));
				return 0;
			}
		}
		return 0;
	}

	int xCheckQIRD(char *cRxBuffer, int buf_sz, char *dst)
	{
		int val = 0;
		char *pc = strstr(cRxBuffer, "+QIRD: ");
		if (pc)
		{
			pc += strlen("+QIRD: ");
			int commas = 0;
			char *pcfind = pc;
			while (*pcfind != '\r')
			{
				if (commas == 2)
				{
					if ((*pcfind >= '0') && (*pcfind <= '9'))
						val = val * 10 + (int)(*pcfind - '0');
					else
						return -1;
				}
				if (*pcfind == ',')
					commas++;
				pcfind++;
			}
			pcfind++;
			if ((commas == 2) && (*pcfind == '\n'))
			{
				pcfind++;
				if (((int)(pcfind - cRxBuffer) + val) <= buf_sz)
				{
					memcpy(dst, pcfind, val);
					return val;
				}
				else
					return -2;
			}
		}
		return -1;
	}

	char strIMEI[16] = {0};
	char strIMSI[16] = {0};
	int valuesQIRDI[MAX_QIRDI_CNT][6] = {0};
	uint8_t sendBufUartM66[50];
	char strBuf[100];
	power_ctrl *powerControl;

public:
	M66Test(power_ctrl *v40ctrl)
	{
		powerControl = v40ctrl;
	}
	const char *getStateStr();
	bool init();
	bool setupModem();
	bool sendSocket(const char *ptrSendSockBuf, const unsigned int inp_sizeSendSockBuf, char **outPtrReceived, unsigned int *outReceivedSize);
	void disconnect();
	bool test() override
	{
		bool on = GSM_POWER_ON();
		if (on == false)
		{
			GSM_POWER_OFF();
			DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "POWER ON TIMEOUT");
			return false;
		}
		else
		{
			DBG::PRINT(LVL::INFO, "M66", "" , "POWER ON OK");
			on = GSM_GET_IMEI((char *)&strIMEI);
			if (on == false)
			{
				GSM_POWER_OFF();
				DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "GET IMEI ERROR");
				return false;
			}
			else
			{
				DBG::PRINT(LVL::INFO, "M66", "" , "GET IMEI OK ", strIMEI);
				on = CHECK_SIM1_CARD();
				if (on == false)
				{
					GSM_POWER_OFF();
					DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "SIM DETECT ERROR");
					return false;
				}
				else
				{
					DBG::PRINT(LVL::INFO, "M66", "" , "SIM DETECT OK");
					on = GSM_GET_IMEI((char *)&strIMEI);
					//on = GSM_GET_IMSI((char *)&strIMSI);
					if (on == false)
					{
						GSM_POWER_OFF();
						DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "GET IMSI ERROR");
						return false;
					}
					else
					{
						GSM_POWER_OFF();
						DBG::PRINT(LVL::INFO, "M66", "" , "GET IMSI OK ", strIMSI);
						return true;
					}
				}
			}
		}
	}

};


#endif /* M66_TEST_HPP_ */



