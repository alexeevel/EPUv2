/*
 * m66.cpp
 *
 *  Created on: 24 июн. 2021 г.
 *      Author: abutko
 */



#include "gsm/m66_test.hpp"

uint8_t recvBufUartM66[M66_RX_BUF_SIZE];
uint8_t *m66_RX_buf_ptr = (uint8_t *)&recvBufUartM66;
int m66_RX_buf_cnt = 0;
unsigned char rcvCharM66 = 0;
uint8_t bigRecvBufUartM66[M66_RX_MAX_BUF_SIZE];
int bigRecvBufUartM66size = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == huartM66Instance)
	{
		*m66_RX_buf_ptr++ = rcvCharM66;
		m66_RX_buf_cnt++;
		HAL_UART_Receive_IT(huart, &rcvCharM66, 1);
		return;
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == huartM66Instance)
	{
		m66_RX_buf_ptr -= m66_RX_buf_cnt;
		m66_RX_buf_cnt = 0;
		HAL_UART_Receive_IT(huart, &rcvCharM66, 1);
		return;
	}
}

const char *M66Test::getStateStr()
{
	switch (this->currState)
	{
	case M66_NOT_INITED:
		return "NOT INITIALIZED";
	case M66_INIT_OK:
		return "INITIALIZATION OK";
	case M66_GOT_ERROR:
		return "GOT ERROR";
	case M66_CONNECTED_OK:
		return "NOT CONNECTED";
	case M66_DISCONNECTED:
		return "DISCONNECTED";
	default:
		return "UNKNOWN";
	};
}

bool M66Test::init()
{
	bool on = GSM_POWER_ON();
	if (on == false)
	{
		GSM_POWER_OFF();
		DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "POWER ON TIMEOUT");
		return false;
	}
	else
	{
		DBG::PRINT(LVL::INFO, "M66", "" , "POWER ON OK");
		on = GSM_GET_IMEI((char *)&strIMEI);
		if (on == false)
		{
			GSM_POWER_OFF();
			DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "GET IMEI ERROR");
			return false;
		}
		else
		{
			DBG::PRINT(LVL::INFO, "M66", "" , "GET IMEI OK ", strIMEI);
			on = CHECK_SIM1_CARD();
			if (on == false)
			{
				GSM_POWER_OFF();
				DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "SIM DETECT ERROR");
				return false;
			}
			else
			{
				DBG::PRINT(LVL::INFO, "M66", "" , "SIM DETECT OK");
				on = GSM_GET_IMSI((char *)&strIMSI);
				if (on == false)
				{
					GSM_POWER_OFF();
					DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "GET IMSI ERROR");
					return false;
				}
				else
				{
					DBG::PRINT(LVL::INFO, "M66", "" , "GET IMSI OK ", strIMSI);

					setupModem();

					//GSM_POWER_OFF();
					return true;
				}
			}
		}
	}
}

bool M66Test::setupModem()
{
	this->currState = M66_NOT_INITED;
	M66_FUNCTIONALITY nowFunctionality;
	if (GSM_Set_Functionality(M66_FUNCTIONALITY_FULL))
	{
		if (GSM_Get_Functionality(&nowFunctionality))
		{
			if (nowFunctionality == M66_FUNCTIONALITY_FULL)
			{
				if (GSM_Set_Network_Registration())
				{
					int cregctr = 0;
					M66_NETWORK_REGISTRATION_CMD creg_n;
					M66_NETWORK_REGISTRATION creg_stat;
					while (cregctr < 35)
					{
						if (GSM_Get_Network_Registration(&creg_n, &creg_stat))
						{
							if ((creg_n == M66_NETWORK_REGISTRATION_CMD_ENABLE) &&
								((creg_stat == M66_NETWORK_REGISTRATION_REGISTERED_HOME_NETWORK) || (creg_stat == M66_NETWORK_REGISTRATION_REGISTERED_ROAMING)))
							{
								break;
							}
						}
						cregctr++;
						osDelay(1000);
					}
					if ((creg_n == M66_NETWORK_REGISTRATION_CMD_ENABLE) &&
						((creg_stat == M66_NETWORK_REGISTRATION_REGISTERED_HOME_NETWORK) || (creg_stat == M66_NETWORK_REGISTRATION_REGISTERED_ROAMING)))
					{
						//GSM_Get_Signal_Quality();
						GSM_Set_GPRS_Stack();
						//if (GSM_Set_GPRS_Stack())
						{
							cregctr = 0;
							M66_GPRS_STACK gpsStack;
							while (cregctr < 35)
							{
								if (GSM_Get_GPRS_Stack(&gpsStack))
								{
									if (gpsStack == M66_GPRS_STACK_ATTACHED)
									{
										break;
									}
								}
								cregctr++;
								osDelay(1000);
							}
							if (gpsStack == M66_GPRS_STACK_ATTACHED)
							{
								if (GSM_Set_Method_Handle_Received_TCP_IP_Data(M66_METHOD_HANDLE_RECEIVED_TCP_IP_DATA_FULL_INDICATION))
								{
									if (GSM_Set_Show_Protocol_Type(M66_SHOW_PROTOCOL_TYPE_DONT_SHOW))
									{
										if (GSM_Set_TCP_IP_Transfer_Mode(M66_TCP_IP_TRANSFER_MODE_NORMAL))
										{
											GSM_TCP_Close_Connection();
											this->currState = M66_INIT_OK;
											if (GSM_TCP_Open_Connection("10.77.60.14", 30197))
											{
												this->currState = M66_CONNECTED_OK;
												snprintf((char *)&strBuf, sizeof(strBuf), "Connect OK");
												DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
												return true;
											}
											else
											{
												this->currState = M66_DISCONNECTED;
												snprintf((char *)&strBuf, sizeof(strBuf), "Connect FAILED");
												DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
											}
										}
										else
										{
											snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_TCP_IP_Transfer_Mode FAILED");
											DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
										}
									}
									else
									{
										snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_Show_Protocol_Type FAILED");
										DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
									}
								}
								else
								{
									snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_Method_Handle_Received_TCP_IP_Data FAILED");
									DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
								}
							}
							else
							{
								snprintf((char *)&strBuf, sizeof(strBuf), "GPRS Attach Stack FAILED");
								DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
							}
						}
						/*
						else
						{
							snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_GPRS_Stack FAILED");
							DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
						}
						*/
					}
					else
					{
						snprintf((char *)&strBuf, sizeof(strBuf), "GSM Network Registration FAILED");
						DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
					}
				}
				else
				{
					snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_Network_Registration FAILED");
					DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
				}
			}
			else
			{
				snprintf((char *)&strBuf, sizeof(strBuf), "GSM Functionality NOT M66_FUNCTIONALITY_FULL");
				DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
			}
		}
		else
		{
			snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Get_Functionality FAILED");
			DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
		}
	}
	else
	{
		snprintf((char *)&strBuf, sizeof(strBuf), "GSM_Set_Functionality FAILED");
		DBG::PRINT(LVL::EMCY, "M66", "" , "setupModem", (char *)&strBuf);
	}
	return false;
}

bool M66Test::sendSocket(const char *ptrSendSockBuf, const unsigned int inp_sizeSendSockBuf, char **outPtrReceived, unsigned int *outReceivedSize)
{
	if ((ptrSendSockBuf != NULL) && (inp_sizeSendSockBuf > 0) && (inp_sizeSendSockBuf < M66_RX_BUF_SIZE))
	{
		char resendCtr = 0;
		int realFoundQRDIs = 0;
		while (resendCtr < 1)
		{
			realFoundQRDIs = 0;
			memset (&valuesQIRDI, 0, sizeof(valuesQIRDI));
			//DEBUG_PRINT_INFO(DBG_M66, "\r\n-=GSM_Modem_Task=- M66 received event send %i bytes to sock\r\n", mevent.sizeSendSockBuf);

			int partData = 1450;
			int wholeSendSize = inp_sizeSendSockBuf;
			int loc_ctr = 0;
			uint8_t *sendDataPtr = (uint8_t *)ptrSendSockBuf;

			while (wholeSendSize > 0)
			{
				int sizeSendSockBuf = partData;
				if (wholeSendSize < partData)
					sizeSendSockBuf = wholeSendSize;

				memset(&recvBufUartM66, 0, sizeof(recvBufUartM66));
				memset(&sendBufUartM66, 0, sizeof(sendBufUartM66));
				snprintf((char *)&sendBufUartM66, sizeof(sendBufUartM66), "AT+QISEND=%d\r\n", sizeSendSockBuf);
				deb_M66_HAL_UART_Transmit_IT((uint8_t *)&sendBufUartM66, strlen((char *)&sendBufUartM66), 300, 0);
				if (!check((char *)&recvBufUartM66, ">"))
				{
					if (strstr((char *)&recvBufUartM66, "ERROR"))
					{
						this->currState = M66_GOT_ERROR;
						return false;
					}
					if (strstr((char *)&recvBufUartM66, "CLOSED"))
					{
						this->currState = M66_DISCONNECTED;
						return false;
					}
				}
				deb_M66_HAL_UART_Transmit_IT(sendDataPtr, sizeSendSockBuf, 1000, 0x80);
				loc_ctr = 0;
				if (!check((char *)&recvBufUartM66, "SEND OK\r"))
				{
					while (loc_ctr < 29)
					{
						if (strstr((char *)&recvBufUartM66, "ERROR"))
						{
							this->currState = M66_GOT_ERROR;
							break;
						}
						if (strstr((char *)&recvBufUartM66, "CLOSED"))
						{
							this->currState = M66_DISCONNECTED;
							break;
						}
						loc_ctr++;
						osDelay(1000);
					}
				}
				if (!check((char *)&recvBufUartM66, "SEND OK\r\n"))
				{
					return false;
					//isSendOKSock = ERR_SOCK_SEND_FAILED;
				}
				wholeSendSize -= sizeSendSockBuf;
				sendDataPtr += sizeSendSockBuf;
			}
			//osDelay(1000);

			bool foundQIRDI = false;
			loc_ctr = 0;
			char *ptrQIRDI = (char *)&recvBufUartM66;
			if (!check(ptrQIRDI, "+QIRDI:"))
			{
				while (loc_ctr < 10)
				{
					if (strstr((char *)&recvBufUartM66, "ERROR"))
					{
						this->currState = M66_GOT_ERROR;
						break;
					}
					if (strstr((char *)&recvBufUartM66, "CLOSED"))
					{
						this->currState = M66_DISCONNECTED;
						break;
					}
					char *pc = strstr(ptrQIRDI, "+QIRDI:");
					if (pc)
					{
						foundQIRDI = true;
						ptrQIRDI = pc;
						ptrQIRDI += 6;
						loc_ctr = 5;
					}
					loc_ctr++;
					osDelay(1000);
				}
			}
			else
			{//TODO Maybe need to wait for more qirdis
				foundQIRDI = true;
			}
			if (foundQIRDI)
			{
#if 0
			memset(debug_buf_m66, 0, sizeof(debug_buf_m66));
			my_strncpy( debug_buf_m66, "\r\nM66 <<", DEBUG_BUF_SIZE_M66 );
			memcpy( &debug_buf_m66[strlen( debug_buf_m66 )], &recvBufUartM66, m66_RX_buf_cnt );
			usb_debug_fix(&debug_buf_m66, m66_RX_buf_cnt + strlen("\r\nM66 <<"));
#endif
#ifdef USE_M66_DEBUG
				if (m66_RX_buf_cnt > 0)
					DBG::PRINT(LVL::INFO, "M66 <<", "" , "", (const char *)&recvBufUartM66);
#endif

				char *tempBeginFind = (char *)&recvBufUartM66;
				char *tempEndFind = NULL;
				int retValue = 1;
				while ((realFoundQRDIs < 10) && (retValue > 0))
				{
					retValue = xCheckQIRDIint(tempBeginFind, &valuesQIRDI[realFoundQRDIs][0], &valuesQIRDI[realFoundQRDIs][1], &valuesQIRDI[realFoundQRDIs][2], &valuesQIRDI[realFoundQRDIs][3], &valuesQIRDI[realFoundQRDIs][4], &valuesQIRDI[realFoundQRDIs][5], &tempEndFind);
					if (retValue)
					{
						tempBeginFind = tempEndFind;
						realFoundQRDIs++;
					}
				}
				realFoundQRDIs--;
				if (realFoundQRDIs == 0)
				{
					//usb_debug("\r\n-=M66=- Restart send %i times\r\n", resendCtr);
					//asm("  bkpt");
					resendCtr++;
					continue;
				}
				else
					break;
			}
			else
				return false;
		}
		volatile int wholePacketSize = 0;
		for (int i=0; i<realFoundQRDIs; i++)
		{
			//usb_debug("\r\nReceived +QIRDI[%i] %i bytes", i, valuesQIRDI[i][4]);
			wholePacketSize += valuesQIRDI[i][4];
		}
		/*
		 * +QIRDI: 0,1,0,1,1460,1460

+QIRDI: 0,1,0,2,138,1598
		 *
		 *
		 */
		//usb_debug("\r\nTotal Received +QIRDI %i in summ of %i bytes\r\n", realFoundQRDIs, wholePacketSize);
		//if (valuesQIRDI[realFoundQRDIs - 1][5] != wholePacketSize)
		//{
		//	asm("  bkpt");
		//}
		if (wholePacketSize > 0)
		{
			char *tempBeginFind = (char *)&bigRecvBufUartM66;
			bigRecvBufUartM66size = 0;
			for (int i=0; i<realFoundQRDIs; i++)
			{
				memset(&sendBufUartM66, 0, sizeof(sendBufUartM66));
				sprintf((char *)&sendBufUartM66, "AT+QIRD=%i,%i,%i,%i\r\n", valuesQIRDI[i][0], valuesQIRDI[i][1], valuesQIRDI[i][2], valuesQIRDI[i][4]);
				memset(&recvBufUartM66, 0, sizeof(recvBufUartM66));
				deb_M66_HAL_UART_Transmit_IT((uint8_t *)&sendBufUartM66, strlen((char *)&sendBufUartM66), 500, 1);//was 3000
				int valofbuf = 0;
				unsigned int sz = m66_RX_buf_cnt;
				if ((bigRecvBufUartM66size + sz) <= M66_RX_MAX_BUF_SIZE)
				{
					if ((valofbuf = xCheckQIRD((char *)&recvBufUartM66, sz, tempBeginFind)) >= 0)
					{
						tempBeginFind += valofbuf;
						bigRecvBufUartM66size += valofbuf;
						//DEBUG_PRINT_INFO(DBG_M66, "\r\n-=GSM_Modem_Task=- Received %i bytes data\r\n", sz);
					}
				}
			}
			if (bigRecvBufUartM66size == wholePacketSize)
			{
				*outPtrReceived = (char *)&bigRecvBufUartM66;
				*outReceivedSize = bigRecvBufUartM66size;
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return true;
}

void M66Test::disconnect()
{
	GSM_TCP_Close_Connection();
	GSM_POWER_OFF();
}
