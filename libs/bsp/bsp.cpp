/*
 * bsp.cpp
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#include "bsp.hpp"
#include "cmsis_os2.h"
#include "debug/debug.hpp"
using namespace debug;
std::array<gpio_out_pin, EPU_BSP::LED_COUNT> EPU_BSP::led_pins = {gpio_out_pin(LED1_GPIO_Port,LED1_Pin, false)
		, gpio_out_pin(LED2_GPIO_Port,LED2_Pin, false)
		, gpio_out_pin(LED3_GPIO_Port,LED3_Pin, false)
		, gpio_out_pin(LED4_GPIO_Port,LED4_Pin, false)
		, gpio_out_pin(LED5_GPIO_Port,LED5_Pin, false)
		, gpio_out_pin(LED6_GPIO_Port,LED6_Pin, false)};

bool power_ctrl::test()
{

	size_t cnt=0;
	bool result=false;
	on();

	while(cnt<wait_ms&&!is_on())
	{
		osDelay(1);

	}
	if(cnt<wait_ms)
	{
		DBG::PRINT(LVL::INFO, "POWER", name , "TEST OK", "WAIT= %d ms", cnt);
		result = true;
	}
	else
	{
		DBG::PRINT(LVL::EMCY, "POWER", name , "TEST FAIL", "WAIT= %d ms", cnt);

	}
	off();
	return result;
}
