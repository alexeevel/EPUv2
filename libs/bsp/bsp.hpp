/*
 * bsp.hpp
 *
 *  Created on: 1 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef INC_BSP_HPP_
#define INC_BSP_HPP_
#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_rtc.h"
#include "main.h"
#include <array>

#include "hal/gpio.hpp"
#include "hwtesting/hwtesting.hpp"

class power_ctrl:public AbstractHWTest
{
	gpio_out_pin control;
	gpio_in_pin status;
	uint32_t wait_ms;
#if (DEBUG==1)
	const char* name;
#endif
public:
	power_ctrl(GPIO_TypeDef* GPIOc, uint16_t Pinc,GPIO_TypeDef* GPIOs, uint16_t Pins, const char *nme, uint32_t wait=5 ):
		control(GPIOc, Pinc, false), status(GPIOs, Pins, false), wait_ms(wait)
	{
#if (DEBUG==1)
		name=nme;
#else
		(void)name;
#endif
	}
	bool test()override;
	void on(){control.set();}
	void off(){control.reset();}
	bool is_on(){return status.read();}
};
class EPU_BSP
{



public:
	static constexpr size_t LED_COUNT = 6;
	static uint32_t get_unixtime()
	{
		return 0;
	}
	static void led_on(uint32_t led)
	{
		if(led<led_pins.size())
			led_pins[led].set();
	}
	static void led_off(uint32_t led){if(led<led_pins.size())
		led_pins[led].reset();}
	static void led_toggle(uint32_t led){if(led<led_pins.size())
		led_pins[led].toggle();}



private:
	static std::array<gpio_out_pin, LED_COUNT> led_pins;

};

#endif /* INC_BSP_HPP_ */
