/*
 * spi.hpp
 *
 *  Created on: 7 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef SPI_SPI_HPP_
#define SPI_SPI_HPP_
#include "bsp/bsp.hpp"
#include "cmsis_os2.h"
class spi_port {
	SPI_HandleTypeDef *spi;
	gpio_out_pin cs;
	size_t pause_us = 1000U;
public:
	spi_port(SPI_HandleTypeDef *spi, GPIO_TypeDef *port, uint16_t cs_pin) :
			spi(spi), cs(port, cs_pin, false) {
		cs.set();
	}
	void deselect()
	{
		cs.set();
	}
	void select()
	{
		cs.reset();
	}
	error_t tx(uint8_t *buff, size_t count) {
		cs.reset();
		osDelay(pause_us / 1e3);
		auto result = HAL_SPI_Transmit(spi, buff, count, 1000);
		osDelay(pause_us / 1e3);
		cs.set();
		return result;
	}
	error_t txrx(uint8_t *tx, uint8_t * rx, size_t count) {
		cs.reset();
		osDelay(pause_us / 1e3);
		auto result = HAL_SPI_TransmitReceive(spi, tx, rx, count, 1000);
		osDelay(pause_us / 1e3);
		cs.set();
		return result;
	}

};

#endif /* SPI_SPI_HPP_ */
