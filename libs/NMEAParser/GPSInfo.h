// GPSInfo.h: interface for the GPSInfo class.
//
//////////////////////////////////////////////////////////////////////
#include <cstdint>


struct GPSInfo  
{
public:
	float m_latitude;
	float m_longitude;
	float m_altitude;
	uint32_t m_nSentences;
	uint8_t m_signalQuality;
	uint8_t m_satelitesInUse;

	GPSInfo();
	virtual ~GPSInfo();
};


