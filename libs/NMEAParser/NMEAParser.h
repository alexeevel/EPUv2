// NMEAParser.h: interface for the NMEAParser class.
//
//////////////////////////////////////////////////////////////////////



#include "GPSInfo.h"	// Added by ClassView
#include <cstdint>
#include <cstddef>

class NMEAParser  
{
public:
	NMEAParser();

	virtual ~NMEAParser();
	void Parse(const char *buf, const size_t bufSize);
	GPSInfo& GetActualGPSInfo();

private:


	void ParseRecursive(const char ch);
	void ParseNMEASentence(const char *addressField,
		                   const char *buf, const size_t bufSize);
	void ProcessGPGGA(const char *buf, const size_t bufSize);
	void ProcessGPGSA(const char *buf, const size_t bufSize);
	void ProcessGPGSV(const char *buf, const size_t bufSize);
	void ProcessGPRMB(const char *buf, const size_t bufSize);
	void ProcessGPRMC(const char *buf, const size_t bufSize);
	void ProcessGPZDA(const char *buf, const size_t bufSize);

	bool m_logging;
	GPSInfo m_GPSInfo;
};


