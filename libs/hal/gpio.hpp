/*
 * gpio.hpp
 *
 *  Created on: 4 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef HAL_GPIO_HPP_
#define HAL_GPIO_HPP_
#include "stm32l4xx_hal.h"

class gpio_out_pin
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
	bool inverted;
public:
	gpio_out_pin():GPIOx(nullptr),GPIO_Pin(0),inverted(false){}
	gpio_out_pin(GPIO_TypeDef* GPIO, uint16_t Pin, bool inv):GPIOx(GPIO),GPIO_Pin(Pin),inverted(inv){}
	void set(){HAL_GPIO_WritePin(GPIOx, GPIO_Pin, inverted?GPIO_PIN_RESET:GPIO_PIN_SET);}
	void reset(){HAL_GPIO_WritePin(GPIOx, GPIO_Pin, inverted?GPIO_PIN_SET:GPIO_PIN_RESET);}
	void toggle(){HAL_GPIO_TogglePin(GPIOx, GPIO_Pin);}
	void write(bool val){if(val)set();else reset();}
	bool read()
	{
		return HAL_GPIO_ReadPin(GPIOx, GPIO_Pin)==inverted?GPIO_PIN_RESET:GPIO_PIN_SET;
	}



};

class gpio_in_pin
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
	bool inverted;
public:
	gpio_in_pin(GPIO_TypeDef* GPIO, uint16_t Pin, bool inv):GPIOx(GPIO),GPIO_Pin(Pin),inverted(inv){}
	bool read()
		{
			return HAL_GPIO_ReadPin(GPIOx, GPIO_Pin)==inverted?GPIO_PIN_RESET:GPIO_PIN_SET;
		}

};
#endif /* HAL_GPIO_HPP_ */
