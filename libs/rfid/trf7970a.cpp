/*
 * trf7970a.c
 *
 *  Created on: Apr 15, 2021
 *      Author: ironcaterpillar
 */

#include <rfid/trf7970a.hpp>
#include "stm32l4xx_hal.h"
#include "main.h"
#include "spi.h"
#include "cmsis_os2.h"
#include "debug/debug.hpp"

using namespace debug;

TRF7970::TRF7970(spi_port &&spi, gpio_out_pin &&en0, gpio_out_pin &&en2) :
		spi(std::move(spi)), en0(std::move(en0)), en2(std::move(en2)) {
	spi.select();
	osDelay(2);
	spi.deselect();
	osDelay(3);
	en2.set();
	osDelay(1);
	en0.set();
	osDelay(1);
	SendCommand(SOFT_INIT);
	SendCommand(IDLE);
	osDelay(1);
	SendCommand(RESET_FIFO);
	osDelay(10);
	Get8Regs();
}
uint8_t TRF7970::form_command(uint8_t command, uint8_t read, uint8_t continios,
		uint8_t addr_command) {
	return (command ? (1 << 7) : 0) | (read ? (1 << 6) : 0)
			| (continios ? (1 << 5) : 0) | (addr_command & 0b11111);

}
void TRF7970::SendCommand(uint8_t command) {

	command = form_command(1, 0, 0, command);
	spi.tx(&command, 1);

}

uint8_t TRF7970::GetReg(uint8_t reg) {
	uint8_t command = form_command(0, 1, 0, reg);
	uint8_t rx_arr[2] = { 0 };
	uint8_t tx_arr[] = { command, 0 };
	spi.txrx(tx_arr, rx_arr, sizeof(rx_arr));
	return rx_arr[1];
}

void TRF7970::SetReg(uint8_t reg, uint8_t val) {

	uint8_t command[] = { form_command(0, 0, 0, reg), val };
	spi.tx(command, 1);
	return;
}

void TRF7970::SetDetection(uint8_t detection) {
	SetReg(TAR_DET_LVL_REG, detection);

}

uint64_t TRF7970::Get8Regs() {
	uint64_t regs = 0;
	uint8_t tx_arr[] = { form_command(0, 1, 1, 0), 0, 0, 0, 0, 0, 0, 0 };
	spi.txrx(tx_arr, (uint8_t*) &regs, sizeof(regs));
	return regs;

}

bool TRF7970::test() {
	osDelay(5);
	bool result = false;
	uint64_t val;
	if ((val = TRF7970::Get8Regs()) != 0x80ff7f00) {
		DBG::PRINT(LVL::EMCY, "TRF", "", "TEST FAIL", "ISO_REG=%X", val);
	} else {
		DBG::PRINT(LVL::INFO, "TRF", "", "TEST OK");
	}
	return result;

}
