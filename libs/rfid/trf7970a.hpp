/*
 * trf7970a.h
 *
 *  Created on: Apr 15, 2021
 *      Author: ironcaterpillar
 */

#ifndef RFID_TRF7970A_HPP_
#define RFID_TRF7970A_HPP_
#include "stdint.h"
#include "hwtesting/hwtesting.hpp"
#include "spi/spi.hpp"

class TRF7970: public AbstractHWTest {
	static constexpr auto ISO_REG = 0x01U;
	static constexpr auto SOFT_INIT = 0x03U;
	static constexpr auto RESET_FIFO = 0x0FU;
	static constexpr auto IDLE = 0x0U;
	static constexpr auto TAR_DET_LVL_REG = 0x18U;

	spi_port spi;
	gpio_out_pin en0, en2;

	static uint8_t form_command(uint8_t command, uint8_t read, uint8_t continios,
			uint8_t addr_command);
	uint8_t GetReg(uint8_t reg);
	uint64_t Get8Regs();
	void SetReg(uint8_t reg, uint8_t val);
	void SendCommand(uint8_t command);

public:
	bool test()override;
	TRF7970(spi_port && port, gpio_out_pin && en0, gpio_out_pin && en1);

	void SetDetection(uint8_t detection);
};
#endif /* RFID_TRF7970A_HPP_ */
