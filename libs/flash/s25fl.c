/*
 * s25fl.c
 *
 *  Created on: Apr 13, 2021
 *      Author: ironcaterpillar
 */

#include "stm32l4xx_hal.h"
#include "s25fl.h"
#include "stdint.h"
#include "gpio.h"
#include "quadspi.h"
#include "cmsis_os2.h"

void s25_reset(uint8_t reset) {
	HAL_GPIO_WritePin(FL_RESET_GPIO_Port, FL_RESET_Pin,
			reset ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

static void OneByteCommand(uint8_t command) {
	//if (spi_mode == SPI_MODE_QUAD)
	//return OneByteCommandQuad(command);
	QSPI_CommandTypeDef sCommand;

	/* Enable write operations ------------------------------------------ */
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = command;
	sCommand.AddressMode = QSPI_ADDRESS_NONE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode = QSPI_DATA_NONE;
	sCommand.DummyCycles = 0;
	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData = 0;

	HAL_StatusTypeDef res = HAL_QSPI_Command(&hqspi, &sCommand,
			HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
	if (res != HAL_OK) {
		//THROW_EXCEPTION_DATA(HW,ERROR,res);
	}
}
static uint32_t status = 0;
static void WriteDisableSerialFlash() {
	OneByteCommand(WRITE_DISABLE_CMD);
	status = s25_read_register(READ_STATUS_REGISTER);
	//PollStatusBit(READ_STATUS_CMD, 1, 0);
}

static void WriteEnableSerialFlash() {
	OneByteCommand(WRITE_ENABLE_CMD);
	status = s25_read_register(READ_STATUS_REGISTER);
	//PollStatusBit(READ_STATUS_CMD, 1, 1);
}
void s25_erase_flash()
{
	WriteEnableSerialFlash();
	OneByteCommand(BULK_ERASE);

}
uint64_t s25_read_jdec() {

	QSPI_CommandTypeDef sCommand;

	uint32_t data;

	sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = READ_JEDEC_CMD;
	sCommand.AddressMode = QSPI_ADDRESS_NONE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode = QSPI_DATA_1_LINE;
	sCommand.DummyCycles = 0;
	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData = 8;
	sCommand.Address = 0;

	if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		Error_Handler();
	}

	if (HAL_QSPI_Receive(&hqspi, (uint8_t*) &data, 1000) != HAL_OK) {
		Error_Handler();
	}

	return data;

}
static void s25_write_registers(uint8_t status, uint8_t config) {

	WriteEnableSerialFlash();
	uint8_t tx_arr[] = { status, config };
	QSPI_CommandTypeDef sCommand;

	sCommand.AddressSize = QSPI_ADDRESS_32_BITS;
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = PROGRAM_REGS;
	sCommand.AddressMode = QSPI_ADDRESS_NONE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode = QSPI_DATA_1_LINE;
	sCommand.DummyCycles = 0;
	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData = 2;
	sCommand.Address = 0;

	if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		Error_Handler();
	}

	if (HAL_QSPI_Transmit(&hqspi, (uint8_t*) &tx_arr, 1000) != HAL_OK) {
		Error_Handler();
	}
	WriteDisableSerialFlash();
}
void s25_enable_quad() {
	s25_write_registers(0, 2);
}
void s25_disable_quad() {
	s25_write_registers(0, 0);
}
uint16_t s25_read_rems() {
	QSPI_CommandTypeDef sCommand;

	uint16_t data;

	sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = READ_JEDEC_CMD;
	sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode = QSPI_DATA_1_LINE;
	sCommand.DummyCycles = 0;
	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData = 2;
	sCommand.Address = 1;

	if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		Error_Handler();
	}

	if (HAL_QSPI_Receive(&hqspi, (uint8_t*) &data, 1000) != HAL_OK) {
		Error_Handler();
	}

	return data;

}
uint16_t s25_read_register(uint32_t reg) {
	QSPI_CommandTypeDef sCommand;

	uint16_t data;

	sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = reg;
	sCommand.AddressMode = QSPI_ADDRESS_NONE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode = QSPI_DATA_1_LINE;
	sCommand.DummyCycles = 0;
	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData = 2;
	sCommand.Address = 0;

	if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		Error_Handler();
	}

	if (HAL_QSPI_Receive(&hqspi, (uint8_t*) &data, 1000) != HAL_OK) {
		Error_Handler();
	}

	return data;

}
void s25_erase_page(uint32_t address)
{
	WriteEnableSerialFlash();

		QSPI_CommandTypeDef sCommand;


		sCommand.AddressSize = QSPI_ADDRESS_32_BITS;
		sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
		sCommand.Instruction = ERASE_PAGE;
		sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
		sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
		sCommand.DataMode = QSPI_DATA_NONE;
		sCommand.DummyCycles = 0;
		sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
		sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
		sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
		sCommand.NbData = 0;
		sCommand.Address = address;

		if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)
				!= HAL_OK) {
			Error_Handler();
		}


		osDelay(512);
		WriteDisableSerialFlash();
}
int s25_read_flash(uint32_t addr, uint32_t count, unsigned char *ptrOut) {
	again: {
		QSPI_CommandTypeDef sCommand;

		/* Enable write operations ------------------------------------------ */
		sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
		sCommand.Instruction = READ_QUAD;
		sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
		sCommand.AddressSize = QSPI_ADDRESS_32_BITS;
		sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_4_LINES;
		sCommand.AlternateBytesSize = QSPI_ALTERNATE_BYTES_8_BITS;	// QSPI_ALTERNATE_BYTES_8_BITS;
		sCommand.AlternateBytes = 0;
		sCommand.DataMode = QSPI_DATA_4_LINES;

		sCommand.DummyCycles = 4;		//4;
		sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
		sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
		sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
		sCommand.Address = addr;
		sCommand.NbData = count;

		HAL_StatusTypeDef res = HAL_ERROR;
		int tryCount = 0;
		while ((tryCount < 10) && (res != HAL_OK)) {
			/*HAL_StatusTypeDef */res = HAL_QSPI_Command(&hqspi, &sCommand,
					1000/*HAL_QPSI_TIMEOUT_DEFAULT_VALUE*/);
			tryCount++;
		}
		if (res != HAL_OK) {
			HAL_QSPI_DeInit(&hqspi);
			HAL_QSPI_Init(&hqspi);
			//int jedec = ReadJEDECCodeFromSerialFlash();
			//if ((jedec & 0xFFFFFF) != 0x004326BF)
			//	THROW_EXCEPTION(HW, NOTFOUND);

			//InitSerialFlash(hqspi_flash);
			goto again;

		}

		res = HAL_QSPI_Receive(&hqspi, ptrOut, 1000);
		if (res != HAL_OK) {

		}

		return count;
	}
}
int s25_init()
{
	return(s25_read_jdec()!=0x4d200201);
}
void s25_program_page(uint32_t addr, uint32_t count, uint8_t *data) {

	WriteEnableSerialFlash();
	QSPI_CommandTypeDef sCommand;

	/* Enable write operations ------------------------------------------ */
	sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction = PAGE_PROGRAM;

	sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
	sCommand.AddressSize = QSPI_ADDRESS_32_BITS;
	sCommand.Address = addr;

	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.AlternateBytesSize = QSPI_ALTERNATE_BYTES_8_BITS;
	sCommand.AlternateBytes = 0;

	sCommand.DataMode = QSPI_DATA_4_LINES;
	sCommand.NbData = count;

	sCommand.DummyCycles = 0;

	sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

	HAL_StatusTypeDef res = HAL_QSPI_Command(&hqspi, &sCommand,
			HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
	if (res != HAL_OK) {
		HAL_Delay(10);
		//THROW_EXCEPTION_DATA(HW,ERROR,res);
	}

	res = HAL_QSPI_Transmit(&hqspi, data, 1000);
	if (res != HAL_OK) {
		osDelay(10);
		//THROW_EXCEPTION_DATA(HW,ERROR,res);
	}

	osDelay(1);
	//PollStatusBitQuad(READ_STATUS_CMD, 0, 0);
	WriteDisableSerialFlash();
}

