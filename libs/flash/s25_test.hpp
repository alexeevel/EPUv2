/*
 * s25_test.hpp
 *
 *  Created on: 4 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef FLASH_S25_TEST_HPP_
#define FLASH_S25_TEST_HPP_

#include "hwtesting/hwtesting.hpp"
#include "s25fl.h"
#include "main.h"
#include "debug/debug.hpp"
using namespace debug;

class S25QFlashTest : public AbstractHWTest
{
public:
	bool test() override{
		uint32_t jedec;
		if(S25_JEDEC_CODE==(jedec=s25_read_jdec())){
			DBG::PRINT(LVL::INFO, "S25 FLASH", "" , "TEST OK");
			return true;
		}
		else
		{
			DBG::PRINT(LVL::EMCY, "S25 FLASH", "" , "TEST FAIL", "JEDEC CODE=%X", jedec);
						return false;
		}
	}
};


#endif /* FLASH_S25_TEST_HPP_ */
