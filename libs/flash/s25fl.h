/*
 * s25fl.h
 *
 *  Created on: Apr 13, 2021
 *      Author: ironcaterpillar
 */

#ifndef FLASH_S25FL_H_
#define FLASH_S25FL_H_
#ifdef __cplusplus
extern "C" {
#endif

#define S25_FLASH_BLOCK_SIZE (1024*256)
#define READ_REMS_CMD 0x90U
#define READ_JEDEC_CMD 0x9FU
#define WRITE_ENABLE_CMD 0x06U
#define WRITE_DISABLE_CMD 0x04U
#define PAGE_PROGRAM 0x34U
#define READ_QUAD 0xEC
#define PROGRAM_REGS 0x01U
#define READ_CONFIG_REGISTER 0x35U
#define READ_STATUS_REGISTER 0x05U
#define ERASE_PAGE 0xDCU
#define BULK_ERASE 0x60
#define S25_JEDEC_CODE 0x4d200201

void s25_reset(uint8_t reset);
uint16_t s25_read_rems();
uint64_t s25_read_jdec();
void s25_program_page(uint32_t addr, uint32_t count, uint8_t * data);
int s25_read_flash(uint32_t addr,  uint32_t count, unsigned char *ptrOut);
void s25_enable_quad();
uint16_t s25_read_register(uint32_t reg);
void s25_erase_page(uint32_t address);
#ifdef __cplusplus
}
#endif

#endif /* FLASH_S25FL_H_ */
