/*
 * double_buffer.hpp
 *
 *  Created on: 26 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef CONTAINERS_DOUBLE_BUFFER_HPP_
#define CONTAINERS_DOUBLE_BUFFER_HPP_

#include "buffer.hpp"

namespace containers
{

class DoubleBuffer: public AbstractBuffer
{
	Buffer buffer1;
	Buffer buffer2;
	Buffer * current_buff;
	size_t got_data;
public:
	size_t get_data_size();
	DoubleBuffer(size_t size);
	virtual BufferIterator & data_iterator() override;
	void swap();
	virtual void reset()override;
	Buffer & current_buffer();
	Buffer & spare_buffer();
	virtual BufferIterator begin()override;
	virtual BufferIterator end()override;
	virtual void clear()override;
	//Buffer
};

}
#endif /* CONTAINERS_DOUBLE_BUFFER_HPP_ */
