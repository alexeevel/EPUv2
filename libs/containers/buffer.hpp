/*
 * buffer.hpp
 *
 *  Created on: 26 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef CONTAINERS_BUFFER_HPP_
#define CONTAINERS_BUFFER_HPP_
#include <memory>

namespace containers
{


class BufferIterator : public std::iterator<std::input_iterator_tag, uint8_t>
{
  uint8_t* p;
public:
  BufferIterator(uint8_t* x) :p(x) {}
  BufferIterator(const BufferIterator& mit) : p(mit.p) {}
  BufferIterator& operator++() {++p;return *this;}
  BufferIterator operator++(int) {BufferIterator tmp(*this); operator++(); return tmp;}
  bool operator==(const BufferIterator& rhs) const {return p==rhs.p;}
  bool operator!=(const BufferIterator& rhs) const {return p!=rhs.p;}
  uint8_t& operator*() {return *p;}
};

class AbstractBuffer
{
public:
	virtual BufferIterator & data_iterator()=0;
	virtual void reset()=0;
	virtual void clear()=0;
	virtual BufferIterator begin()=0;
	virtual BufferIterator end()=0;
};

class Buffer : public AbstractBuffer
{
	std::unique_ptr<uint8_t> buffer;
	BufferIterator pointer;
	size_t size;
public:
	Buffer(size_t size);
	virtual BufferIterator & data_iterator() override;
	virtual void reset()override;
	virtual BufferIterator begin()override;
	virtual BufferIterator end()override;
	virtual void clear()override;
};

}



#endif /* CONTAINERS_BUFFER_HPP_ */
