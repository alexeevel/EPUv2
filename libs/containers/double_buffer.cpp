/*
 * DoubleBuffer.cpp
 *
 *  Created on: 26 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#include "double_buffer.hpp"

namespace containers {

DoubleBuffer::DoubleBuffer(size_t size) :
		buffer1(size), buffer2(size), current_buff(&buffer1), got_data(0) {

}
BufferIterator& DoubleBuffer::data_iterator() {
	return current_buff->data_iterator();
}
void DoubleBuffer::swap() {
	got_data = &(*current_buff->data_iterator())-&(*current_buff->begin());
	current_buff = &spare_buffer();
	current_buff->reset();
}
Buffer& DoubleBuffer::current_buffer() {
	return *current_buff;
}
Buffer& DoubleBuffer::spare_buffer() {
	return current_buff == &buffer1 ? buffer2 : buffer1;
}

void DoubleBuffer::reset() {
	current_buff = &buffer1;
	buffer1.reset();
	buffer2.reset();
}

BufferIterator DoubleBuffer::begin() {
	return current_buff->begin();
}
BufferIterator DoubleBuffer::end() {
	return current_buff->end();
}

void DoubleBuffer::clear()
{
	current_buff->clear();
}
size_t DoubleBuffer::get_data_size()
{
	return got_data;
}
}
