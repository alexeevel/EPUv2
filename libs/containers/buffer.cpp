/*
 * buffer.cpp
 *
 *  Created on: 26 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#include "buffer.hpp"
#include "string.h"
namespace containers {


Buffer::Buffer(size_t size):buffer(new uint8_t[size]), pointer(&(*buffer)),size(size) {


}

BufferIterator & Buffer::data_iterator()
{
	return pointer;
}

 void Buffer::reset(){
	 pointer = &(*buffer);
 }

 BufferIterator Buffer::begin()
{
	return BufferIterator(&(*buffer));
}
 BufferIterator Buffer::end()
{
	return BufferIterator(&(*buffer)+size);
}
 void Buffer::clear()
{
	reset();
	memset(&(*buffer), 0, size);
}
}


