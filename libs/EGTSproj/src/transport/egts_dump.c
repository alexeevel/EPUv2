/*****************************************************************************/
/*                                                                           */
/* File: egts_dump.c                                                         */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Debug                                                     */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*	                                                                     */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Misceleniouse debug routines. Not a part of EGTS mandatory   */
/*   source.                                                                 */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"

#include "../include/egts.h"
#include "../include/egts_impl.h"

#include "../services/egts_services.h"

#include <stdarg.h>
#include <stdlib.h>

/******************************************************************************
*
*/

extern char deb_egts_buf[256];
extern int len_ostr;

void egts_debug_ctx(void *ctx, const char *x, ...)
{
	va_list vaArgP;
	va_start(vaArgP, x);
	int retbytes = vsnprintf((char *)&deb_egts_buf[len_ostr], sizeof(deb_egts_buf) - len_ostr, x, vaArgP);
	len_ostr += retbytes;
	va_end(vaArgP);
}
#ifdef EGTS_DBG

/******************************************************************************
* prototypes
*/

/*****************************************************************************/
/*                                                                           */
/* egts_dump_subrecord()                                                     */
/*                                                                           */
/* Description: Debug function for dump the subrecord using specified        */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: precord - subrecord                                            */
/*            psubrecord - subrecord                                         */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_subrecord(
  egts_record_t*      precord ,
  egts_subrecord_t*   psubrecord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
  );

/*****************************************************************************/
/*                                                                           */
/* egts_TELEDATA_SRC_str()                                                   */
/*                                                                           */
/* Description: Return string representation of TELEDATA SRC                 */
/*                                                                           */
/* Arguments: r - TELEDATA SRC                                               */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_TELEDATA_SRC_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case EGTS_SRC_TIMER_IGNITION        : pc =  "TIMER_IGNITION";  break;
  case EGTS_SRC_DISTANCE              : pc =  "DISTANCE";  break;
  case EGTS_SRC_ANGLE                 : pc =  "ANGLE";  break;
  case EGTS_SRC_RESPONSE              : pc =  "RESPONSE";  break;
  case EGTS_SRC_INPUT                 : pc =  "INPUT";  break;
  case EGTS_SRC_TIMER_NO_IGNITION     : pc =  "TIMER_NO_IGNITION";  break;
  case EGTS_SRC_PERIPHERIAL_OFF       : pc =  "PERIPHERIAL_OFF";  break;
  case EGTS_SRC_SPEED_TRESHOLD        : pc =  "SPEED_TRESHOLD";  break;
  case EGTS_SRC_CPU_RESTART           : pc =  "CPU_RESTART";  break;
  case EGTS_SRC_OUTPUT_OVERLOAD       : pc =  "OUTPUT_OVERLOAD";  break;
  case EGTS_SRC_TAMPER                : pc =  "TAMPER ";  break;
  case EGTS_SRC_BATTERY_POWER         : pc =  "BATTERY_POWER";  break;
  case EGTS_SRC_LOW_BATTERY           : pc =  "LOW_BATTERY";  break;
  case EGTS_SRC_ALARM                 : pc =  "ALARM";  break;
  case EGTS_SRC_CALL_REQUEST          : pc =  "CALL_REQUEST";  break;
  case EGTS_SRC_EMERGENCY             : pc =  "EMERGENCY";  break;
  case EGTS_SRC_EXT_SERVICE           : pc =  "EXT_SERVICE";  break;

  case EGTS_SRC_BATTERY_FAULT         : pc =  "BATTERY_FAULT";  break;
  case EGTS_SRC_EXTRA_ACC             : pc =  "EXTRA_ACC";  break;
  case EGTS_SRC_EXTRA_BREAK           : pc =  "EXTRA_BREAK";  break;
  case EGTS_SRC_ERROR_NAV             : pc =  "ERROR_NAV";  break;
  case EGTS_SRC_ERROR_SENSOR          : pc =  "ERROR_SENSOR";  break;
  case EGTS_SRC_ERROR_GSM_AERIAL      : pc =  "ERROR_GSM_AERIAL";  break;
  case EGTS_SRC_ERROR_NAV_AERIAL      : pc =  "ERROR_NAV_AERIAL";  break;
  case EGTS_SRC_LOW_SPEED             : pc =  "LOW_SPEED";  break;
  case EGTS_SRC_NO_IGNITION_MOVE      : pc =  "NO_IGNITION_MOVE";  break;
  case EGTS_SRC_TIMER_EMERGENCY       : pc =  "TIMER_EMERGENCY";  break;
  case EGTS_SRC_NAV_ON_OFF            : pc =  "NAV_ON_OFF";  break;
  case EGTS_SRC_UNSTABLE_NAV          : pc =  "UNSTABLE_NAV";  break;
  case EGTS_SRC_IP_CONNECT            : pc =  "IP_CONNECT";  break;
  case EGTS_SRC_UNSTABLE_GSM          : pc =  "UNSTABLE_GSM";  break;
  case EGTS_SRC_UNSTABLE_IP           : pc =  "UNSTABLE_IP";  break;
  case EGTS_SRC_MODE                  : pc =  "MODE";  break;

  default : break;
  }
  return pc;
}



/*****************************************************************************/
/*                                                                           */
/* egts_dump_record()                                                        */
/*                                                                           */
/* Description: Debug function for dump the record using specified           */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: precord - subrecord                                            */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_record( 
  egts_record_t*           precord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
  );

/*****************************************************************************/
/*                                                                           */
/* egts_result_str()                                                         */
/*                                                                           */
/* Description: Return string representation of Result Code                  */
/*                                                                           */
/* Arguments: r - Result Code                                                */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_result_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case EGTS_PC_OK	              : pc = "OK";  break;
  case EGTS_PC_IN_PROGRESS	    : pc = "IN_PROGRESS";  break;
  case EGTS_PC_UNS_PROTOCOL	    : pc = "UNS_PROTOCOL";  break;
  case EGTS_PC_DECRYPT_ERROR    : pc = "DECRYPT_ERROR";  break;
  case EGTS_PC_PROC_DENIED      : pc = "PROC_DENIED";  break;
  case EGTS_PC_INC_HEADERFORM   : pc = "INC_HEADERFORM";  break;
  case EGTS_PC_INC_DATAFORM     : pc = "INC_DATAFORM";  break;
  case EGTS_PC_UNS_TYPE         : pc = "UNS_TYPE"; break;
  case EGTS_PC_NOTEN_PARAMS     : pc = "NOTEN_PARAMS"; break;
  case EGTS_PC_DBL_PROC         : pc = "DBL_PROC"; break;
  case EGTS_PC_PROC_SRC_DENIED  : pc = "PROC_SRC_DENIED"; break;
  case EGTS_PC_HEADERCRC_ERROR  : pc = "HEADERCRC_ERROR"; break;
  case EGTS_PC_DATACRC_ERROR    : pc = "DATACRC_ERROR"; break;
  case EGTS_PC_INVDATALEN       : pc = "INVDATALEN"; break;
  case EGTS_PC_ROUTE_NFOUND     : pc = "ROUTE_NFOUND"; break;
  case EGTS_PC_ROUTE_CLOSED     : pc = "ROUTE_CLOSED"; break;
  case EGTS_PC_ROUTE_DENIED     : pc = "ROUTE_DENIED"; break;
  case EGTS_PC_INVADDR          : pc = "INVADDR"; break;
  case EGTS_PC_TTLEXPIRED       : pc = "TTLEXPIRED"; break;
  case EGTS_PC_NO_ACK           : pc = "NO_ACK"; break;
  case EGTS_PC_OBJ_NFOUND       : pc = "OBJ_NFOUND"; break;
  case EGTS_PC_EVNT_NFOUND      : pc = "EVNT_NFOUND"; break;
  case EGTS_PC_SRVC_NFOUND      : pc = "SRVC_NFOUND"; break;
  case EGTS_PC_SRVC_DENIED      : pc = "SRVC_DENIED"; break;
  case EGTS_PC_SRVC_UNKN        : pc = "SRVC_UNKN"; break;
  case EGTS_PC_AUTH_DENIED      : pc = "AUTH_DENIED"; break;
  case EGTS_PC_ALREADY_EXISTS   : pc = "ALREADY_EXISTS"; break;
  case EGTS_PC_ID_NFOUND        : pc = "ID_NFOUND"; break;
  case EGTS_PC_INC_DATETIME     : pc = "INC_DATETIME"; break;
  case EGTS_PC_IO_ERROR         : pc = "IO_ERROR"; break;
  case EGTS_PC_NO_RES_AVAIL     : pc = "NO_RES_AVAIL"; break;
  case EGTS_PC_MODULE_FAULT     : pc = "MODULE_FAULT"; break;
  case EGTS_PC_MODULE_PWR_FLT   : pc = "MODULE_PWR_FLT"; break;
  case EGTS_PC_MODULE_PROC_FLT  : pc = "MODULE_PROC_FLT"; break;
  case EGTS_PC_MODULE_SW_FLT    : pc = "MODULE_SW_FLT"; break;
  case EGTS_PC_MODULE_FW_FLT    : pc = "MODULE_FW_FLT"; break;
  case EGTS_PC_MODULE_IO_FLT    : pc = "MODULE_IO_FLT"; break;
  case EGTS_PC_MODULE_MEM_FLT   : pc = "MODULE_MEM_FLT"; break;
  case EGTS_PC_TEST_FAILED      : pc = "TEST_FAILED"; break;
  default :
    break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_SST_str()                                                            */
/*                                                                           */
/* Description: Return string representation of Service Statement            */
/*                                                                           */
/* Arguments: r - Service Statement                                          */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_SST_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case EGTS_SST_IN_SERVICE	    : pc =  "IN_SERVICE"; break;
  case EGTS_SST_OUT_OF_SERVICE  : pc =  "OUT_OF_SERVICE"; break;
  case EGTS_SST_DENIED          : pc =  "DENIED"; break;
  case EGTS_SST_NO_CONF         : pc =  "NO_CONF"; break;
  case EGTS_SST_TEMP_UNAVAIL    : pc =  "TEMP_UNAVAIL"; break;
  default:  break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CT_str()                                                             */
/*                                                                           */
/* Description: Return string representation of Command Type                 */
/*                                                                           */
/* Arguments: r - Command Type                                               */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_CT_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case CT_COMCONF : pc =  "COMCONF"; break;
  case CT_MSGCONF : pc =  "MSGCONF"; break;
  case CT_MSGFROM : pc =  "MSGFROM"; break;
  case CT_MSGTO   : pc =  "MSGTO"; break;
  case CT_COM     : pc =  "COM"; break;
  case CT_DELCOM  : pc =  "DELCOM"; break;
  case CT_SUBREQ  : pc =  "SUBREQ"; break;
  case CT_DELIV   : pc =  "DELIV"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CT_str()                                                             */
/*                                                                           */
/* Description: Return string representation of Command Confirmation Type    */
/*                                                                           */
/* Arguments: r - Command Confirmation Type                                  */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_CC_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case CC_OK      : pc =  "OK"; break;
  case CC_ERROR   : pc =  "ERROR"; break;
  case CC_ILL     : pc =  "ILL"; break;
  case CC_DEL     : pc =  "DEL"; break;
  case CC_NFOUND  : pc =  "NFOUND"; break;
  case CC_NCONF   : pc =  "NCONF"; break;
  case CC_INPROG  : pc =  "INPROG"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CHS_str()                                                            */
/*                                                                           */
/* Description: Return string representation of Charset                      */
/*                                                                           */
/* Arguments: r - Charset                                                    */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_CHS_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case CHS_CP_1251  : pc =  "CP_1251"; break;
  case CHS_ANSI     : pc =  "ANSI"; break;
  case CHS_BIN      : pc =  "BIN"; break;
  case CHS_LATIN_1  : pc =  "LATIN_1"; break;
  case CHS_BIN_1    : pc =  "BIN_1"; break;
  case CHS_JIS      : pc =  "JIS"; break;
  case CHS_CYRILIC  : pc =  "CYRILIC"; break;
  case CHS_HEBREW   : pc =  "HEBREW"; break;
  case CHS_UCS2     : pc =  "UCS2"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CHS_str()                                                            */
/*                                                                           */
/* Description: Return string representation of command Action               */
/*                                                                           */
/* Arguments: r - Action                                                     */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_ACT_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case ACT_PARAM  : pc =  "PARAM"; break;
  case ACT_GET    : pc =  "GET"; break;
  case ACT_SET    : pc =  "SET"; break;
  case ACT_ADD    : pc =  "ADD"; break;
  case ACT_DEL    : pc =  "DEL"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_OT_str()                                                             */
/*                                                                           */
/* Description: Return string representation of Object Type                  */
/*                                                                           */
/* Arguments: r - Object Type                                                */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_OT_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case OT_FIRMWARE  : pc =  "FIRMWARE"; break;
  case OT_CONFIG    : pc =  "CONFIG"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_MT_str()                                                             */
/*                                                                           */
/* Description: Return string representation of Module Type                  */
/*                                                                           */
/* Arguments: r - Module Type                                                */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_MT_str( u8 r )
{
  const char* pc = "?";  
  switch (r)
  {
  case MT_AUX   : pc =  "AUX"; break;
  case MT_MAIN  : pc =  "MAIN"; break;
  default: break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CLT_str()                                                            */
/*                                                                           */
/* Description: Return string representation of Call Type                    */
/*                                                                           */
/* Arguments: r - Call Type                                                  */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_CLT_str( u8 r )
{
  const char* pc = "?";    
  switch (r)
  {
  case CLT_EMERG  : pc =  "EMERG"; break;
  case CLT_TEST   : pc =  "TEST"; break;
  default :  break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CLT_str()                                                            */
/*                                                                           */
/* Description: Return string representation of Activation Type              */
/*                                                                           */
/* Arguments: r - Activation Type                                            */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/


const char* egts_ACType_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case ACT_MANUAL : pc =  "MANUAL";  break;
  case ACT_AUTO   : pc =  "AUTO"; break;
  default : break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* egts_CLT_str()                                                            */
/*                                                                           */
/* Description: Return string representation of Test Mode                    */
/*                                                                           */
/* Arguments: r - Test Mode                                                  */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

const char* egts_TEST_MODE_str( u8 r )
{
  const char* pc = "?";
  switch (r)
  {
  case TEST_MODE_SEQ_ALL_TESTS    : pc = "SEQ_ALL_TESTS"; break;
  case TEST_MODE_CALL_CENTER      : pc = "CALL_CENTER"; break;
  case TEST_MODE_EXT_CALL_CENTER  : pc = "EXT_CALL_CENTER"; break;
  case TEST_MODE_MIC              : pc = "MIC"; break;
  case TEST_MODE_AUDIO            : pc = "AUDIO"; break;
  case TEST_MODE_IGNITION         : pc = "IGNITION"; break;
  case TEST_MODE_USER_INTERFACE   : pc = "USER_INTERFACE"; break;
  case TEST_MODE_ACCUM            : pc = "ACCUM"; break;
  case TEST_MODE_SENSOR           : pc = "SENSOR"; break;
  default:  break;
  }
  return pc;
}

/*****************************************************************************/
/*                                                                           */
/* estate_dump_string()                                                      */
/*                                                                           */
/* Description: Debug helper for dump string value                           */
/*                                                                           */
/* Arguments: p_str - string to dump                                         */
/*            len - string length                                            */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  estate_dump_string(
  u8*   p_str , 
  u32   len ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
)
{
  u32 i,l;
  u8* puc;

  l=0U;
  i=0U;
  for ( puc = (u8*)p_str; i < len; i++ )
  {
    if ( ( *puc < (u8)' ' ) || ( *puc > (u8)127 ) )
    {
      (*fn_dump)( ctx , "\\x%02X" , *puc++);
      l += 4U;
    }
    else
    {
      (*fn_dump)( ctx , "%c" , *puc++);
      l += 1U;
    }
    if ( l >= 76U )
    {
      (*fn_dump)( ctx , "\r\n");
      l = 0U;
    }
  }
  (*fn_dump)( ctx , "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* estate_dump_string_z()                                                    */
/*                                                                           */
/* Description: Debug helper for dump zero-terminated string value           */
/*                                                                           */
/* Arguments: p_str - string to dump                                         */
/*            len - maximum string length                                    */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  estate_dump_string_z(
  u8*   p_str , 
  u32   len ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) )
{
  u32 i,l;
  u8* puc;

  l=0U;
  i=0U;
  for ( puc = (u8*)p_str; (i < len) && (*puc != 0U); i++ )
  {
    if ( (*puc < (u8)' ') || ( *puc > 127U ) )
    {
      (*fn_dump)( ctx , "\\x%02X" , *puc++);
      l += 4U;
    }
    else
    {
      (*fn_dump)( ctx , "%c" , *puc++);
      l += 1U;
    }
    if ( l >= 76U )
    {
      (*fn_dump)( ctx , "\r\n");
      l = 0U;
    }
  }
  (*fn_dump)( ctx , "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* egts_dump_FIRMWARE_DATA_header()                                          */
/*                                                                           */
/* Description: Debug helper for dump egts_FIRMWARE_DATA_header_t stucture   */
/*                                                                           */
/* Arguments: ctx - context of callback call                                 */
/*            pv_subrecord - pointer to egts_FIRMWARE_DATA_header_t          */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  egts_dump_FIRMWARE_DATA_header( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  egts_FIRMWARE_DATA_header_t* psrec = (egts_FIRMWARE_DATA_header_t*)pv_subrecord;
  (*afn_dump)( ctx , "SR_DATA_header:\r\n");
  (*afn_dump)( ctx , "  Object Type                      OT: %u (%s)\r\n"     , psrec->OT, egts_OT_str(psrec->OT) );
  (*afn_dump)( ctx , "  Module Type                      MT: %u (%s)\r\n"     , psrec->MT, egts_MT_str(psrec->MT) );
  (*afn_dump)( ctx , "  Component or Module Identifier   CMI: %u (%02Xh)\r\n" , psrec->CMI, psrec->CMI );
  (*afn_dump)( ctx , "  Version                          VER: %u (%04Xh)\r\n" , psrec->VER, psrec->VER );
  (*afn_dump)( ctx , "  Whole Object Signature           WOS: %u (%04Xh)\r\n" , psrec->WOS, psrec->WOS );
  if ( psrec->FN_len != 0U )
  {
    (*afn_dump)( ctx , "  File Name                        FN:\r\n");
    estate_dump_string( psrec->FN, (u32)psrec->FN_len, ctx, afn_dump );
  }
  else
  {
    (*afn_dump)( ctx , "  File Name                        FN: -- \r\n");
  }

}

/******************************************************************************
*
*/

static
void  egts_dump_subrecord(
  egts_record_t*      precord ,
  egts_subrecord_t*   psubrecord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  const egts_subrecord_handler_t* chandler = NULL;

  (*fn_dump)( ctx , "SUBRECORD:\r\n" );
  chandler = egts_find_subrecord_handler( precord->record.RST , psubrecord->subrecord.SRT );
  if ( !chandler )
  {
    (*fn_dump)( ctx , "---unknown subrecord--\r\n" );
    return;
  }

  if ( psubrecord->SRD ) {
    chandler->fn_dump( ctx , psubrecord->SRD , fn_dump );
  }  else {
    (*fn_dump)( ctx , "---no data--\r\n" );
  }

}


/*****************************************************************************/
/*                                                                           */
/* egts_dump_record()                                                        */
/*                                                                           */
/* Description: Debug function for dump the record using specified           */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: precord - subrecord                                            */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_record( 
  egts_record_t*           precord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
  )
{

  egts_subrecord_t*            psubrecord;
  u16                          i;
  u8  s,n,h,d,m;
  u32 y;

  (*fn_dump)( ctx , "RECORD:\r\n" );
  (*fn_dump)( ctx , "Record Length           RL: %u\r\n", precord->record.RL );
  (*fn_dump)( ctx , "Record Number           RN: %u\r\n", precord->record.RN );

  (*fn_dump)( ctx , " * Source Service On Device           SSOD: %u\r\n", precord->record.SSOD );
  (*fn_dump)( ctx , " * Recipient Service On Device        RSOD: %u\r\n", precord->record.RSOD );
  (*fn_dump)( ctx , " * Group                               GRP: %u\r\n", precord->record.GRP );
  (*fn_dump)( ctx , " * Record Processing Priority          RPP: %u\r\n", precord->record.RPP );
  (*fn_dump)( ctx , " * Time Field Exists                  TMFE: %u\r\n", precord->record.TMFE );
  (*fn_dump)( ctx , " * Event ID Field  Exists             EVFE: %u\r\n", precord->record.EVFE );
  (*fn_dump)( ctx , " * Object ID  Field Exists            OBFE: %u\r\n", precord->record.OBFE );

  if ( precord->record.OBFE != 0U ) {
    (*fn_dump)( ctx , "Object Identifier      OID: %u\r\n", precord->record.OID );
  } else {
    (*fn_dump)( ctx , "Object Identifier      OID: -\r\n" );
  }
  if ( precord->record.EVFE != 0U ) {
    (*fn_dump)( ctx , "Event Identifier      EVID: %u\r\n", precord->record.EVID );
  } else {
    (*fn_dump)( ctx , "Event Identifier      EVID: -\r\n" );
  }
  if ( precord->record.TMFE != 0U ) {
    egts_get_tm( precord->record.TM , &s, &n, &h, &d, &m, &y );
    (*fn_dump)( ctx , "Time                    TM: %02u.%02u.%04u %02u:%02u:%02u  %08h\r\n",
       d, m, y, h, n, s,  
       precord->record.TM );
  } else {
    (*fn_dump)( ctx , "Time                    TM: -\r\n" );
  }
  (*fn_dump)( ctx , "Source Service Type    SST: %u\r\n", precord->record.SST );
  (*fn_dump)( ctx , "Recipient Service Type RST: %u\r\n", precord->record.RST );

  if ( ( precord->nsubrecords != 0U ) && 
       ( precord->psubrecords != NULL ) )
  {
    psubrecord = precord->psubrecords;
    for ( i = 0U; ( psubrecord != NULL ) && ( i < precord->nsubrecords ); i++ )
    {
      egts_dump_subrecord( 
        precord ,
        psubrecord , 
        ctx , fn_dump );
      psubrecord++;
    }
  }

}
  
/*****************************************************************************/
/*                                                                           */
/* estate_dump_packet()                                                      */
/*                                                                           */
/* Description: Debug function for dump the whole packet using specified     */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: pheader - transport header                                     */
/*            signed_up - signature mark, 1/0                                */
/*            presponce - responce data                                      */
/*            precords - array of records                                    */
/*            nrecords - record count                                        */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  estate_dump_packet( 
  egts_header_t*            pheader , 
  u8                        signed_up ,
  egts_responce_header_t*   presponce ,
  egts_record_t*            precords ,
  u16                       nrecords ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  (*fn_dump)( ctx , ">>packet received:\r\n" );
  (*fn_dump)( ctx , "Protocol Version     PRV: %02Xh \r\n", &pheader->PRV );
  (*fn_dump)( ctx , "Security Key ID     SKID: %02Xh \r\n", pheader->SKID );
  (*fn_dump)( ctx , "Prefix               PRF: %02Xh \r\n", pheader->PRF );
  (*fn_dump)( ctx , "Route                RTE: %u \r\n", pheader->RTE );
  (*fn_dump)( ctx , "Encryption Algorithm ENA: %02Xh \r\n", pheader->ENA );
  (*fn_dump)( ctx , "Compressed           CMP: %u\r\n", pheader->CMP );
  (*fn_dump)( ctx , "Priority              PR: %u \r\n", pheader->PR );
  (*fn_dump)( ctx , "Header Length         HL: %u \r\n", pheader->HL );
  (*fn_dump)( ctx , "Header Encoding       HE: %02Xh \r\n", pheader->HE );

  (*fn_dump)( ctx , "Frame Data Length    FDL: %u\r\n", pheader->FDL );
  (*fn_dump)( ctx , "Packet Identifier    PID: %u\r\n", pheader->PID );
  (*fn_dump)( ctx , "Packet Type           PT: %04Xh \r\n", pheader->PT );
  if ( pheader->RTE != 0U )
  {
  (*fn_dump)( ctx , "Peer Address         PRA: %04Xh \r\n", pheader->PRA );
  (*fn_dump)( ctx , "Recipient Address    RCA: %04Xh \r\n", pheader->RCA );
  (*fn_dump)( ctx , "Time To Live         TTL: %u \r\n", pheader->TTL );
  }
  else
  {
  (*fn_dump)( ctx , "Peer Address         PRA: - \r\n" );
  (*fn_dump)( ctx , "Recipient Address    RCA: - \r\n" );
  (*fn_dump)( ctx , "Time To Live         TTL: - \r\n" );
  }
  (*fn_dump)( ctx , "Header Check Sum     HCS: %02Xh \r\n", pheader->HCS );

  (*fn_dump)( ctx , "-------------------------\r\n");
  switch ( pheader->PT  )
  {
  case EGTS_PT_RESPONSE:
  (*fn_dump)( ctx , " PT_RESPONSE\r\n");
    if ( presponce )
    {
  (*fn_dump)( ctx , "Response Packet ID   PID: %u\r\n" ,  presponce->RPID );
  (*fn_dump)( ctx , "Processing Result     PR: %u (%s)\r\n" ,  presponce->PR , egts_result_str(presponce->PR) );
    }
    else
    {
  (*fn_dump)( ctx , "Response Packet ID  RPID: error\r\n" );
  (*fn_dump)( ctx , "Processing Result     PR: error\r\n" );
    }
    break;
  case EGTS_PT_APPDATA:
  (*fn_dump)( ctx , " PT_APPDATA\r\n");
    break;
  case EGTS_PT_SIGNED_APPDATA:
  (*fn_dump)( ctx , " PT_SIGNED_APPDATA\r\n");
  (*fn_dump)( ctx , "Signature Length    SIGL: ...\r\n" );
  (*fn_dump)( ctx , "Signature Data      SIGD: ...\r\n" );
  (*fn_dump)( ctx , "Sign-up check: %s\r\n" , (signed_up!=(u8)0) ? "PASSED" : "FAILED" );
    break;
  default:
  (*fn_dump)( ctx , " PT_? (%u)\r\n" , pheader->PT );
    break;
  }
  (*fn_dump)( ctx , "-------------------------\r\n");

  
  if ( ( precords != NULL ) && 
       ( nrecords != 0U ) )
  {
    while ( 0U != nrecords-- ) {
      egts_dump_record( precords++ , ctx , fn_dump );
    }
  }
  else {
    (*fn_dump)( ctx , "Frame Data            FD: error\r\n");
  }


  (*fn_dump)( ctx , ">>\r\n\r\n");

}

void this_printf_dump( void* ctx , const char* fmt , ... )
{
	(void)ctx;
	va_list argList;
	va_start(argList, fmt);
    vprintf(fmt, argList);
    va_end(argList);
}


const char* egts_service_str( u8 r )
{
	const char* SSTtext = "UNKNOWN";
	switch (r)
	{
	case EGTS_AUTH_SERVICE:
		SSTtext = "AUTH_SERVICE";
		break;
	case EGTS_TELEDATA_SERVICE:
		SSTtext = "TELEDATA_SERVICE";
		break;
	case EGTS_COMMANDS_SERVICE:
		SSTtext = "COMMANDS_SERVICE";
		break;
	case EGTS_FIRMWARE_SERVICE:
		SSTtext = "FIRMWARE_SERVICE";
		break;
	case EGTS_ECALL_SERVICE:
		SSTtext = "ECALL_SERVICE";
		break;
	default:
		break;
	}
	return SSTtext;
}

/*****************************************************************************/
/*                                                                           */
/* estate_dump_string()                                                      */
/*                                                                           */
/* Description: Debug helper for dump string value                           */
/*                                                                           */
/* Arguments: p_str - string to dump                                         */
/*            len - string length                                            */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  estate_short_dump_string(
  u8*   p_str ,
  u32   len ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... )
)
{
	u32 i,l;
	u8* puc;
	l=0U;
	i=0U;
	for ( puc = (u8*)p_str; i < len; i++ )
	{
		if ( ( *puc < (u8)' ' ) || ( *puc > (u8)127 ) )
		{
			(*fn_dump)( ctx , "\\x%02X" , *puc++);
			l += 4U;
		}
		else
		{
			(*fn_dump)( ctx , "%c" , *puc++);
			l += 1U;
		}
		if ( l >= 76U )
		{
			(*fn_dump)( ctx , "\r\n");
			l = 0U;
		}
	}
	(*fn_dump)( ctx , " ");
}

static void  egts_short_dump_subrecord(
  egts_record_t*      precord ,
  egts_subrecord_t*   psubrecord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... )
  )
{
	const egts_subrecord_handler_t* chandler = NULL;
	chandler = egts_find_subrecord_handler( precord->record.RST , psubrecord->subrecord.SRT );
	if ( !chandler )
	{
		(*fn_dump)( ctx , "---unknown subrecord--\r\n" );
		return;
	}
	if ( psubrecord->SRD )
	{
		chandler->fn_dump( ctx , psubrecord->SRD , fn_dump );
	}
	else
	{
		(*fn_dump)( ctx , "---no data--\r\n" );
	}
}

/*****************************************************************************/
/*                                                                           */
/* egts_short_dump_record()                                                        */
/*                                                                           */
/* Description: Debug function for dump the record using specified           */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: precord - subrecord                                            */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

static void  egts_short_dump_record(
  egts_record_t*           precord ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... )
  )
{
	egts_subrecord_t*            psubrecord;
	u16                          i;
	u8  s,n,h,d,m;
	u32 y;
	char *SSTtext = egts_service_str(precord->record.SST);
	char *RSTtext = egts_service_str(precord->record.RST);
	(*fn_dump)( ctx , "Record Num: %u Length: %u ", precord->record.RN, precord->record.RL );
	if ( precord->record.TMFE != 0U )
	{
		egts_get_tm( precord->record.TM , &s, &n, &h, &d, &m, &y );
		(*fn_dump)( ctx , "Time: %02u.%02u.%04u %02u:%02u:%02u \r\n", d, m, y, h, n, s );
	}
	(*fn_dump)( ctx , "SST: %s RST: %s \r\n", SSTtext, RSTtext );
	if ( ( precord->nsubrecords != 0U ) && ( precord->psubrecords != NULL ) )
	{
		psubrecord = precord->psubrecords;
		for ( i = 0U; ( psubrecord != NULL ) && ( i < precord->nsubrecords ); i++ )
		{
			(*fn_dump)( ctx , "SUBREC[%i] ", i );
			egts_short_dump_subrecord(precord, psubrecord, ctx, fn_dump);
			psubrecord++;
		}
	}
}


/*****************************************************************************/
/*                                                                           */
/* estate_short_dump_packet()                                                      */
/*                                                                           */
/* Description: Debug function for dump the whole packet using specified     */
/*   dump callback                                                           */
/*                                                                           */
/* Arguments: pheader - transport header                                     */
/*            signed_up - signature mark, 1/0                                */
/*            presponce - responce data                                      */
/*            precords - array of records                                    */
/*            nrecords - record count                                        */
/*            ctx - context of callback call                                 */
/*            fn_dump - callback for debug dump                              */
/*                                                                           */
/* Return:    string                                                         */
/*                                                                           */
/* Other:     function used internally for debug                             */
/*                                                                           */
/*****************************************************************************/

void  estate_short_dump_packet(
  egts_header_t*            pheader ,
  u8                        signed_up ,
  egts_responce_header_t*   presponce ,
  egts_record_t*            precords ,
  u16                       nrecords ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... )
  )
{
	char *pheaderPR = "Error";
	switch (pheader->PT)
	{
	case EGTS_PT_RESPONSE:
		pheaderPR = "RESPONSE";
		break;
	case EGTS_PT_APPDATA:
		pheaderPR = "APPDATA";
		break;
	case EGTS_PT_SIGNED_APPDATA:
		pheaderPR = "APPDATA_SIGNED";
		break;
	default:
		break;
	}

	(*fn_dump)( ctx , "Packet HL: %u FDL: %u PID: %u PT: %s \r\n", pheader->HL, pheader->FDL, pheader->PID, pheaderPR );
	switch ( pheader->PT  )
	{
		case EGTS_PT_RESPONSE:
			if (presponce)
				(*fn_dump)( ctx , "Resp PID: %u Result: %u (%s)\r\n", presponce->RPID, presponce->PR , egts_result_str(presponce->PR));
			else
				(*fn_dump)( ctx , "Resp PID: ERROR Result: ERROR\r\n");
			break;
		case EGTS_PT_SIGNED_APPDATA:
#if 0
			(*fn_dump)( ctx , "Sign Length: ...\r\n" );
			(*fn_dump)( ctx , "Signature Data      SIGD: ...\r\n" );
#endif
			(*fn_dump)( ctx , "Sign-up check: %s\r\n" , (signed_up!=(u8)0) ? "PASSED" : "FAILED" );
			break;
		default:
			break;
	}
	if ( ( precords != NULL ) && ( nrecords != 0U ) )
	{
		while ( 0U != nrecords-- )
		{
			egts_short_dump_record( precords++ , ctx , fn_dump );
		}
	}
	else
	{
		(*fn_dump)( ctx , "Frame Data            FD: error\r\n");
	}
	(*fn_dump)( ctx , ">>\r\n\r\n");
}

/******************************************************************************
*
*/


#endif


