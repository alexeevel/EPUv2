/*****************************************************************************/
/*                                                                           */
/* File: egts_commands.h                                                     */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service COMMANDS                                          */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC «Navigation-information systems», 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for service COMMANDS         */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#ifndef egts_command_h
#define egts_command_h

/******************************************************************************
*
*/

extern const egts_subrecord_handler_t  chandler_COMMANDS_COMMAND_DATA;

/******************************************************************************
*
*/

extern const egts_service_handler_t    cservice_COMMANDS;

/******************************************************************************
*
*/


#endif




