/*****************************************************************************/
/*                                                                           */
/* File: egts_teledata_pos_data.c                                                   */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for packet                   */
/*   EGTS_SR_ACCEL_DATA, service ECALL, implementaion                        */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"

#include "../../include/egts.h"
#include "../../include/egts_impl.h"
#include "../../transport/egts_guts.h"

#include "../egts_services.h"
#include "../egts_service_handler.h"
#include "egts_teledata.h"

/******************************************************************************
* prototypes
*/


/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_EXT_POS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_EXT_POS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_EXT_POS_DATA( void* pv_subrecord );

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_EXT_POS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_EXT_POS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_EXT_POS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz );

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_EXT_POS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_EXT_POS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_EXT_POS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz );

#ifdef EGTS_DBG

/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_EXT_POS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_EXT_POS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_EXT_POS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 );

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_EXT_POS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_EXT_POS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_EXT_POS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) ); 
    
#endif
    
/******************************************************************************
* implementations
*/

/*****************************************************************************/
/*                                                                           */
/* egts_get_TELEDATA_EXT_POS_DATA_bits()                                               */
/*                                                                           */
/* Description: Collect TELEDATA_EXT_POS_DATA subrecord bitfields                 */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*                                                                           */
/* Return:    bitfields byte                                                 */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u8    egts_get_TELEDATA_EXT_POS_DATA_bits( egts_TELEDATA_EXT_POS_DATA_t* ps )
{
  u8 b = 0U;
  b |= ( ps->NSFE & 0x01U );
  b <<= 1U;
  b |= ( ps->SFE & 0x01U );
  b <<= 1U;
  b |= ( ps->PFE & 0x01U );
  b <<= 1U;
  b |= ( ps->HFE & 0x01U );
  b <<= 1U;
  b |= ( ps->VFE & 0x01U );
  return b;
}

/*****************************************************************************/
/*                                                                           */
/* egts_set_TELEDATA_EXT_POS_DATA_bits()                                               */
/*                                                                           */
/* Description: Set TELEDATA_EXT_POS_DATA subrecord bitfields                     */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*            b - bitfields byte                                             */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
void  egts_set_TELEDATA_EXT_POS_DATA_bits( egts_TELEDATA_EXT_POS_DATA_t* ps , u8 b )
{
  ps->VFE   = b & 0x01U;
  b >>= 1U;
  ps->HFE   = b & 0x01U;
  b >>= 1U;
  ps->PFE  = b & 0x01U;
  b >>= 1U;
  ps->SFE   = b & 0x01U;
  b >>= 1U;
  ps->NSFE   = b & 0x01U;
}

/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_EXT_POS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_EXT_POS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_EXT_POS_DATA( void* pv_subrecord )
{
	egts_TELEDATA_EXT_POS_DATA_t* psrec = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord;
  u16 s = 0U;

  if ( psrec->VFE )
    s += sizeof(u16); /* VDOP */

  if ( psrec->HFE )
    s += sizeof(u16); /* HDOP */

  if ( psrec->PFE )
    s += sizeof(u16); /* PDOP */

  if ( psrec->SFE )
    s += sizeof(u8); /* SAT */

  if ( psrec->NSFE )
    s += sizeof(u16); /* NS */

  return s;
}

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_EXT_POS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_EXT_POS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_EXT_POS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz )
{
  egts_TELEDATA_EXT_POS_DATA_t* psrec = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord;

  (void)SRL;

  if ( *pbuf_sz > 0U )  {
	  egts_set_TELEDATA_EXT_POS_DATA_bits( psrec , **ppbuf );
    (*ppbuf)++;
    (*pbuf_sz)--;
  } else {
    return -1;
  }

  if ( psrec->VFE )
	  EGTS_READ(psrec->VDOP,*ppbuf,*pbuf_sz)

  if ( psrec->HFE )
	  EGTS_READ(psrec->HDOP,*ppbuf,*pbuf_sz)

  if ( psrec->PFE )
	  EGTS_READ(psrec->PDOP,*ppbuf,*pbuf_sz)

  if ( psrec->SFE )
	  EGTS_READ(psrec->SAT,*ppbuf,*pbuf_sz)

  if ( psrec->NSFE )
	  EGTS_READ(psrec->NS,*ppbuf,*pbuf_sz)

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_EXT_POS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_EXT_POS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_EXT_POS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz )
{
  egts_TELEDATA_EXT_POS_DATA_t* psrec = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord;

  if ( *pbuf_sz > 0U ) {
    *(*ppbuf)++ = egts_get_TELEDATA_EXT_POS_DATA_bits( psrec );
    (*pbuf_sz)--;
  } else {
    return -1;
  }

  if ( psrec->VFE )
	  EGTS_WRITE(psrec->VDOP,*ppbuf,*pbuf_sz)

  if ( psrec->HFE )
	  EGTS_WRITE(psrec->HDOP,*ppbuf,*pbuf_sz)

  if ( psrec->PFE )
	  EGTS_WRITE(psrec->PDOP,*ppbuf,*pbuf_sz)

  if ( psrec->SFE )
	  EGTS_WRITE(psrec->SAT,*ppbuf,*pbuf_sz)

  if ( psrec->NSFE )
	  EGTS_WRITE(psrec->NS,*ppbuf,*pbuf_sz)

  return 0;
}


/******************************************************************************
*
*/

#ifdef EGTS_DBG


/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_EXT_POS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_EXT_POS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_EXT_POS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 )
{
  egts_TELEDATA_EXT_POS_DATA_t* psrec1 = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord1;
  egts_TELEDATA_EXT_POS_DATA_t* psrec2 = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord2;

  (void)ctx;

  if ( psrec1->VFE != psrec2->VFE ) {
    egts_dbg_printf("inequieal VFE");
    return -1;
  }
  
  if ( psrec1->HFE != psrec2->HFE ) {
    egts_dbg_printf("inequieal HFE");
    return -1;
  }

  if ( psrec1->PFE != psrec2->PFE ) {
    egts_dbg_printf("inequieal PFE");
    return -1;
  }

  if ( psrec1->SFE != psrec2->SFE ) {
    egts_dbg_printf("inequieal SFE");
    return -1;
  }

  if ( psrec1->NSFE != psrec2->NSFE ) {
    egts_dbg_printf("inequieal NSFE");
    return -1;
  }

  if ( psrec1->VFE )
  {
	  if ( psrec1->VDOP != psrec2->VDOP ) {
	    egts_dbg_printf("inequieal VDOP");
	    return -1;
	  }
  }

  if ( psrec1->HFE )
  {
	  if ( psrec1->HDOP != psrec2->HDOP ) {
	    egts_dbg_printf("inequieal HDOP");
	    return -1;
	  }
  }

  if ( psrec1->PFE )
  {
	  if ( psrec1->PDOP != psrec2->PDOP ) {
	    egts_dbg_printf("inequieal PDOP");
	    return -1;
	  }
  }

  if ( psrec1->SFE )
  {
	  if ( psrec1->SAT != psrec2->SAT ) {
	    egts_dbg_printf("inequieal SAT");
	    return -1;
	  }
  }

  if ( psrec1->NSFE )
  {
	  if ( psrec1->NS != psrec2->NS ) {
	    egts_dbg_printf("inequieal NS");
	    return -1;
	  }
  }

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_EXT_POS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_EXT_POS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_EXT_POS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  egts_TELEDATA_EXT_POS_DATA_t* psrec = (egts_TELEDATA_EXT_POS_DATA_t*)pv_subrecord;
  (*afn_dump)( ctx , "TELEDATA_EXT_POS_DATA:\n");
  (*afn_dump)( ctx , "  FLAGS: NSFE= %u, SFE= %u, PFE= %u, HFE= %u, VFE= %u\n" ,
     psrec->NSFE, psrec->SFE, psrec->PFE, psrec->HFE, psrec->VFE );

  if ( psrec->VFE )
  {
	  (*afn_dump)( ctx , "  Vertical Dilution of Precision   VDOP: %u (%08Xh)\n" , psrec->VDOP, psrec->VDOP );
  }

  if ( psrec->HFE )
  {
	  (*afn_dump)( ctx , "  Horizontal Dilution of Precision  HDOP: %u (%08Xh)\n" , psrec->HDOP, psrec->HDOP );
  }

  if ( psrec->PFE )
  {
	  (*afn_dump)( ctx , "  Position Dilution of Precision    PDOP: %u (%08Xh)\n" , psrec->PDOP, psrec->PDOP );
  }

  if ( psrec->SFE )
  {
	  (*afn_dump)( ctx , "  Satellites                        SAT: %u (%08Xh)\n" , psrec->SAT, psrec->SAT );
  }

  if ( psrec->NSFE )
  {
	  (*afn_dump)( ctx , "  Navigation System                 NS: %u (%08Xh)\n" , psrec->NS, psrec->NS );
  }

}

#endif

/******************************************************************************
*
*/

const egts_subrecord_handler_t  chandler_TELEDATA_EXT_POS_DATA =
{
  /* SRT */             (u8)EGTS_SR_EXT_POS_DATA ,
  /* subrecord_size */  (u16)sizeof(egts_TELEDATA_EXT_POS_DATA_t) ,
  /* fn_get_size */     egts_get_size_TELEDATA_EXT_POS_DATA ,
  /* fn_read */         egts_read_TELEDATA_EXT_POS_DATA ,
  /* fn_write */        egts_write_TELEDATA_EXT_POS_DATA ,
#ifdef EGTS_DBG
  /* fs_is_equial */    egts_is_equial_TELEDATA_EXT_POS_DATA ,
  /* fn_dump */         egts_dump_TELEDATA_EXT_POS_DATA
#endif
};


/******************************************************************************
*
*/
