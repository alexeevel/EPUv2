/*****************************************************************************/
/*                                                                           */
/* File: egts_teledata.h                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for service ECALL            */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#ifndef egts_teledata_h
#define egts_teledata_h

/******************************************************************************
*
*/

extern const egts_subrecord_handler_t  chandler_TELEDATA_POS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_EXT_POS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_AD_SENSORS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_STATE_DATA;
/*
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_COUNTERS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_ACCEL_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_LOOPIN_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_ABS_DIG_SENS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_ABS_AN_SENS_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_ABS_CNTR_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_ABS_LOOPIN_DATA;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_LIQUID_LEVEL_SENSOR;
extern const egts_subrecord_handler_t  chandler_TELEDATA_SR_PASSENGERS_COUNTERS;
*/
/******************************************************************************
*
*/

extern const egts_service_handler_t    cservice_TELEDATA;

/******************************************************************************
*
*/


#endif




