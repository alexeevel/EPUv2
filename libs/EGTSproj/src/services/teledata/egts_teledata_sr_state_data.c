/*****************************************************************************/
/*                                                                           */
/* File: egts_teledata_pos_data.c                                                   */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for packet                   */
/*   EGTS_SR_ACCEL_DATA, service ECALL, implementaion                        */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"

#include "../../include/egts.h"
#include "../../include/egts_impl.h"
#include "../../transport/egts_guts.h"

#include "../egts_services.h"
#include "../egts_service_handler.h"
#include "egts_teledata.h"

/******************************************************************************
* prototypes
*/


/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_SR_STATE_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_SR_STATE_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_SR_STATE_DATA( void* pv_subrecord );

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_SR_STATE_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_SR_STATE_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_SR_STATE_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz );

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_SR_STATE_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_SR_STATE_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_SR_STATE_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz );

#ifdef EGTS_DBG

/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_SR_STATE_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_SR_STATE_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_SR_STATE_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 );

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_SR_STATE_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_SR_STATE_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_SR_STATE_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) ); 
    
#endif
    
/******************************************************************************
* implementations
*/

/*****************************************************************************/
/*                                                                           */
/* egts_get_TELEDATA_SR_STATE_DATA_bits()                                               */
/*                                                                           */
/* Description: Collect TELEDATA_SR_STATE_DATA subrecord bitfields                 */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*                                                                           */
/* Return:    bitfields byte                                                 */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u8    egts_get_TELEDATA_SR_STATE_DATA_bits( egts_TELEDATA_SR_STATE_DATA_t* ps )
{
  u8 b = 0U;
  b |= ( ps->NMS & 0x01U );
  b <<= 1U;
  b |= ( ps->IBU & 0x01U );
  b <<= 1U;
  b |= ( ps->BBU & 0x01U );
  return b;
}

/*****************************************************************************/
/*                                                                           */
/* egts_set_TELEDATA_SR_STATE_DATA_bits()                                               */
/*                                                                           */
/* Description: Set TELEDATA_SR_STATE_DATA subrecord bitfields                     */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*            b - bitfields byte                                             */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
void  egts_set_TELEDATA_SR_STATE_DATA_bits( egts_TELEDATA_SR_STATE_DATA_t* ps , u8 b )
{
  ps->BBU   = b & 0x01U;
  b >>= 1U;
  ps->IBU   = b & 0x01U;
  b >>= 1U;
  ps->NMS  = b & 0x01U;
}

/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_SR_STATE_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_SR_STATE_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_SR_STATE_DATA( void* pv_subrecord )
{
/*	egts_TELEDATA_SR_STATE_DATA_t* psrec = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord;*/
  u16 s = 0U;

  s += sizeof(u8) +  /* ST */
		sizeof(u8) +  /* MPSV */
		sizeof(u8) +  /* BBV */
		sizeof(u8) +  /* IBV */
		sizeof(u8);  /* FLAGS */

  return s;
}

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_SR_STATE_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_SR_STATE_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_SR_STATE_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz )
{
  egts_TELEDATA_SR_STATE_DATA_t* psrec = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord;

  (void)SRL;

  EGTS_READ(psrec->ST,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->MPSV,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->BBV,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->IBV,*ppbuf,*pbuf_sz)

  if ( *pbuf_sz > 0U )  {
	  egts_set_TELEDATA_SR_STATE_DATA_bits( psrec , **ppbuf );
    (*ppbuf)++;
    (*pbuf_sz)--;
  } else {
    return -1;
  }

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_SR_STATE_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_SR_STATE_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_SR_STATE_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz )
{
  egts_TELEDATA_SR_STATE_DATA_t* psrec = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord;

  EGTS_WRITE(psrec->ST,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->MPSV,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->BBV,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->IBV,*ppbuf,*pbuf_sz)

  if ( *pbuf_sz > 0U ) {
    *(*ppbuf)++ = egts_get_TELEDATA_SR_STATE_DATA_bits( psrec );
    (*pbuf_sz)--;
  } else {
    return -1;
  }

  return 0;
}


/******************************************************************************
*
*/

#ifdef EGTS_DBG


/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_SR_STATE_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_SR_STATE_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_SR_STATE_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 )
{
  egts_TELEDATA_SR_STATE_DATA_t* psrec1 = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord1;
  egts_TELEDATA_SR_STATE_DATA_t* psrec2 = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord2;

  (void)ctx;

  if ( psrec1->ST != psrec2->ST ) {
    egts_dbg_printf("inequieal ST");
    return -1;
  }
  
  if ( psrec1->MPSV != psrec2->MPSV ) {
    egts_dbg_printf("inequieal MPSV");
    return -1;
  }

  if ( psrec1->BBV != psrec2->BBV ) {
    egts_dbg_printf("inequieal BBV");
    return -1;
  }

  if ( psrec1->IBV != psrec2->IBV ) {
    egts_dbg_printf("inequieal IBV");
    return -1;
  }

  if ( psrec1->NMS != psrec2->NMS ) {
    egts_dbg_printf("inequieal NMS");
    return -1;
  }

  if ( psrec1->IBU != psrec2->IBU ) {
    egts_dbg_printf("inequieal IBU");
    return -1;
  }

  if ( psrec1->BBU != psrec2->BBU ) {
    egts_dbg_printf("inequieal BBU");
    return -1;
  }

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_SR_STATE_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_SR_STATE_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_SR_STATE_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  egts_TELEDATA_SR_STATE_DATA_t* psrec = (egts_TELEDATA_SR_STATE_DATA_t*)pv_subrecord;

  (*afn_dump)( ctx , "TELEDATA_SR_STATE_DATA:\n");
  (*afn_dump)( ctx , "  State                          ST: %u (%08Xh)\n" , psrec->ST, psrec->ST );
  (*afn_dump)( ctx , "  Main Power Source Voltage      MPSV: %u (%08Xh)\n" , psrec->MPSV, psrec->MPSV );
  (*afn_dump)( ctx , "  Back Up Battery Voltage        BBV: %u (%08Xh)\n" , psrec->BBV, psrec->BBV );
  (*afn_dump)( ctx , "  Internal Battery Voltage       IBV: %u (%08Xh)\n" , psrec->IBV, psrec->IBV );
  (*afn_dump)( ctx , "  FLAGS: NMS= %u, IBU= %u, BBU= %u\n" ,
     psrec->NMS, psrec->IBU, psrec->BBU);
}


#endif

/******************************************************************************
*
*/

const egts_subrecord_handler_t  chandler_TELEDATA_SR_STATE_DATA =
{
  /* SRT */             (u8)EGTS_SR_STATE_DATA ,
  /* subrecord_size */  (u16)sizeof(egts_TELEDATA_SR_STATE_DATA_t) ,
  /* fn_get_size */     egts_get_size_TELEDATA_SR_STATE_DATA ,
  /* fn_read */         egts_read_TELEDATA_SR_STATE_DATA ,
  /* fn_write */        egts_write_TELEDATA_SR_STATE_DATA ,
#ifdef EGTS_DBG
  /* fs_is_equial */    egts_is_equial_TELEDATA_SR_STATE_DATA ,
  /* fn_dump */         egts_dump_TELEDATA_SR_STATE_DATA
#endif
};


/******************************************************************************
*
*/
