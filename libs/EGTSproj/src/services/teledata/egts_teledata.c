/*****************************************************************************/
/*                                                                           */
/* File: egts_ecall.c                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*	                                                                     */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for service ECALL            */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"
#include "../../include/egts.h"


#include "../egts_services.h"
#include "../egts_service_handler.h"
#include "egts_teledata.h"
#include "../egts_response.h"

#include "../commands/egts_command.h"

/******************************************************************************
*
*/


static
const egts_subrecord_handler_t* const  chandlers_TELEDATA[] =
{
  &chandler_RESPONSE,
  &chandler_COMMANDS_COMMAND_DATA,
  &chandler_TELEDATA_POS_DATA,
  &chandler_TELEDATA_EXT_POS_DATA,
  &chandler_TELEDATA_SR_AD_SENSORS_DATA,
  &chandler_TELEDATA_SR_STATE_DATA,
/*
  &chandler_TELEDATA_SR_COUNTERS_DATA,
  &chandler_TELEDATA_SR_ACCEL_DATA,
  &chandler_TELEDATA_SR_LOOPIN_DATA,
  &chandler_TELEDATA_SR_ABS_DIG_SENS_DATA,
  &chandler_TELEDATA_SR_ABS_AN_SENS_DATA,
  &chandler_TELEDATA_SR_ABS_CNTR_DATA,
  &chandler_TELEDATA_SR_ABS_LOOPIN_DATA,
  &chandler_TELEDATA_SR_LIQUID_LEVEL_SENSOR,
  &chandler_TELEDATA_SR_PASSENGERS_COUNTERS,
 */
  NULL
};

const egts_service_handler_t    cservice_TELEDATA =
{
  /* ST */          (u8)EGTS_TELEDATA_SERVICE ,
  /* chandlers */   chandlers_TELEDATA
};

/******************************************************************************
*
*/
