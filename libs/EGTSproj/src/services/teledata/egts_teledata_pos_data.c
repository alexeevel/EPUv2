/*****************************************************************************/
/*                                                                           */
/* File: egts_teledata_pos_data.c                                                   */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for packet                   */
/*   EGTS_SR_ACCEL_DATA, service ECALL, implementaion                        */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"

#include "../../include/egts.h"
#include "../../include/egts_impl.h"
#include "../../transport/egts_guts.h"

#include "../egts_services.h"
#include "../egts_service_handler.h"
#include "egts_teledata.h"

/******************************************************************************
* prototypes
*/


/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_POS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_POS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_POS_DATA( void* pv_subrecord );

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_POS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_POS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_POS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz );

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_POS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_POS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_POS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz );

#ifdef EGTS_DBG

/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_POS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_POS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_POS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 );

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_POS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_POS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_POS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) ); 
    
#endif
    
/******************************************************************************
* implementations
*/

/*****************************************************************************/
/*                                                                           */
/* egts_get_TELEDATA_POS_DATA_bits()                                               */
/*                                                                           */
/* Description: Collect TELEDATA_POS_DATA subrecord bitfields                 */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*                                                                           */
/* Return:    bitfields byte                                                 */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u8    egts_get_TELEDATA_POS_DATA_bits( egts_TELEDATA_POS_DATA_t* ps )
{
  u8 b = 0U;
  b |= ( ps->ALTE & 0x01U );
  b <<= 1U;
  b |= ( ps->LOHS & 0x01U );
  b <<= 1U;
  b |= ( ps->LAHS & 0x01U );
  b <<= 1U;
  b |= ( ps->MV & 0x01U );
  b <<= 1U;
  b |= ( ps->BB & 0x01U );
  b <<= 2U;
  b |= ( ps->FIX & 0x03U );
  b <<= 1U;
  b |= ( ps->VLD & 0x01U );
  return b;
}

/*****************************************************************************/
/*                                                                           */
/* egts_set_TELEDATA_POS_DATA_bits()                                               */
/*                                                                           */
/* Description: Set TELEDATA_POS_DATA subrecord bitfields                     */
/*                                                                           */
/* Arguments: ps - subrecord                                                 */
/*            b - bitfields byte                                             */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
void  egts_set_TELEDATA_POS_DATA_bits( egts_TELEDATA_POS_DATA_t* ps , u8 b )
{
  ps->VLD   = b & 0x01U;
  b >>= 1U;
  ps->FIX   = b & 0x03U;
  b >>= 2U;
  ps->BB  = b & 0x01U;
  b >>= 1U;
  ps->MV   = b & 0x01U;
  b >>= 1U;
  ps->LAHS   = b & 0x01U;
  b >>= 1U;
  ps->LOHS   = b & 0x01U;
  b >>= 1U;
  ps->ALTE   = b & 0x01U;
}

/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_POS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_POS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_POS_DATA( void* pv_subrecord )
{
	egts_TELEDATA_POS_DATA_t* psrec = (egts_TELEDATA_POS_DATA_t*)pv_subrecord;
  u16 s = 0U;

  s += sizeof(u32) +  /* NTM */
		sizeof(u32) +  /* LAT */
		sizeof(u32) +  /* LONG */
		sizeof(u8) +  /* FLG */
		sizeof(u16) +  /* SPD */
		sizeof(u8) +  /* DIR */
		sizeof(u8)*3 +  /* ODM */
		sizeof(u8) +  /* DIN */
		sizeof(u8);  /* SRC */
  if ( psrec->ALTE )
    s += sizeof(u8)*3; /* ALT */
  /*//TODO */
  if ( psrec->SRC == 0xFF)
    s += sizeof(u16); /* SRCD */

  return s;
}

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_POS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_POS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_POS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz )
{
  egts_TELEDATA_POS_DATA_t* psrec = (egts_TELEDATA_POS_DATA_t*)pv_subrecord;

  (void)SRL;

  EGTS_READ(psrec->NTM,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->LAT,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->LONG,*ppbuf,*pbuf_sz)

  if ( *pbuf_sz > 0U )  {
	  egts_set_TELEDATA_POS_DATA_bits( psrec , **ppbuf );
    (*ppbuf)++;
    (*pbuf_sz)--;
  } else {
    return -1;
  }
  /*//TODO: bits DIRH ALTS */

  EGTS_READ(psrec->SPD,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->DIR,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->ODM,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->DIN,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->SRC,*ppbuf,*pbuf_sz)

  if ( psrec->ALTE )
	  EGTS_READ(psrec->ALT,*ppbuf,*pbuf_sz)
  /*//TODO */
  if ( psrec->SRC == 0xFF)
	  EGTS_READ(psrec->SRCD,*ppbuf,*pbuf_sz)

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_POS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_POS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_POS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz )
{
  egts_TELEDATA_POS_DATA_t* psrec = (egts_TELEDATA_POS_DATA_t*)pv_subrecord;

  EGTS_WRITE(psrec->NTM,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->LAT,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->LONG,*ppbuf,*pbuf_sz)

  if ( *pbuf_sz > 0U ) {
    *(*ppbuf)++ = egts_get_TELEDATA_POS_DATA_bits( psrec );
    (*pbuf_sz)--;
  } else {
    return -1;
  }
  /*//TODO: bits DIRH ALTS */

  EGTS_WRITE(psrec->SPD,*ppbuf,*pbuf_sz)
  if (psrec->ALTS)
	  *((unsigned char *)(ppbuf - 1)) |= 0x40;
  if (psrec->DIRH)
	  *((unsigned char *)(ppbuf - 1)) |= 0x80;

  EGTS_WRITE(psrec->DIR,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->ODM,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->DIN,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->SRC,*ppbuf,*pbuf_sz)

  if ( psrec->ALTE )
	  EGTS_WRITE(psrec->ALT,*ppbuf,*pbuf_sz);

  /*//TODO */
  if ( psrec->SRC == 0xFF)
	  EGTS_WRITE(psrec->SRCD,*ppbuf,*pbuf_sz)

  return 0;
}


/******************************************************************************
*
*/

#ifdef EGTS_DBG


/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_POS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_POS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_POS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 )
{
  egts_TELEDATA_POS_DATA_t* psrec1 = (egts_TELEDATA_POS_DATA_t*)pv_subrecord1;
  egts_TELEDATA_POS_DATA_t* psrec2 = (egts_TELEDATA_POS_DATA_t*)pv_subrecord2;

  (void)ctx;

  if ( psrec1->NTM != psrec2->NTM ) {
    egts_dbg_printf("inequieal NTM");
    return -1;
  }
  
  if ( psrec1->LAT != psrec2->LAT ) {
    egts_dbg_printf("inequieal LAT");
    return -1;
  }

  if ( psrec1->LONG != psrec2->LONG ) {
    egts_dbg_printf("inequieal LONG");
    return -1;
  }

  if ( psrec1->ALTE != psrec2->ALTE ) {
    egts_dbg_printf("inequieal ALTE");
    return -1;
  }

  if ( psrec1->LOHS != psrec2->LOHS ) {
    egts_dbg_printf("inequieal LOHS");
    return -1;
  }

  if ( psrec1->LAHS != psrec2->LAHS ) {
    egts_dbg_printf("inequieal LAHS");
    return -1;
  }

  if ( psrec1->MV != psrec2->MV ) {
    egts_dbg_printf("inequieal MV");
    return -1;
  }

  if ( psrec1->BB != psrec2->BB ) {
    egts_dbg_printf("inequieal BB");
    return -1;
  }

  if ( psrec1->FIX != psrec2->FIX ) {
    egts_dbg_printf("inequieal FIX");
    return -1;
  }

  if ( psrec1->VLD != psrec2->VLD ) {
    egts_dbg_printf("inequieal VLD");
    return -1;
  }

  if ( psrec1->SPD != psrec2->SPD ) {
    egts_dbg_printf("inequieal SPD");
    return -1;
  }

  if ( psrec1->DIRH != psrec2->DIRH ) {
    egts_dbg_printf("inequieal DIRH");
    return -1;
  }

  if ( psrec1->VLD != psrec2->ALTS ) {
    egts_dbg_printf("inequieal ALTS");
    return -1;
  }

  if ( psrec1->DIR != psrec2->DIR ) {
    egts_dbg_printf("inequieal DIR");
    return -1;
  }

  if (( psrec1->ODM[0] != psrec2->ODM[0] ) || ( psrec1->ODM[1] != psrec2->ODM[1] ) || ( psrec1->ODM[2] != psrec2->ODM[2] )) {
    egts_dbg_printf("inequieal ODM");
    return -1;
  }

  if ( psrec1->DIN != psrec2->DIN ) {
    egts_dbg_printf("inequieal DIN");
    return -1;
  }

  if ( psrec1->SRC != psrec2->SRC ) {
    egts_dbg_printf("inequieal SRC");
    return -1;
  }

  if ( psrec1->ALTE )
  {
	  if (( psrec1->ALT[0] != psrec2->ALT[0] ) || ( psrec1->ALT[1] != psrec2->ALT[1] ) || ( psrec1->ALT[2] != psrec2->ALT[2] )) {
	    egts_dbg_printf("inequieal ALT");
	    return -1;
	  }
  }

  /*//TODO */
  if ( psrec1->SRC == 0xFF)
  {
	  if ( psrec1->SRCD != psrec2->SRCD ) {
	    egts_dbg_printf("inequieal SRCD");
	    return -1;
	  }
  }
  
  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_POS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_POS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static void  egts_dump_TELEDATA_POS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) 
  )
{
	u8  s,n,h,d,m;
	u32 y;
	egts_TELEDATA_POS_DATA_t* psrec = (egts_TELEDATA_POS_DATA_t*)pv_subrecord;
	egts_get_tm( psrec->NTM , &s, &n, &h, &d, &m, &y );
	float lat = ((float)psrec->LAT / 0xFFFFFFFF) * 90;
	float longit = ((float)psrec->LONG / 0xFFFFFFFF) * 180;
	unsigned int odm = 0;
	memcpy(&odm, psrec->ODM, 3);

	char *SRCtext = "UNKNOWN";
	switch (psrec->SRC)
	{
	case EGTS_SRC_TIMER_IGNITION:
		SRCtext = "TIMER_IGNITION";
		break;
	case EGTS_SRC_DISTANCE:
		SRCtext = "DISTANCE";
		break;
	case EGTS_SRC_ANGLE:
		SRCtext = "ANGLE";
		break;
	case EGTS_SRC_RESPONSE:
		SRCtext = "RESPONSE";
		break;
	case EGTS_SRC_INPUT:
		SRCtext = "INPUT";
		break;
	case EGTS_SRC_TIMER_NO_IGNITION:
		SRCtext = "TIMER_NO_IGNITION";
		break;
	case EGTS_SRC_PERIPHERIAL_OFF:
		SRCtext = "PERIPHERIAL_OFF";
		break;
	case EGTS_SRC_SPEED_TRESHOLD:
		SRCtext = "SPEED_TRESHOLD";
		break;
	case EGTS_SRC_CPU_RESTART:
		SRCtext = "CPU_RESTART";
		break;
	case EGTS_SRC_OUTPUT_OVERLOAD:
		SRCtext = "OUTPUT_OVERLOAD";
		break;
	case EGTS_SRC_TAMPER:
		SRCtext = "TAMPER";
		break;
	case EGTS_SRC_BATTERY_POWER:
		SRCtext = "BATTERY_POWER";
		break;
	case EGTS_SRC_LOW_BATTERY:
		SRCtext = "LOW_BATTERY";
		break;
	case EGTS_SRC_ALARM:
		SRCtext = "ALARM";
		break;
	case EGTS_SRC_CALL_REQUEST:
		SRCtext = "CALL_REQUEST";
		break;
	case EGTS_SRC_EMERGENCY:
		SRCtext = "EMERGENCY";
		break;
	case EGTS_SRC_EXT_SERVICE:
		SRCtext = "EXT_SERVICE";
		break;
	case EGTS_SRC_BATTERY_FAULT:
		SRCtext = "BATTERY_FAULT";
		break;
	case EGTS_SRC_EXTRA_ACC:
		SRCtext = "EXTRA_ACC";
		break;
	case EGTS_SRC_EXTRA_BREAK:
		SRCtext = "EXTRA_BREAK";
		break;
	case EGTS_SRC_ERROR_NAV:
		SRCtext = "ERROR_NAV";
		break;
	case EGTS_SRC_ERROR_SENSOR:
		SRCtext = "ERROR_SENSOR";
		break;
	case EGTS_SRC_ERROR_GSM_AERIAL:
		SRCtext = "ERROR_GSM_AERIAL";
		break;
	case EGTS_SRC_ERROR_NAV_AERIAL:
		SRCtext = "ERROR_NAV_AERIAL";
		break;
	case EGTS_SRC_LOW_SPEED:
		SRCtext = "LOW_SPEED";
		break;
	case EGTS_SRC_NO_IGNITION_MOVE:
		SRCtext = "NO_IGNITION_MOVE";
		break;
	case EGTS_SRC_TIMER_EMERGENCY:
		SRCtext = "TIMER_EMERGENCY";
		break;
	case EGTS_SRC_NAV_ON_OFF:
		SRCtext = "NAV_ON_OFF";
		break;
	case EGTS_SRC_UNSTABLE_NAV:
		SRCtext = "UNSTABLE_NAV";
		break;
	case EGTS_SRC_IP_CONNECT:
		SRCtext = "IP_CONNECT";
		break;
	case EGTS_SRC_UNSTABLE_GSM:
		SRCtext = "UNSTABLE_GSM";
		break;
	case EGTS_SRC_UNSTABLE_IP:
		SRCtext = "UNSTABLE_IP";
		break;
	case EGTS_SRC_MODE:
		SRCtext = "MODE";
		break;
	default:
		break;
	}

#if LONG_DEBUG
	(*afn_dump)( ctx , "TELEDATA_POS_DATA:\n");
	(*afn_dump)( ctx , "  Navigation Time                   NTM: %02u.%02u.%04u %02u:%02u:%02u  (%u) (%08Xh)\n" ,
	d, m, y, h, n, s, psrec->NTM, psrec->NTM );
	/* Latitude , degree,  (WGS - 84) / 90 * 0xFFFFFFFF */
	/* Longitude , degree,  (WGS - 84) / 180 * 0xFFFFFFFF */
	(*afn_dump)( ctx , "  Latitude                          LAT: %f (%08Xh)\n" , lat, psrec->LAT );
	(*afn_dump)( ctx , "  Longitude                         LONG: %f (%08Xh)\n" , longit, psrec->LONG );
	(*afn_dump)( ctx , "  FLAGS: ALTE= %u, LOHS= %u, LAHS= %u, BB= %u, FIX= %u, VLD= %u\n" ,
     psrec->ALTE, psrec->LOHS, psrec->LAHS, psrec->BB, psrec->FIX, psrec->VLD );
	/*//TODO: bits DIRH ALTS */
	(*afn_dump)( ctx , "  Speed                             SPD: %u (%08Xh)\n" , psrec->SPD, psrec->SPD );
	(*afn_dump)( ctx , "  Direction                         DIR: %u (%08Xh)\n" , psrec->DIR, psrec->DIR );
	(*afn_dump)( ctx , "  Odometer                          ODM: %u (%08Xh)\n" , psrec->ODM, psrec->ODM );
	(*afn_dump)( ctx , "  Digital Inputs                    DIN: %u (%08Xh)\n" , psrec->DIN, psrec->DIN );
	 (*afn_dump)( ctx , "  Source                            SRC: %u (%08Xh)\n" , psrec->SRC, psrec->SRC );

	if ( psrec->ALTE )
		(*afn_dump)( ctx , "  Altitude                         ALT: %u (%08Xh)\n" , psrec->ALT, psrec->ALT );
	/*//TODO */
	if ( psrec->SRC == 0xFF)
		(*afn_dump)( ctx , "  Source Data                      SRCD: %u (%08Xh)\n" , psrec->SRCD, psrec->SRCD );
#else
	(*afn_dump)( ctx , "EGTS_SR_POS_DATA [%02u.%02u.%04u %02u:%02u:%02u] ", d, m, y, h, n, s );
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	(*afn_dump)( ctx , "LAT: %u° LONG: %u° ", psrec->LAT, psrec->LONG );
#else
	(*afn_dump)( ctx , "LAT: %f° LONG: %f° ", lat, longit );
#endif
	if ( psrec->ALTE )
	{
		unsigned int alt = 0;
		memcpy(&alt, psrec->ALT, 3);
		(*afn_dump)( ctx , "ALT: %um ", alt );
	}
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	(*afn_dump)( ctx , "SPEED: %ukm/h DIR: %u° ODOMETER: %ukm DI: %02X SRC: %s \n", (int)(((float)psrec->SPD) / 10), psrec->DIR, odm, psrec->DIN, SRCtext );
#else
	(*afn_dump)( ctx , "SPEED: %3.1fkm/h DIR: %u° ODOMETER: %ukm DI: %02X SRC: %s \n", (((float)psrec->SPD) / 10), psrec->DIR, odm, psrec->DIN, SRCtext );
#endif
  /*//TODO: bits DIRH ALTS */
#endif
}


#endif

/******************************************************************************
*
*/

const egts_subrecord_handler_t  chandler_TELEDATA_POS_DATA =
{
  /* SRT */             (u8)EGTS_SR_POS_DATA ,
  /* subrecord_size */  (u16)sizeof(egts_TELEDATA_POS_DATA_t) ,
  /* fn_get_size */     egts_get_size_TELEDATA_POS_DATA ,
  /* fn_read */         egts_read_TELEDATA_POS_DATA ,
  /* fn_write */        egts_write_TELEDATA_POS_DATA ,
#ifdef EGTS_DBG
  /* fs_is_equial */    egts_is_equial_TELEDATA_POS_DATA ,
  /* fn_dump */         egts_dump_TELEDATA_POS_DATA
#endif
};


/******************************************************************************
*
*/
