/*****************************************************************************/
/*                                                                           */
/* File: egts_teledata_pos_data.c                                                   */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Service ECALL                                             */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Parser/Composer/Debuger entries for packet                   */
/*   EGTS_SR_ACCEL_DATA, service ECALL, implementaion                        */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: private implemenation                                          */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"

#include "../../include/egts.h"
#include "../../include/egts_impl.h"
#include "../../transport/egts_guts.h"

#include "../egts_services.h"
#include "../egts_service_handler.h"
#include "egts_teledata.h"

/******************************************************************************
* prototypes
*/


/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_SR_AD_SENSORS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_SR_AD_SENSORS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord );

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_SR_AD_SENSORS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_SR_AD_SENSORS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz );

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_SR_AD_SENSORS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_SR_AD_SENSORS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz );

#ifdef EGTS_DBG

/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_SR_AD_SENSORS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_SR_AD_SENSORS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_SR_AD_SENSORS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 );

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_SR_AD_SENSORS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_SR_AD_SENSORS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_SR_AD_SENSORS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) ); 
    
#endif
    
/******************************************************************************
* implementations
*/

/*****************************************************************************/
/*                                                                           */
/* egts_get_size_TELEDATA_SR_AD_SENSORS_DATA()                                          */
/*                                                                           */
/* Description: Returns size of specified TELEDATA_SR_AD_SENSORS_DATA subrecord         */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
u16   egts_get_size_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord )
{
	egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord;
  u16 s = 0U;
  int i = 0;

  s += sizeof(u8); /* DIOE */
  s += sizeof(u8); /* DOUT */
  s += sizeof(u8); /* ASFE */

  for (i=0; i<8; i++)
  {
    if ( (psrec->DIOE & (1 << i)) > 0 )
   	  s += sizeof(u8); /* ADIO[i] */
    if ( (psrec->ASFE & (1 << i)) > 0 )
   	  s += sizeof(psrec->ANS[0]); /* ANS */
  }

  return s;
}

/*****************************************************************************/
/*                                                                           */
/* egts_read_TELEDATA_SR_AD_SENSORS_DATA()                                              */
/*                                                                           */
/* Description: Read subrecord TELEDATA_SR_AD_SENSORS_DATA from buffer                  */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            SRL - subrecord data length                                    */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_read_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord , u16 SRL ,
  u8**  ppbuf ,
  u16*  pbuf_sz )
{
  egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord;
  int i = 0;
  (void)SRL;

  EGTS_READ(psrec->DIOE,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->DOUT,*ppbuf,*pbuf_sz)
  EGTS_READ(psrec->ASFE,*ppbuf,*pbuf_sz)

  for (i=0; i<8; i++)
  {
    if ( (psrec->DIOE & (1 << i)) > 0 )
    	EGTS_READ(psrec->ADIO[i],*ppbuf,*pbuf_sz)
  }

  for (i=0; i<8; i++)
  {
	if ( (psrec->ASFE & (1 << i)) > 0 )
    	EGTS_READ(psrec->ANS[i],*ppbuf,*pbuf_sz)
  }

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_write_TELEDATA_SR_AD_SENSORS_DATA()                                             */
/*                                                                           */
/* Description: Write subrecord TELEDATA_SR_AD_SENSORS_DATA to buffer                   */
/*                                                                           */
/* Arguments: pv_subrecord - pointer to subrecord                            */
/*            ppbuf - pointer to buffer begin                                */
/*            pbuf_sz - size of buffer                                       */
/*                                                                           */
/* Return:    size of subrecord                                              */
/*                                                                           */
/* Other:     internal use only                                              */
/*                                                                           */
/*****************************************************************************/

static
int   egts_write_TELEDATA_SR_AD_SENSORS_DATA( void* pv_subrecord ,
  u8**   ppbuf ,
  u16*   pbuf_sz )
{
  egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord;
  int i = 0;

  EGTS_WRITE(psrec->DIOE,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->DOUT,*ppbuf,*pbuf_sz)
  EGTS_WRITE(psrec->ASFE,*ppbuf,*pbuf_sz)

  for (i=0; i<8; i++)
  {
    if ( (psrec->DIOE & (1 << i)) > 0 )
    	EGTS_WRITE(psrec->ADIO[i],*ppbuf,*pbuf_sz)
  }

  for (i=0; i<8; i++)
  {
	if ( (psrec->ASFE & (1 << i)) > 0 )
    	EGTS_WRITE(psrec->ANS[i],*ppbuf,*pbuf_sz)
  }

  return 0;
}


/******************************************************************************
*
*/

#ifdef EGTS_DBG


/*****************************************************************************/
/*                                                                           */
/* egts_is_equial_TELEDATA_SR_AD_SENSORS_DATA()                                         */
/*                                                                           */
/* Description: Member-wise compare TELEDATA_SR_AD_SENSORS_DATA subrecords              */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord1 - firts subrecord to test                        */
/*            pv_subrecord2 - second subrecord to test                       */
/*                                                                           */
/* Return:    0 if subrecords are equial, nonzero valu otherwise             */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
int  egts_is_equial_TELEDATA_SR_AD_SENSORS_DATA( void* ctx , void* pv_subrecord1 , void* pv_subrecord2 )
{
  egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec1 = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord1;
  egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec2 = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord2;
  int i = 0;
  (void)ctx;

  if ( psrec1->DIOE != psrec2->DIOE ) {
    egts_dbg_printf("inequieal DIOE");
    return -1;
  }

  if ( psrec1->DOUT != psrec2->DOUT ) {
    egts_dbg_printf("inequieal DOUT");
    return -1;
  }

  if ( psrec1->ASFE != psrec2->ASFE ) {
    egts_dbg_printf("inequieal ASFE");
    return -1;
  }

  for (i=0; i<8; i++)
  {
    if ( (psrec1->DIOE & (1 << i)) > 0 )
    	if ( psrec1->ADIO[i] != psrec2->ADIO[i] ) {
   	    egts_dbg_printf("inequieal ADIO");
   	    return -1;
   	  }
  }

  for (i=0; i<8; i++)
  {
	if ( (psrec1->ASFE & (1 << i)) > 0 )
    	if ( psrec1->ANS[i] != psrec2->ANS[i] ) {
   	    egts_dbg_printf("inequieal ANS");
   	    return -1;
   	  }
  }

  return 0;
}

/*****************************************************************************/
/*                                                                           */
/* egts_dump_TELEDATA_SR_AD_SENSORS_DATA()                                              */
/*                                                                           */
/* Description: Dump content of TELEDATA_SR_AD_SENSORS_DATA subrecord                   */
/*                                                                           */
/* Arguments: ctx - dump callback context                                    */
/*            pv_subrecord - subrecord to dump                               */
/*            afn_dump - callback                                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     debug use only                                                 */
/*                                                                           */
/*****************************************************************************/

static
void  egts_dump_TELEDATA_SR_AD_SENSORS_DATA( void* ctx , void* pv_subrecord ,
  void (* afn_dump)( void* actx , const char* fmt , ... ) 
  )
{
  egts_TELEDATA_SR_AD_SENSORS_DATA_t* psrec = (egts_TELEDATA_SR_AD_SENSORS_DATA_t*)pv_subrecord;
  int i = 0;

  (*afn_dump)( ctx , "TELEDATA_SR_AD_SENSORS_DATA:\n");
  (*afn_dump)( ctx , "  Digital Inputs Octet Exists  DIOE: %u (%08Xh)\n" , psrec->DIOE, psrec->DIOE );
  (*afn_dump)( ctx , "  Digital Outputs              DOUT: %u (%08Xh)\n" , psrec->DOUT, psrec->DOUT );
  (*afn_dump)( ctx , "  Analog Sensor Field Exists   ASFE: %u (%08Xh)\n" , psrec->ASFE, psrec->ASFE );
  for (i=0; i<8; i++)
  {
    if ( (psrec->DIOE & (1 << i)) > 0 )
   	  (*afn_dump)( ctx , "  Additional Digital Inputs Octet  ADIO[%u]: %u (%08Xh)\n" , i, psrec->ADIO[i], psrec->ADIO[i] );
  }
  for (i=0; i<8; i++)
  {
    if ( (psrec->ASFE & (1 << i)) > 0 )
   	  (*afn_dump)( ctx , "  Analog Sensor                    ANS[%u]: %u (%08Xh)\n" , i, psrec->ANS[i], psrec->ANS[i] );
  }

}

#endif

/******************************************************************************
*
*/

const egts_subrecord_handler_t  chandler_TELEDATA_SR_AD_SENSORS_DATA =
{
  /* SRT */             (u8)EGTS_SR_AD_SENSORS_DATA ,
  /* subrecord_size */  (u16)sizeof(egts_TELEDATA_SR_AD_SENSORS_DATA_t) ,
  /* fn_get_size */     egts_get_size_TELEDATA_SR_AD_SENSORS_DATA ,
  /* fn_read */         egts_read_TELEDATA_SR_AD_SENSORS_DATA ,
  /* fn_write */        egts_write_TELEDATA_SR_AD_SENSORS_DATA ,
#ifdef EGTS_DBG
  /* fs_is_equial */    egts_is_equial_TELEDATA_SR_AD_SENSORS_DATA ,
  /* fn_dump */         egts_dump_TELEDATA_SR_AD_SENSORS_DATA
#endif
};


/******************************************************************************
*
*/
