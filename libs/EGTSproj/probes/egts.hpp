/*
 * egts.hpp
 *
 *  Created on: 24 июн. 2021 г.
 *      Author: abutko
 */

#ifndef EGTSPROJ_PROBES_EGTS_HPP_
#define EGTSPROJ_PROBES_EGTS_HPP_

#include "hwtesting/hwtesting.hpp"
#include "debug/debug.hpp"
using namespace debug;

#include "egts_config.h"
#include "egts.h"
#include "egts_impl.h"
#include "egts_probe.h"

void setDefValue();

class EGTS : public AbstractHWTest
{
	egts_state_t		estate_rx;
	egts_state_t		estate_tx;
	egts_profile_t def_profile;
	u8             def_priority;
	egts_route_t   test_route;
	u16            PID;
	const tf_probe_send*      pfn_probe_send;
	egts_responce_header_t    responce;

public:
	bool EGTS_Init();
	void EGTS_Deinit();
	bool analyzePacket(const char *inBuf, const unsigned int inSize);
	bool nextPacket(char **outBuf, unsigned int *outSize);
	bool test() override
	{
		bool on = true;//EGTS_Init();
		if (on == false)
		{
			DBG::PRINT(LVL::EMCY, "M66", "" , "TEST FAIL", "POWER ON TIMEOUT");
			return false;
		}
		else
		{
			DBG::PRINT(LVL::INFO, "M66", "" , "POWER ON OK");
			return true;
		}
	}
};


#endif /* EGTSPROJ_PROBES_EGTS_HPP_ */
