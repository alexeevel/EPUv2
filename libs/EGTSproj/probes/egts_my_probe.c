/*
 * egts_my_probe.c
 *
 *  Created on: 28 февр. 2020 г.
 *      Author: abutko
 */


#include "egts_config.h"
#include "egts.h"
#include "egts_impl.h"

#include "egts_probe.h"

/*****************************************************************************
*
*/

#include <stdarg.h>
#include <stdlib.h>

//#ifdef WINDOWS
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
#include "main.h"
#include "egts_my_probe.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
//#include "serial/m66.h"
#include "usbcomm.h"
#define EGTS_SYSMSG_EVENT_QUEUE_SIZE              ( ( unsigned portSHORT )10 )

xQueueHandle egtsEventQueue;
xTaskHandle xTaskEGTSMainHandle;

char deb_egts_buf[256];
int len_ostr = 0;
void egts_debug_ctx(void *ctx, const char *x, ...);

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#endif
#include "../transport/egts_dump.h"

//#include "error.h"
//#include "serial/gps.h"

/*****************************************************************************
*
*/

#if !(defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__))
egts_state_t  estate_rx;
egts_state_t  estate_tx;

FILE *fglob = NULL;
int sockfd = 0;
u8 recvedByte = 0;
unsigned char send_buf_final[45000];
unsigned int send_buf_final_len = 0;

int  socket_estate_tx_buffer_loop( void* ctx , void* pbuf , u32 sz )
{
	u8 *puc = (u8*)pbuf;
	int i=0;
	for (i=0; i<sz; i++)
	  printf("%02x ", puc[i]);
#if 1
	/*send(sockfd, pbuf, sz, 0);*/
	memcpy(&(send_buf_final[send_buf_final_len]), pbuf, sz);
	send_buf_final_len += sz;
	fwrite(pbuf, sz, 1, fglob);

#endif
#if 0
     while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
     {
   	  printf("%02x ", recvedByte);
#if 1
   	  egts_rx_byte((egts_state_t*)ctx, recvedByte);
#endif
     }
     printf("\n");
#endif

#if 0
	  for ( ; sz; sz-- )
	    egts_rx_byte( (egts_state_t*)ctx, *puc++ );
#endif
	return 0;
}

int  estate_tx_buffer_loop( void* ctx , void* pbuf , u32 sz )
{
  u8 *puc = (u8*)pbuf;

  for ( ; sz; sz-- )
    egts_rx_byte( (egts_state_t*)ctx, *puc++ );

  return 0;
}

void  estate_tx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
  (void)actx;
  (void)PID;
  (void)err;

  if ( dgb_str )
    printf( "tx error: %s\n" , dgb_str );
}

void  estate_rx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
  (void)actx;
  (void)PID;
  (void)err;

  if ( dgb_str )
    printf( "rx error: %s\n" , dgb_str );
}

void printf_dump( void* ctx , const char* fmt , ... )
{
  va_list v;

  (void)ctx;
  va_start(v,fmt);
  vprintf( fmt , v );
}

void egts_probe_printf( const char* fmt , ... )
{
  va_list v;
  va_start(v,fmt);
  vprintf( fmt , v );
}

u8    temp_buf[ 65536 ];

/*****************************************************************************
*
*/

egts_probe_ctx_t       probes_ctx;
volatile int count = 0;

/*****************************************************************************
*
*/

extern GPS_INFO info;
static char RxBuffer[1000];
static unsigned int usRxCounter;
#define use_date 1
static int gps_serial_parse( void )
{
      char c;
      float f;
      char *psz, *strptr;
      unsigned int i;
#if 1
      psz = strstr( RxBuffer, "RMC" );

      if( psz )
      {
            /* searching for data status flag */
            strptr = (char *)memchr(psz, ',', 10); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;

            info.rmc.ds = (GPS_DATA_STATUS)(*strptr);

            if( *strptr != GPS_DATA_VALID )
                  return -EAGAIN;

            strptr = RxBuffer;
            for ( i = 0; i < 1000; i++ )
            {
                  c = strptr[i];
                  if( !( ((c > 0x20) && (c < 0x67)) || c == 0x0D || c == 0x0A ) ) strptr[i] = '0';
            }

            /* searching for UTC */
            strptr = (char *)memchr(psz, ',', 10); strptr++;
            sscanf( strptr, "%02d%02d%02d.", &info.rmc.utc_h, &info.rmc.utc_m, &info.rmc.utc_s );
            /*snprintf( info.rmc.utc, 11, "%010.3f", f );*/

            /* searching for latitude */
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            sscanf( strptr, "%f,", &f );
            memcpy( &info.rmc.lat, strptr, 10);
            /*snprintf( info.rmc.lat, 11, "%09.4f", f );*/
            int full = (int)(f / 100);
            info.rmc.lat_float = (float)full + (f / 100 - (float)full) / 60 * 100;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            info.rmc.lat_dir = (GPS_LAT_DIRECTION)(*strptr);
            /*longitude*/
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            sscanf( strptr, "%f,", &f );
            memcpy( &info.rmc.lg, strptr, 10);
            /*snprintf( info.rmc.lg, 11, "%010.4f", f );*/
            full = (int)(f / 100);
            info.rmc.long_float = (float)full + (f / 100 - (float)full) / 60 * 100;/*f / 100;*/
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            info.rmc.long_dir = (GPS_LONG_DIRECTION)(*strptr);
#ifdef use_date
            /* searching for date */
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            sscanf( strptr, "%f,", &f );
            //memcpy( &info.rmc.lg, strptr, 10);
            /*snprintf( info.rmc.lg, 11, "%010.4f", f );*/
            info.rmc.speed = (f != 0) ? (int)(f * 1,852 * 10) : 0;
            memset( info.rmc.dt, 0, 7 );
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            sscanf( strptr, "%02d%02d%02d.", &info.rmc.dt_d, &info.rmc.dt_m, &info.rmc.dt_y );
            /*sscanf( strptr, "%[0123456789]", info.rmc.dt );*/
#endif
      }
#endif
      psz = strstr( RxBuffer, "GGA" );

      if( psz )
      {
            if( info.rmc.ds == GPS_DATA_INVALID )
                  return -1;

            /* searching for satillites */
            strptr = (char *)memchr(psz, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            /*sscanf( strptr, "%d", &info.gga.sat );*/


            /* searching for altitude */
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            strptr = (char *)memchr(strptr, ',', 20); strptr++;
            sscanf( strptr, "%[0123456789.]", info.gga.alt );
            sscanf( info.gga.alt, "%f,", &f );
            info.gga.altitude = (int)(f);

            info.gga.ds = GPS_DATA_VALID;
      }
      else
      {
            return -1;
      }

      return 0;
}

#define h_addr h_addr_list[0] /* for backward compatibility */

int main(int argc, char* argv[])
{
	int portno, n, res;
	struct sockaddr_in addr;
	struct hostent *serverg;
	char buffer[256];

	int serial_port = open("/dev/ttyUSB0", O_RDWR);
	if (serial_port < 0) {
	    printf("Error %i from open: %s\n", errno, strerror(errno));
	}

#if 0
/*	portno = 20629;
	serverg = gethostbyname("94.41.85.159");*/ /* navi.link */
	portno = 20629;
	serverg = gethostbyname("62.76.74.184"); /* navi.link */
#else
	portno = 30197;
	serverg = gethostbyname("193.232.47.4"); /*tr.gpshome.ru*/
#endif
	if (serverg == NULL)
	{
		egts_probe_printf("ERROR, no such host\n");
		exit(0);
	}

	bcopy((char *)serverg->h_addr, (char *)&addr.sin_addr.s_addr,  serverg->h_length);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		egts_probe_printf("ERROR opening socket");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(portno);
	/*addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);*/

	if ((res = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr))) < 0)
		error("ERROR connecting");


  egts_profile_t def_profile;
  u8             def_priority;
  egts_route_t   test_route;
  u16            PID;
  const tf_probe_send*      pfn_probe_send;
  egts_responce_header_t    responce;

  (void)argc;
  (void)argv;

  def_profile.CMP  = 0;
  def_profile.ENA  = EGTS_DATA_ENA_NONE;
  def_profile.HE   = EGTS_HEADER_ENCODING_NONE;
  def_profile.SGN  = 0;
  def_profile.SKID = 0;

  def_priority     = 0;

  test_route.PRA   = 1;
  test_route.RCA   = 1;
  test_route.TTL   = 1;

  probes_ctx.precords    = NULL;
  probes_ctx.nrecords    = 0;
  probes_ctx.nerr        = 0;
  probes_ctx.last_result = 0;

  estate_rx.cur_rx_packet = (egts_packet_state_t*)memalloc_func( sizeof(egts_packet_state_t) );

  egts_init( &estate_rx, &probes_ctx ,
    NULL ,
    estate_rx_packet_probe ,
    estate_tx_error ,
    estate_rx_error
    );

  estate_tx.cur_tx_packet = (egts_packet_state_t*)memalloc_func( sizeof(egts_packet_state_t) );

  egts_init( &estate_tx, &estate_rx ,
	socket_estate_tx_buffer_loop, /*estate_tx_buffer_loop ,*/
    NULL ,
    estate_tx_error ,
    estate_rx_error
    );

  /*
  *
  */


  PID = 1;
  responce.PR   = EGTS_PC_OK;
  responce.RPID = 1;

	fglob = fopen("myexchg1.bin", "wb");
	if (!fglob)
		exit(0);


  printf("\n");
  printf("Header encoding - NONE\n");
  printf("Data encoding - NONE\n");
  printf("Compression - NONE\n");
  printf("Sign-up - NONE\n");
  printf("Responce - NONE\n");
  printf("\n");
  u32 recvete = 0;
  u8 recvedByte = 0;
  for ( pfn_probe_send = egts_probes_vector;
        *pfn_probe_send;
        pfn_probe_send++ )
  {
	  if(0)
		{
		  usRxCounter = read(serial_port, &RxBuffer, sizeof(RxBuffer));
				if (!gps_serial_parse())
				{
					/*
					//invalidCtr = 0;
					//gps_info_show();
					//xQueueSend(xDataQueue, &info, 0);
					 *
					 */
				}
				else
				{
					/*
					//invalidCtr++;
					 *
					 */
				}
		}


	  send_buf_final_len = 0;
    (*pfn_probe_send)( &estate_tx , &def_profile , def_priority , NULL , PID, /*(PID==2)?&responce:*/NULL , temp_buf , 65535 );
	send(sockfd, &send_buf_final, send_buf_final_len, 0);
usleep(2000000);

estate_short_dump_packet(
		&(estate_tx.cur_tx_packet->header),
		def_profile.SGN ,
  NULL ,
  probes_ctx.precords ,
  probes_ctx.nrecords ,
  &estate_tx ,
  this_printf_dump
);

count = 0;
printf("Received: ");
#if 1
#if 0
	FILE *f = fopen("met4.bin", "wb");
#endif
     while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
     {
    	 count++;
   	  printf("%02x ", recvedByte);
   	  /*if (count > 29)*/
   	  fwrite(&recvedByte, sizeof(recvedByte), 1, fglob);
#if 1
   	  egts_rx_byte( (egts_state_t*)&estate_rx, recvedByte);
#endif
     }
     /*
   	//fclose(fglob);
     printf("\n");
     //exit(0);
      *
      */
#endif
     usleep(1000);
     /*
    	estate_short_dump_packet(
    			&(estate_rx.cur_rx_packet->header),
    			def_profile.SGN ,
    	  NULL ,
 	  estate_rx.cur_rx_packet->records ,
 	  estate_rx.cur_rx_packet->nrecords ,
    	  &estate_rx ,
    	  this_printf_dump
    	);
*/
    PID++;
	  recvete++;
#if 0
	  if (recvete == 1)
		    PID++;
	  if (recvete == 3)
	  exit(0);
#endif
#if 1
	  if (recvete > 4)
		  pfn_probe_send--;
#else
	  if ((recvete > 3) && ((recvete % 2) > 0))
		  pfn_probe_send -= 2;
#endif

  }
  pfn_probe_send = *egts_probes_vector[3];
  while(1)
    {

    (*pfn_probe_send)( &estate_tx , &def_profile , def_priority , NULL , PID, NULL/*&responce*/ , temp_buf , 65535 );
usleep(1000000);
count = 0;
printf("Received: ");
     while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
     {
    	 count++;
   	  printf("%02x ", recvedByte);
#if 1
   	  egts_rx_byte( (egts_state_t*)&estate_rx, recvedByte);
#endif
     }
     printf("\n");
     usleep(1000000);

    PID++;
  }

  usleep(100000);

  free( estate_rx.cur_rx_packet );
  free( estate_tx.cur_tx_packet );

  printf("\n");
  printf("erorr count: %u\n" , probes_ctx.nerr );
  if ( probes_ctx.nerr )
  {
    printf("autotests FAILED\n");
    return 1;
  }

  printf("autotests PASSED\n");
	return 0;
}

#else

//#define ram2 attribute((section(".RAM2")))
egts_state_t		estate_rx;
egts_state_t		estate_tx;
u8			/*attribute((section("RAM2")))	*/	temp_buf[2000]/* attribute((aligned(512)))*/;
u8					send_buf[2000];
int					send_buf_len = 0;
egts_probe_ctx_t	probes_ctx;

int socket_estate_tx_buffer_loop(void *ctx, void* pbuf, u32 sz)
{
	if ((send_buf_len + sz) < sizeof(send_buf))
	{
		memcpy(&(send_buf[send_buf_len]), pbuf, sz);
		send_buf_len += sz;
		return 0;
	}
	else
		return -1;
#if 0
	u8 *puc = (u8*)pbuf;
	int i=0;
	for (i=0; i<sz; i++)
		printf("%02x ", puc[i]);
#if 1
	send(sockfd, pbuf, sz, 0);
	fwrite(pbuf, sz, 1, fglob);
#endif
#if 0
	while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
	{
		printf("%02x ", recvedByte);
#if 1
		egts_rx_byte((egts_state_t*)ctx, recvedByte);
#endif
	}
	printf("\n");
#endif

#if 0
	for ( ; sz; sz-- )
		egts_rx_byte( (egts_state_t*)ctx, *puc++ );
#endif
#endif
}

void  estate_tx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	if ( dgb_str )
		egts_probe_printf( "tx error: %s\n" , dgb_str );
#else
	if ( dgb_str )
		printf( "tx error: %s\n" , dgb_str );
#endif
}

void  estate_rx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	if ( dgb_str )
		egts_probe_printf( "rx error: %s\n" , dgb_str );
#else
	if ( dgb_str )
		printf( "rx error: %s\n" , dgb_str );
#endif
}

/*
void egts_probe_printf( const char* fmt , ... )
{
#if 0
	va_list v;
	va_start(v,fmt);
	vprintf( fmt , v );
#endif
}
*/
extern char exchangeNoError;
unsigned char rx_egts_resp[1500];

void mainEGTS_Task()
{
	xEGTS_Sysmsg_Event xEvent;

	egts_profile_t def_profile;
	u8             def_priority;
	egts_route_t   test_route;
	u16            PID;
	const tf_probe_send*      pfn_probe_send;
	egts_responce_header_t    responce;

	def_profile.CMP  = 0;
	def_profile.ENA  = EGTS_DATA_ENA_NONE;
	def_profile.HE   = EGTS_HEADER_ENCODING_NONE;
	def_profile.SGN  = 0;
	def_profile.SKID = 0;
	def_priority     = 0;
	test_route.PRA   = 1;
	test_route.RCA   = 1;
	test_route.TTL   = 1;
	probes_ctx.precords    = NULL;
	probes_ctx.nrecords    = 0;
	probes_ctx.nerr        = 0;
	probes_ctx.last_result = 0;

	estate_rx.cur_rx_packet = (egts_packet_state_t*)pvPortMalloc( sizeof(egts_packet_state_t) );
	egts_init(&estate_rx, &probes_ctx,
		NULL,
		estate_rx_packet_probe,
		estate_tx_error,
		estate_rx_error
	);

	estate_tx.cur_tx_packet = (egts_packet_state_t*)pvPortMalloc( sizeof(egts_packet_state_t) );
	egts_init( &estate_tx, &estate_rx ,
		socket_estate_tx_buffer_loop, /*estate_tx_buffer_loop ,*/
		NULL,
		estate_tx_error,
		estate_rx_error
	);

	PID = 1;
	responce.PR   = EGTS_PC_OK;
	responce.RPID = 1000;
#if 0
	fglob = fopen("myexchg1.bin", "wb");
	if (!fglob)
		exit(0);
	printf("\n");
	printf("Header encoding - NONE\n");
	printf("Data encoding - NONE\n");
	printf("Compression - NONE\n");
	printf("Sign-up - NONE\n");
	printf("Responce - NONE\n");
	printf("\n");

	---------------------------------------------------------
	count = 0;
	printf("Received: ");
	#if 1
	#if 0
		FILE *f = fopen("met4.bin", "wb");
	#endif
	     while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
	     {
	    	 count++;
	   	  printf("%02x ", recvedByte);
	   	  /*if (count > 29)*/
	   	  fwrite(&recvedByte, sizeof(recvedByte), 1, fglob);
	#if 1
	   	  egts_rx_byte( (egts_state_t*)&estate_rx, recvedByte);
	#endif
	     }
	     /*
	   	//fclose(fglob);
	     printf("\n");
	     //exit(0);
	      *
	      */
	#endif
	     usleep(1000);
	     /*
	    	estate_short_dump_packet(
	    			&(estate_rx.cur_rx_packet->header),
	    			def_profile.SGN ,
	    	  NULL ,
	 	  estate_rx.cur_rx_packet->records ,
	 	  estate_rx.cur_rx_packet->nrecords ,
	    	  &estate_rx ,
	    	  this_printf_dump
	    	);
	*/
	    PID++;
		  recvete++;
		  if (recvete == 1)
			    PID++;
	#if 0
		  if (recvete == 3)
		  exit(0);
	#endif
	#if 1
		  if (recvete > 3)
			  pfn_probe_send--;
	#else
		  if ((recvete > 3) && ((recvete % 2) > 0))
			  pfn_probe_send -= 2;
	#endif

	  }
	  pfn_probe_send = *egts_probes_vector[3];
	  while(1)
	    {

	    (*pfn_probe_send)( &estate_tx , &def_profile , def_priority , NULL , PID, NULL/*&responce*/ , temp_buf , 65535 );
	usleep(1000000);
	count = 0;
	printf("Received: ");
	     while(recv(sockfd, &recvedByte, 1, MSG_DONTWAIT ) > 0)
	     {
	    	 count++;
	   	  printf("%02x ", recvedByte);
	#if 1
	   	  egts_rx_byte( (egts_state_t*)&estate_rx, recvedByte);
	#endif
	     }
	     printf("\n");
	     usleep(1000000);

	    PID++;
	  }
#endif

	  char isconnected = 0;
	BaseType_t resQue;
	u32 recvete = 0;
	osDelay(4000);

	while(1)
	{
		PID = 1;
		recvete = 0;
		isconnected = 0;
#if 0
		HAL_GPIO_WritePin(_3V3_1_EN_GPIO_Port, _3V3_1_EN_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(_5V_EN_GPIO_Port, _5V_EN_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(_4V_EN_GPIO_Port, _4V_EN_Pin, GPIO_PIN_SET);
		osDelay(10);
		//HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, GPIO_PIN_SET);

		usb_debug("-=EGTS=- Powering ON SKZI... \r\n");
		if (power_On_SKZI() == ERR_OK)
		{
			usb_debug("-=EGTS=- Powering ON SKZI... OK. Test SKZI \r\n");
			int trysCount = 0;
			while (get_state_SKZI() != ERR_OK)
			{
				osDelay(50);
				trysCount++;
				if (trysCount > 9000)
				{
					usb_debug("\r\n\r\n\r\nError waiting starting SKZI module!. Reseting... \r\n\r\n\r\n");
					osDelay(1000);
					NVIC_SystemReset();
				}
			}
			usb_debug("-=EGTS=- IMEI: %s, CIMI: %s \r\n", strIMEI, strCIMI);
			usb_debug("-=EGTS=- Test SKZI... OK. Connecting EGTS server \r\n");
			if (try_connect_SKZI("EGTS server", 3089, 100) == ERR_OK)
			{
				usb_debug("-=EGTS=- Connecting EGTS server... OK \r\n");
				while(1)
				{
					resQue = xQueueReceive(egtsEventQueue, &xEvent, 1/*portMAX_DELAY*/);
					if (resQue == pdPASS)
					{

						if ((xEvent.msg == SKZI_EGTS_SYSMSG_CONNECTED)/* && (isconnected == 0)*/)
						{
							isconnected = 1;
							pfn_probe_send = egts_probes_vector; /*()*pfn_probe_send; pfn_probe_send++)*/
							{
								send_buf_len = 0;
								(*pfn_probe_send)(&estate_tx, &def_profile, def_priority, NULL, PID, NULL/*&responce*/, temp_buf, 3000);
								if (send_buf_len > 0)
								{
									send_SKZI(&send_buf, send_buf_len, 100);
									//m66SendMsg(&send_buf, send_buf_len);
									//espSendMsg(&send_buf, send_buf_len);
									memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
									len_ostr = 0;
									egts_debug_ctx(NULL, "\r\n");
									estate_short_dump_packet(
											&(estate_tx.cur_tx_packet->header),
											def_profile.SGN ,
									  NULL ,
									  probes_ctx.precords ,
									  probes_ctx.nrecords ,
									  &estate_tx ,
									  egts_debug_ctx
									);
									if (len_ostr > 0)
										usb_debug_fix(&deb_egts_buf, len_ostr);
								}
							}
						}
						else
							if ((xEvent.msg == SKZI_EGTS_SYSMSG_RECEVD) && (isconnected == 1))
							{
								if ((xEvent.size > 0) && (xEvent.data != NULL))
								{
									memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
									len_ostr = 0;
									usb_debug_ctx(NULL, "\r\nRX : ");
									for (int i=0; i<xEvent.size; i++)
										usb_debug_ctx(NULL, "%02X ", rx_egts_resp[i]);
									if (len_ostr > 0)
										usb_debug_fix(&deb_egts_buf, len_ostr);

									memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
									len_ostr = 0;
									unsigned char *ptr = (unsigned char *)&rx_egts_resp;
									for (int i=0; i<xEvent.size; i++)
										egts_rx_byte( (egts_state_t*)&estate_rx,  *ptr++);
									if (len_ostr > 0)
										usb_debug_fix(&deb_egts_buf, len_ostr);
								}
								//for (int i=0; i<probes_ctx.nrecords; i++)
								{
									for (int j=0; j<probes_ctx.precords->nsubrecords; j++)
									{
										vPortFree(probes_ctx.precords->psubrecords[j].SRD);
										vPortFree(probes_ctx.precords->psubrecords);
									}
									vPortFree(probes_ctx.precords);
								}
								if ((*pfn_probe_send) == NULL)
									continue;
								PID++;
								recvete++;
								pfn_probe_send++;
								//if ((recvete > 4) && (recvete < 6))
								//	pfn_probe_send--;
								if ((*pfn_probe_send) != NULL)
								{
									send_buf_len = 0;
									(*pfn_probe_send)(&estate_tx, &def_profile, def_priority, NULL, PID, NULL/*&responce*/, temp_buf, 8000);
									if (send_buf_len > 0)
									{
										HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, GPIO_PIN_SET);
										send_SKZI(&send_buf, send_buf_len, 100);
										//m66SendMsg(&send_buf, send_buf_len);
										//espSendMsg(&send_buf, send_buf_len);
//										if (recvete == 4)
	//										pfn_probe_send++;
										memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
										len_ostr = 0;
										egts_debug_ctx(NULL, "\r\n");
										estate_short_dump_packet(
												&(estate_tx.cur_tx_packet->header),
												def_profile.SGN ,
										  NULL ,
										  probes_ctx.precords ,
										  probes_ctx.nrecords ,
										  &estate_tx ,
										  egts_debug_ctx
										);
										if (len_ostr > 0)
											usb_debug_fix(&deb_egts_buf, len_ostr);
									}
								}
								else
								{
									disconnect_SKZI();
								}
							}
							else
								if (xEvent.msg == SKZI_EGTS_SYSMSG_DISCONNECTED)
								{
									HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_SET);
									PID = 1;
									recvete = 0;
									isconnected = 0;
									//if (info.fileCreated)
									{
										//HAL_GPIO_WritePin(_3V3_1_EN_GPIO_Port, _3V3_1_EN_Pin, GPIO_PIN_RESET);
										HAL_GPIO_WritePin(_5V_EN_GPIO_Port, _5V_EN_Pin, GPIO_PIN_RESET);
										HAL_GPIO_WritePin(_4V_EN_GPIO_Port, _4V_EN_Pin, GPIO_PIN_RESET);
									}
									break;
								}
						}
						else
						{
						}
				} //while(1)
				usb_debug("-=EGTS=- Main cycle finished... Sleeping \r\n");
			}
			else
				usb_debug("-=EGTS=- Connecting EGTS server... FAILED \r\n");
		}
		else
			usb_debug("-=EGTS=- Powering ON SKZI... FAILED \r\n");
		//HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, GPIO_PIN_RESET);
#endif
		//osDelay(60000);
		static uint32_t ptpbcounter = 1;
#if 0
		if (info.fileCreated == 0)
		{
			int smCtr = 0;
			while (smCtr < 60 * 20)
			{
				osDelay(1000);
				if (info.fileCreated)
					break;
			}
		}
#endif
//exchangeNoError = 1;
		if (exchangeNoError)
		{
#if 0
			plState.sendOK = 1;
			plState.transmitOK++;
			plState.lastError = 0;

			if (plState.nowState == EPLUMBSTATE_ARMED)
			{
			/*
	  			if (plState.cmd == EPLUMBCMD_STATUS_CHANGED)
					plState.cmd = EPLUMBCMD_LOCATE_COORDINATES;
					else
	  					if (plState.cmd == EPLUMBCMD_SEND_COORDINATES)
							plState.cmd = EPLUMBCMD_NOTHING_TO_SEND;
*/

			}
			else
				if (plState.nowState == EPLUMBSTATE_ALARMED)
				{
		  			if ((plState.cmd == EPLUMBCMD_STATUS_CHANGED) || (plState.cmd == EPLUMBCMD_ALARM_OPTO) || (plState.cmd == EPLUMBCMD_ALARM_BUTTON) || (plState.cmd == EPLUMBCMD_ALARM_OPTO_BUTTON))
						plState.cmd = EPLUMBCMD_LOCATE_COORDINATES;
						else
	 	 					if (plState.cmd == EPLUMBCMD_SEND_COORDINATES)
								plState.cmd = EPLUMBCMD_NOTHING_TO_SEND;
							else
	 	 						if (plState.cmd == EPLUMBCMD_GO_OPENED)
	 	 						{
	 	 							plState.nowState = EPLUMBSTATE_OPENED;
									plState.cmd = EPLUMBCMD_NOTHING_TO_SEND;
								}
				}
#endif
		}
		else
		{
#if 0
			plState.sendOK = 0;
			plState.transmitFAILED++;
			if (plState.lastError == 0)
				plState.lastError = 2;
#endif
		}
#if 0
		logwakeup();
		osDelay(500);
		while (ptpbcounter < 15)
		{
			ptpbcounter++;
			osDelay(200);
			HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
			usb_debug("-=EGTS=- Finished exchange, Ready to reboot... \r\n");
		}
#endif
		if (exchangeNoError)
		{
				//sleeping
#if 0
				powerOffPerephirial();

								  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET); // Led1 + 2 - 3-color LED
								  HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); // led 1 - green, led 2 - red, both - yellow
								  HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
								  HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_SET);
								  HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, GPIO_PIN_RESET);
								  HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, GPIO_PIN_RESET);
								  HAL_GPIO_WritePin(_3V3_1_EN_GPIO_Port, _3V3_1_EN_Pin, GPIO_PIN_RESET);
										HAL_GPIO_WritePin(_5V_EN_GPIO_Port, _5V_EN_Pin, GPIO_PIN_RESET);
										HAL_GPIO_WritePin(_4V_EN_GPIO_Port, _4V_EN_Pin, GPIO_PIN_RESET);

								  //HAL_PWREx_EnableInternalWakeUpLine();
								  //HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN5_LOW);
								  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
								  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN2);
								  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN3);
								  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN4);
								  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN5);

								  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN5_HIGH);
								  HAL_PWREx_EnableInternalWakeUpLine();
								  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);


								  HAL_NVIC_DisableIRQ(DMA2_Channel3_IRQn);
								  HAL_NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);
								  HAL_NVIC_DisableIRQ(UART4_IRQn);
								  HAL_NVIC_DisableIRQ(USART1_IRQn);
								  HAL_NVIC_DisableIRQ(USART2_IRQn);
								  HAL_NVIC_DisableIRQ(USART3_IRQn);
								  HAL_NVIC_DisableIRQ(ADC1_2_IRQn);

								  GPIO_InitTypeDef GPIO_InitStruct = {0};

								  /*Configure GPIO pin : PE6 */
								  GPIO_InitStruct.Pin = GPIO_PIN_6;
								  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
								  GPIO_InitStruct.Pull = GPIO_NOPULL;
								  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

								  /*Configure GPIO pins : PC13 PC5 */
								  GPIO_InitStruct.Pin = GPIO_PIN_13;
								  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
								  GPIO_InitStruct.Pull = GPIO_NOPULL;
								  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

								  HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
								  HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

								  HAL_GPIO_WritePin(TIM_RST_GPIO_Port, TIM_RST_Pin, GPIO_PIN_RESET);
								HAL_GPIO_WritePin(OPTO_TX_GPIO_Port, OPTO_TX_Pin, GPIO_PIN_RESET);


								  HAL_PWREx_EnableInternalWakeUpLine();
								  LL_RTC_DisableWriteProtection(RTC);
								  LL_RTC_WAKEUP_Disable(RTC);
								  while(!LL_RTC_IsActiveFlag_WUTW(RTC))
								  {
									  ;
								  }
								  LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_CKSPRE);
								  LL_RTC_WAKEUP_SetAutoReload(RTC, TIME_WAITING_IN_ARMED_STATE_S);
								  LL_RTC_ClearFlag_WUT(RTC);
								  // Enable rising edge external interrupt on line 20
								  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_20);
								  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_20);

								  LL_RTC_EnableIT_WUT(RTC);
								  LL_RTC_WAKEUP_Enable(RTC);
								  LL_RTC_EnableWriteProtection(RTC);
								  HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
								  //LL_RTC_WAKEUP_Disable(RTC);
								  LL_RTC_ClearFlag_WUT(RTC);
								  HAL_PWREx_DisableLowPowerRunMode();
#endif
}
		NVIC_SystemReset();
	} //while(1)
}

void vEGTS_SendMsg(SKZI_EGTS_SYSMSG msgType, void *dataIn, int sizeIn)
{
	memcpy(&rx_egts_resp, dataIn, (sizeIn > sizeof(rx_egts_resp)) ? sizeof(rx_egts_resp) : sizeIn);
	xEGTS_Sysmsg_Event event = {msgType, &rx_egts_resp, sizeIn};
	unsigned portBASE_TYPE xStatus = xQueueSend(egtsEventQueue, &event, 0);
	if (xStatus == pdFALSE)
	{
		//my_strncpy( debug_buf, "\r\ngprs event queue full!", DEBUG_BUF_SIZE );
		//vMyDataStreamOut( ( unsigned portCHAR* )debug_buf, strlen( debug_buf ) );
	}

}

int main_transport_egts_func()
{
	egtsEventQueue = xQueueCreate(EGTS_SYSMSG_EVENT_QUEUE_SIZE, (unsigned portBASE_TYPE)sizeof(xEGTS_Sysmsg_Event));
	if (egtsEventQueue == NULL)
		return ERR_NO_MEMORY;

	xTaskCreate(mainEGTS_Task, "egts_main", configMINIMAL_STACK_SIZE + 350, NULL, 1, &xTaskEGTSMainHandle);
	if (xTaskEGTSMainHandle)
	{
		/*
		my_strncpy( debug_buf, "\r\nESP32 task1 created", DEBUG_BUF_SIZE );
		vDataStreamOut( ( unsigned portCHAR* )debug_buf, strlen( debug_buf ) );
		snprintf( debug_buf, DEBUG_BUF_SIZE, "\r\nFree heap size = %ld", xPortGetFreeHeapSize() );
		vDataStreamOut( ( unsigned portCHAR* )debug_buf, strlen( debug_buf ) );
		*/
	}
	else
	{
		return ERR_NO_MEMORY;
	}
	return 0;
}

#endif
