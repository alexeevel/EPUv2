/*
 * egts_my_probe.h
 *
 *  Created on: 2 мая 2020 г.
 *      Author: abutko
 */

#ifndef PROBES_EGTS_MY_PROBE_H_
#define PROBES_EGTS_MY_PROBE_H_

typedef enum SKZI_EGTS_SYSMSG_
{
	SKZI_EGTS_SYSMSG_NONE = 0,
	SKZI_EGTS_SYSMSG_POWER_ON,
	SKZI_EGTS_SYSMSG_POWER_OFF,
	SKZI_EGTS_SYSMSG_CONNECTED,
	SKZI_EGTS_SYSMSG_DISCONNECTED,
	SKZI_EGTS_SYSMSG_SENTOK,
	SKZI_EGTS_SYSMSG_RECEVD,
} SKZI_EGTS_SYSMSG;

typedef struct xEGTS_Sysmsg_Event_
{
	SKZI_EGTS_SYSMSG msg;
	void *data;
	int size;
} xEGTS_Sysmsg_Event;

void vEGTS_SendMsg(SKZI_EGTS_SYSMSG msgType, void *dataIn, int sizeIn);
int main_transport_egts_func();

#endif /* PROBES_EGTS_MY_PROBE_H_ */
