/*
 * egts.cpp
 *
 *  Created on: 24 июн. 2021 г.
 *      Author: abutko
 */

#include "egts.hpp"

#include "skzi/gps.h"

GPS_INFO info;

void setDefValue()
{
#ifndef NO_GPS
	info.rmc.ds = GPS_DATA_VALID;
	info.rmc.utc_s = 0;
	info.rmc.utc_m = 35;
	info.rmc.utc_h = 10;
	info.rmc.dt_d = 3;
	info.rmc.dt_m = 8;
	info.rmc.dt_y = 20;
	//info.rmc.lat_float = 54.726288;
	//info.rmc.long_float = 55.947727;
	info.rmc.lat_float = 55.733637;
	info.rmc.long_float = 37.577652;
	info.rmc.lat_dir = GPS_LAT_N;
	info.rmc.long_dir = GPS_LONG_E;
	info.rmc.speed = 0;
	info.gga.ds = GPS_DATA_VALID;
	info.gga.altitude = 100.0;
	info.isEmul = 1;
	info.gm.tm_year = 0;
	info.alarmInOpto = 0;
	info.alarmInBtn = 0;
#endif
}

u8			/*attribute((section("RAM2")))	*/	temp_buf[2000]/* attribute((aligned(512)))*/;
u8					send_buf[500];
static int					send_buf_len = 0;
egts_probe_ctx_t	probes_ctx;

int socket_estate_tx_buffer_loop(void *ctx, void* pbuf, u32 sz)
{
	if ((send_buf_len + sz) < sizeof(send_buf))
	{
		memcpy(&(send_buf[send_buf_len]), pbuf, sz);
		send_buf_len += sz;
		return 0;
	}
	else
		return -1;
}

void  estate_tx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	if ( dgb_str )
		egts_probe_printf( "tx error: %s\n" , dgb_str );
#else
	if ( dgb_str )
		printf( "tx error: %s\n" , dgb_str );
#endif
}

void  estate_rx_error( void* actx , u16 PID , u8 err , const char* dgb_str )
{
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	if ( dgb_str )
		egts_probe_printf( "rx error: %s\n" , dgb_str );
#else
	if ( dgb_str )
		printf( "rx error: %s\n" , dgb_str );
#endif
}

char deb_egts_buf[256];
static int len_ostr = 0;
unsigned char rx_egts_resp[500];

extern "C" void egts_debug_ctx(void *ctx, const char *x, ...);
extern "C" void  estate_short_dump_packet(
  egts_header_t*            pheader ,
  u8                        signed_up ,
  egts_responce_header_t*   presponce ,
  egts_record_t*            precords ,
  u16                       nrecords ,
  void* ctx ,
  void (* fn_dump)( void* actx , const char* fmt , ... )
  );
void usb_debug_fix(const char *x, int size)
{

}
void usb_debug_ctx(void *ctx, const char *x, ...)
{

}

bool EGTS::EGTS_Init()
{
	def_profile.CMP  = 0;
	def_profile.ENA  = EGTS_DATA_ENA_NONE;
	def_profile.HE   = EGTS_HEADER_ENCODING_NONE;
	def_profile.SGN  = 0;
	def_profile.SKID = 0;
	def_priority     = 0;
	test_route.PRA   = 1;
	test_route.RCA   = 1;
	test_route.TTL   = 1;
	probes_ctx.precords    = NULL;
	probes_ctx.nrecords    = 0;
	probes_ctx.nerr        = 0;
	probes_ctx.last_result = 0;

	estate_rx.cur_rx_packet = (egts_packet_state_t*)pvPortMalloc( sizeof(egts_packet_state_t) );
	egts_init(&estate_rx, &probes_ctx,
		NULL,
		estate_rx_packet_probe,
		estate_tx_error,
		estate_rx_error
	);

	estate_tx.cur_tx_packet = (egts_packet_state_t*)pvPortMalloc( sizeof(egts_packet_state_t) );
	egts_init( &estate_tx, &estate_rx ,
		socket_estate_tx_buffer_loop, /*estate_tx_buffer_loop ,*/
		NULL,
		estate_tx_error,
		estate_rx_error
	);

	PID = 1;
	responce.PR   = EGTS_PC_OK;
	responce.RPID = 1000;

	pfn_probe_send = egts_probes_vector; /*()*pfn_probe_send; pfn_probe_send++)*/

	return true;
}

void EGTS::EGTS_Deinit()
{
	vPortFree(estate_rx.cur_rx_packet);
	vPortFree(estate_tx.cur_tx_packet);
}

bool EGTS::nextPacket(char **outBuf, unsigned int *outSize)
{
	//if ((recvete > 4) && (recvete < 6))
	//	pfn_probe_send--;
	if ((*pfn_probe_send) != NULL)
	{
		send_buf_len = 0;
		(*pfn_probe_send)(&estate_tx, &def_profile, def_priority, NULL, PID, NULL/*&responce*/, temp_buf, 3000);
		if (send_buf_len > 0)
		{
			//send_SKZI(&send_buf, send_buf_len, 100);
			memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
			len_ostr = 0;
			egts_debug_ctx(NULL, "\r\n");
			estate_short_dump_packet(
					&(estate_tx.cur_tx_packet->header),
					def_profile.SGN ,
			  NULL ,
			  probes_ctx.precords ,
			  probes_ctx.nrecords ,
			  &estate_tx ,
			  egts_debug_ctx
			);
			if (len_ostr > 0)
				usb_debug_fix((const char *)&deb_egts_buf, len_ostr);
			*outBuf = (char *)&send_buf;
			*outSize = send_buf_len;

			PID++;
			//recvete++;
			pfn_probe_send++;

			return true;
		}
		else
		{
			*outBuf = NULL;
			*outSize = 0;
			return false;
		}
	}
	else
	{
		*outBuf = NULL;
		*outSize = 0;
		return false;
	}
}

bool EGTS::analyzePacket(const char *inBuf, const unsigned int inSize)
{
#if 1
	if ((inSize > 0) && (inBuf != NULL))
	{
		memcpy(&rx_egts_resp, inBuf, (inSize > sizeof(rx_egts_resp)) ? sizeof(rx_egts_resp) : inSize);
		memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
		len_ostr = 0;
		usb_debug_ctx(NULL, "\r\nRX : ");
		for (unsigned int i=0; i<inSize; i++)
			usb_debug_ctx(NULL, "%02X ", rx_egts_resp[i]);
		if (len_ostr > 0)
			usb_debug_fix((char *)&deb_egts_buf, len_ostr);

		memset(&deb_egts_buf, 0, sizeof(deb_egts_buf));
		len_ostr = 0;
		unsigned char *ptr = (unsigned char *)&rx_egts_resp;
		for (unsigned int i=0; i<inSize; i++)
			egts_rx_byte( (egts_state_t*)&estate_rx,  *ptr++);
		if (len_ostr > 0)
			usb_debug_fix((char *)&deb_egts_buf, len_ostr);
	}
#endif
	//for (int i=0; i<probes_ctx.nrecords; i++)
	{
		for (int j=0; j<probes_ctx.precords->nsubrecords; j++)
		{
			vPortFree(probes_ctx.precords->psubrecords[j].SRD);
			vPortFree(probes_ctx.precords->psubrecords);
		}
		vPortFree(probes_ctx.precords);
	}
	return true;
}

