#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sms_types.h"

/* maximum pdu reading size */
#define MAX_STRING_LENGTH 255

/************************************************************************
  Input file structure:

  1 string - PDU
  2 string - predicted error
  non used all other strings with PDU in text description
************************************************************************/

u8 char_to_digit (
  u8 in_char
)
{
  if ( in_char > (u8)47 && in_char < (u8)58 )
    return in_char - (u8)48;
  if ( in_char > (u8)64 && in_char < (u8)71 )
    return in_char - (u8)55;

  fprintf ( stderr, "Unable to get hexadecimal from char value\n" );
  exit ( 1 );
}

int main_pdu_get ( int argc, char *argv[] )
{
  FILE *fd = NULL;
  u8 *pdu_text = NULL;
  u8 *pdu = NULL;
  u32 pdu_len;
  SMS_error err_code;
  SMS_error err_code_new;
  u16 i, j;
  SMS_DELIVER_T sd;
  int rval = 0;

  if ( argc != 2 )
  {
    fprintf ( stderr, "Use pdu_test.exe <test file name>\n" );
    return 1;
  }

  fd = fopen (argv[1], "r");
  
  if ( fd == NULL )
  {
    fprintf ( stderr, "Unable to open file:%s\n", argv[1] );
    return 1;
  }

  pdu_text = (u8*) calloc ( MAX_STRING_LENGTH , sizeof (u8) );
  if ( pdu_text == NULL )
  {
    fprintf ( stderr, "%s Allocate memory for PDU text presentation failed\n", argv[1] );
    rval = 1;
    goto finish;
  }
 
  if ( fgets ( (char*)pdu_text, MAX_STRING_LENGTH, fd ) == NULL )
  {
    fprintf ( stderr, "%s Unable to read PDU\n", argv[1] );
    rval = 1;
    goto finish;
  }

  if ( feof ( fd ) )
  {
    fprintf ( stderr, "%s Unexpected end of file\n", argv[1] );
    rval = 1;
    goto finish;
  }
  
  pdu_len = strlen ( (const char*)pdu_text );
  if ( pdu_len < 10 )
  {
    fprintf ( stderr, "%s Abnormal PDU size\n", argv[1] );
    rval = 1;
    goto finish;
  }
  
  pdu_len -= 1; /* Cut null terminator symbol */

  pdu = (u8*) calloc ( pdu_len , sizeof(u8) );
  if ( pdu == NULL )
  {
    fprintf ( stderr, "%s Allocate memory for PDU failed\n", argv[1] );
    rval = 1;
    goto finish;
  }
  
  if ( fscanf ( fd, "%d", &err_code ) != 1 )
  {
    fprintf ( stderr, "%s Unable to read predicted error code\n", argv[1] );
    rval = 1;
    goto finish;
  }

  fprintf ( stdout, "PDU[%d]:\n", pdu_len );
  for ( j = 0, i = 0; i != pdu_len; i+= 2 )
  {
    pdu[j] = char_to_digit ( pdu_text[i] );
    pdu[j] <<= 4;
    pdu[j] += char_to_digit ( pdu_text[i + 1] );
    fprintf ( stdout, "%c%c", pdu_text[i], pdu_text[i + 1] );
    j++;
  }
  fprintf ( stdout, "\n" );

  fprintf ( stdout, "Predicted error code:\n%d\n", err_code );

  /* WARNING! HACK: Hard coded error initiation */
  if ( err_code == SMS_GFE_OUT_OF_BOUNDS )
  {
      /* Resize pdu_len for test PDU length oversize error */
      fprintf ( stdout, "Resized pdu_len = 11! for raising error condition.\n" );
      pdu_len = 11;
  }

/* START prepare for extract structure data from array */
  sd.disable_smsc_field = 0;
/* STOP prepare for extract structure data from array */

  err_code_new = sms_pdu_get ( pdu, pdu_len, &sd ); /* extract function call */

  if ( err_code_new != err_code )
  {
    fprintf ( stderr, "%s Error code mismatch get:\n%d\n", argv[1], err_code_new );
    rval = 1;
    goto finish;
  }

finish:

/* START free allocated array and structure */  
  free ( pdu_text );
  pdu_text = NULL;
  free ( pdu );
  pdu = NULL;
/* STOP free allocated array and structure */

  fclose ( fd );

  if ( rval == 0 )
    fprintf ( stdout, "exit success\n" );

  return rval;
}
