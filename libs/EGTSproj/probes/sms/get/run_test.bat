@ECHO OFF
@del /F /Q get_tests.log
@del /F /Q errors.log
::SMS-DELIVER PDU tests routine
FOR /f "tokens=* delims=" %%a in (.\tests.list) do (
ECHO %%a >>get_tests.log
.\pdu_get.exe %%a 1>>get_tests.log 2>>errors.log
)
