#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sms_types.h"

/************************************************************************
  Input file structure:

  1 values and comments, one per string
  2 predicted error
  3 Generated PDU message
************************************************************************/

/* maximum string reading size */
#define MAX_STRING_LENGTH 255

SMS_SUBMIT_T ss;
char *comment;
FILE *fd;

u8 alloc_mem ( void );
void free_mem ( void );
u8 read_comment ( void );
u8 read_hex ( u8* );
u8 read_2hex ( u16* );
u8 read_string ( char* );
u8 read_dec ( u8* );
u8 read_long ( u64* );

int main_pdu_set ( int argc, char *argv[] )
{
  u8 rval = 0;
  u8 dta;
  u8 *data = NULL;
  u32 data_len = 33; /* predicted value */
  u8 *pdu = NULL;
  u32 pdu_len;
	u32 s;
  int err_code = 0, new_err_code;
  u16 i;

  if ( argc != 2 )
  {
    fprintf ( stderr, "Use pdu_set.exe <test file name>\n" );
    return 1;
  }

  fd = NULL;
  fd = fopen (argv[1], "r");
  
  if ( fd == NULL )
  {
    fprintf ( stderr, "Unable to open file:%s\n", argv[1] );
    return 1;
  }

  if ( alloc_mem () != 0 )
    return 1;

  data = (u8*) memalloc_func ( data_len * sizeof ( u8 ) );
  if ( data == NULL )
  {
    fprintf ( stderr, "%s allocate error\n", argv[1] );
    rval = 1;
    goto finish;
  }
  memset ( data, 0x00, data_len );

  pdu = (u8*) memalloc_func ( MAX_STRING_LENGTH * sizeof ( u8 ) );
  if ( pdu == NULL )
  {
    fprintf ( stderr, "%s allocate error\n", argv[1] );
    rval = 1;
    goto finish;
  }
  memset ( pdu, 0x00, MAX_STRING_LENGTH );

  if ( read_hex ( &ss.disable_smsc_field ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.include_smsc ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.smsc.type_of_address.type_of_number ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.smsc.type_of_address.num_plan_id ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_string ( (char*)ss.smsc_number_in_string ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_dec ( &ss.smsc_number_in_string_len ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.fo.tp_mti = (u32)dta;
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.fo.tp_rd = (u32)dta;
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.fo.tp_srr = (u32)dta;
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.fo.tp_udhi = (u32)dta;
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.fo.tp_rp = (u32)dta;
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.tp_mr = dta;
  if ( read_hex ( &ss.tp_da.type_of_address.type_of_number ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_da.type_of_address.num_plan_id ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_string ( (char*)ss.tp_da_number_in_string ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_dec ( &ss.tp_da_number_in_string_len ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.tp_pid.bit0 = (u32)( dta & 0x01 );
  ss.tp_pid.bit1 = (u32)( (dta >> 1) & 0x01 );
  ss.tp_pid.bit2 = (u32)( (dta >> 2) & 0x01 );
  ss.tp_pid.bit3 = (u32)( (dta >> 3) & 0x01 );
  ss.tp_pid.bit4 = (u32)( (dta >> 4) & 0x01 );
  ss.tp_pid.bit5 = (u32)( (dta >> 5) & 0x01 );
  ss.tp_pid.bit6 = (u32)( (dta >> 6) & 0x01 );
  ss.tp_pid.bit7 = (u32)( (dta >> 7) & 0x01 );
  if ( read_hex ( &dta ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  ss.tp_dcs.octet_high = (u32)dta;
  if ( read_hex ( &dta ) != 0)
  {
    rval = 1;
    goto finish;
  }
  ss.tp_dcs.octet_low = (u32)dta;
  if ( read_dec ( (unsigned char*)&ss.used_time_format ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_long ( &ss.time_in_seconds ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_udl ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_ud.udhl ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_ud.ie_ident ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_ud.ie_len ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_2hex ( &ss.tp_ud.ie_dat.message_ref_num ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_ud.ie_dat.message_count ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_hex ( &ss.tp_ud.ie_dat.message_num ) != 0 )
  {
    rval = 1;
    goto finish;
  }
  if ( read_string ( (char*)ss.tp_ud.sm ) != 0 )
  {
    rval = 1;
    goto finish;
  }

  if ( fscanf ( fd, "%d", &err_code ) != 1 )
  {
    fprintf ( stderr, "%s Read error\n", argv[1] );
    rval = 1;
    goto finish;
  }

  fprintf ( stdout, "Predicted error code:\n%d\n", err_code );

  new_err_code = sms_pdu_set ( data, &data_len, &ss );

  if ( new_err_code != err_code )
  {
    fprintf ( stderr, "%s Error code mismatch:%d\n", argv[1], new_err_code );
    rval = 1;
    goto finish;
  }

  for ( i = 0; i != data_len; i++ )
    printf ( "%.2X", data[i] );
  printf ( "\n" );

  for ( pdu_len = 0; pdu_len != MAX_STRING_LENGTH; pdu_len++ )
		if ( fscanf ( fd, "%2X",  &s ) != 1 )
      break; /* end of file reached */
    else {
			pdu[pdu_len] = (u8)s;
      fprintf ( stdout, "%.2X", pdu[pdu_len] );
		}
  fprintf ( stdout, "\n" );

  if ( new_err_code == SMS_PSE_PREFIX )
  {
    if ( pdu_len != data_len )
    {
      fprintf ( stderr, "%s PDU length mismatch %d %d\n", argv[1], pdu_len, data_len );
      rval = 1;
      goto finish;
    }
    
    for ( i = 0; i != data_len; i++ )
    {
      if ( pdu[i] != data[i] )
      {
        fprintf ( stderr, "%s Data mismatch\n", argv[1] );
        rval = 1;
        goto finish;
      }
    }
  }

finish:
  free ( data );
  data = NULL;
  free ( pdu );
  pdu = NULL;
  free_mem ();
  
  fclose ( fd );

  if ( rval == 0 )
    fprintf ( stdout, "exit success\n" );

  return rval;
}

u8 alloc_mem ( void )
{
  u8 rval = 0;
  
  ss.smsc_number_in_string = NULL;
  ss.smsc_number_in_string = (u8*) memalloc_func ( MAX_STRING_LENGTH * sizeof(u8) );

  if ( ss.smsc_number_in_string == NULL )
  {
    fprintf ( stderr, "allocate error\n" );
    rval = 1;
    goto finish;
  }

  ss.tp_da_number_in_string = NULL;
  ss.tp_da_number_in_string = (u8*) memalloc_func ( MAX_STRING_LENGTH * sizeof(u8) );

  if ( ss.tp_da_number_in_string == NULL )
  {
    fprintf ( stderr, "allocate error\n" );
    rval = 1;
    goto finish;
  }

  comment = NULL;
  comment = (char*) memalloc_func ( MAX_STRING_LENGTH * sizeof(char) );
  if ( comment == NULL )
  {
    fprintf ( stderr, "allocate error\n" );
    rval = 1;
    goto finish;
  }
  
finish:
  if ( rval == 1 )
    free_mem();

  return rval;
}

void free_mem ( void )
{
  free ( ss.smsc_number_in_string );
  ss.smsc_number_in_string = NULL;

  free ( ss.tp_da_number_in_string );
  ss.tp_da_number_in_string = NULL;

  free ( comment );
  comment = NULL;
}

u8 read_comment ( void )
{
  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fgets ( comment, MAX_STRING_LENGTH, fd ) == NULL )
  {
    fprintf ( stderr, "Commentary read error\n" );
    return 1;
  }
  
  printf ( "%s", comment );
  return 0;
}

u8 read_hex ( u8 *data )
{
  u32 symbol;

  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fscanf ( fd, "%4X", &symbol ) != 1 )
  {
    fprintf ( stderr, "Read error\n" );
    return 1;
  }
  
  *data = (u8) symbol;
  fprintf ( stdout, "0x%.2X\t\t", *data );

  if ( read_comment() != 0)
    return 1;
  
  return 0;
}

u8 read_2hex ( u16 *data )
{
  u32 symbol;

  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fscanf ( fd, "%6X", &symbol ) != 1 )
  {
    fprintf ( stderr, "Read error\n" );
    return 1;
  }
  
  *data = (u16) symbol;
  fprintf ( stdout, "0x%.4X\t\t", *data );

  if ( read_comment() != 0)
    return 1;
  
  return 0;
}

u8 read_dec ( u8 *data )
{
  int symbol;

  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fscanf ( fd, "%d", &symbol ) != 1 )
  {
    fprintf ( stderr, "Read error\n" );
    return 1;
  }
  
  *data = (u8) symbol;
  fprintf ( stdout, "%d\t\t", *data );

  if ( read_comment() != 0)
    return 1;
  
  return 0;
}

u8 read_long ( u64 *data )
{
  long int symbol;

  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fscanf ( fd, "%ld", &symbol ) != 1 )
  {
    fprintf ( stderr, "Read error\n" );
    return 1;
  }
  
  *data = (u64) symbol;
  fprintf ( stdout, "%lu\t\t", (unsigned long)*data );

  if ( read_comment() != 0)
    return 1;
  
  return 0;
}

u8 read_string ( char *data )
{
  if ( feof ( fd ) != 0 )
  {
    fprintf ( stderr, "End of file reached\n" );
    return 1;
  }

  if ( fscanf ( fd, "%s", data ) != 1 )
  {
    fprintf ( stderr, "Read error\n" );
    return 1;
  }

  fprintf ( stdout, "%s\t", data );

  if ( read_comment() != 0)
    return 1;

  return 0;
}
