@ECHO OFF
@del /F /Q set_tests.log
@del /F /Q errors.log
::SMS-SUBMIT PDU tests routine
FOR /f "tokens=* delims=" %%a in (.\tests.list) do (
ECHO %%a >>set_tests.log
.\pdu_set.exe %%a 1>>set_tests.log 2>>errors.log
)
