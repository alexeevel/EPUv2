/*****************************************************************************/
/*                                                                           */
/* File: probe_auth.c                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: EGTS Test Suite                                           */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Test routines for AUTH service                               */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: send_test_TELEDATA_POS_DATA                                   */
/*            send_test_AUTH_TERM_IDENTITY1                                  */
/*            send_test_AUTH_TERM_IDENTITY2                                  */
/*            send_test_AUTH_TERM_IDENTITY3                                  */
/*            send_test_AUTH_VEHICLE_DATA                                    */
/*            send_test_AUTH_AUTH_PARAMS                                     */
/*            send_test_AUTH_AUTH_PARAMS1                                    */
/*            send_test_AUTH_AUTH_PARAMS2                                    */
/*            send_test_AUTH_TERM_MODULE_DATA                                */
/*            send_test_AUTH_AUTH_INFO                                       */
/*            send_test_AUTH_SERVICE_INFO                                    */
/*            send_test_AUTH_RESULT_CODE                                     */
/*            send_test_AUTH_INIT                                            */
/*                                                                           */
/*****************************************************************************/

#include "egts_config.h"
#include "egts.h"
#include "egts_impl.h"

/******************************************************************************
*
*/

#include "../egts_probe.h"
#include "probe_teledata.h"
//#include "serial/gps.h"

#include <time.h>
#include <stdlib.h>

extern float batValue;


/*****************************************************************************/
/*                                                                           */
/* send_test_TELEDATA_POS_DATA()                                            */
/*                                                                           */
/* Description: Test function for TELEDATA_POS_DATA packet                  */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

static float sz = -4.201;
static unsigned short globalRecordNumber = 4;
#include "usbcomm.h"

static char ig=0;
void send_test_TELEDATA_POS_DATA(
	egts_state_t*   estate ,
	egts_profile_t* pprofile ,
	u8              PR ,
	egts_route_t*   proute ,
	u16             PID,
	egts_responce_header_t* presponce ,
	void*           ptemp_buf ,
	u16             temp_buf_sz  )
{
	egts_record_t               *record = (egts_record_t *)memalloc_func( sizeof(egts_record_t) );
	egts_subrecord_t            *subrecord = (egts_subrecord_t *)memalloc_func( sizeof(egts_subrecord_t) );
	egts_TELEDATA_POS_DATA_t   *sr = (egts_TELEDATA_POS_DATA_t *)memalloc_func( sizeof(egts_TELEDATA_POS_DATA_t) );

	record->record.RL   = 0;  /* calculated automaticaly */
	record->record.RN   = globalRecordNumber++;
	record->record.SSOD = 0;
	record->record.RSOD = 0;
	record->record.GRP  = 0;
	record->record.RPP  = 0;
	record->record.TMFE = 0;   record->record.TM   = 0;
	record->record.EVFE = 0;   record->record.EVID = 0;
	record->record.OBFE = 0;   record->record.OID  = 0;
	record->record.SST  = EGTS_TELEDATA_SERVICE;
	record->record.RST  = EGTS_TELEDATA_SERVICE;
	memset(sr, 0, sizeof(egts_TELEDATA_POS_DATA_t));
#ifdef TEST_MAP
	if (!ig)
		info.rmc.long_float += 0.0005;
	else
		info.rmc.long_float -= 0.0005;
	ig = !ig;

	info.rmc.lat_float -= 0.001;
#endif
	if (1)//(info.rmc.ds == GPS_DATA_VALID)
	{
		//time_t     now = time(0);
		//struct tm  tstruct;
		/*char       buf[80];*/
		/*  tstruct = *localtime(&now);*/
		//tstruct = *gmtime (&now);
		if (info.isEmul)
			sr->NTM	= egts_set_tm(info.gm.tm_sec, info.gm.tm_min, info.gm.tm_hour + 5, info.gm.tm_mday, info.gm.tm_mon, info.gm.tm_year);
		else
			sr->NTM	= egts_set_tm(info.rmc.utc_s, info.rmc.utc_m, info.rmc.utc_h, info.rmc.dt_d, info.rmc.dt_m, (info.rmc.dt_y + 2000));
		//sr->NTM	= egts_set_tm( tstruct.tm_sec, tstruct.tm_min, tstruct.tm_hour, tstruct.tm_mday, (tstruct.tm_mon + 1), (tstruct.tm_year + 1900) );
		//sr->NTM = 0;
		sr->LAT	= (u32)((info.rmc.lat_float / 90) * 0xFFFFFFFF);
		sr->LONG	= (u32)((info.rmc.long_float / 180) * 0xFFFFFFFF);
		sr->VLD	= 1;
		if (info.rmc.lat_dir == GPS_LAT_S)
			sr->LAHS = 1;
		sr->LOHS = 0;
		//if (info.rmc.long_dir == GPS_LONG_E)
		//	sr->LOHS = 1;
		sr->FIX = 1;
		if (sr->SPD > 1)
			sr->MV = 1;
		sr->SPD    = (u32)(info.rmc.speed);
#if 0
		if ((info.alarmInOpto > 0) || (plState.cmd == EPLUMBCMD_ALARM_OPTO) || (plState.cmd == EPLUMBCMD_ALARM_OPTO_BUTTON))
			sr->DIN |= 1;
		if ((info.alarmInBtn > 0) || (plState.cmd == EPLUMBCMD_ALARM_BUTTON) || (plState.cmd == EPLUMBCMD_ALARM_OPTO_BUTTON))
			sr->DIN |= 2;
		if (plState.nowState == EPLUMBSTATE_ARMED)
			sr->DIN |= 4;
		if (plState.nowState == EPLUMBSTATE_ALARMED)
			sr->DIN |= 0xC;
#endif
	}
	else
	{
		sr->VLD	= 0;
	}
#if 0
	if (info.gga.ds == GPS_DATA_VALID)
	{
		sr->ALTE = 1;
		if (info.gga.altitude < 0.0)
			sr->ALTS   = 1;
		sr->ALT[0] = abs(info.gga.altitude) & 0xff;
		sr->ALT[1] = (abs(info.gga.altitude) >> 8) & 0xff;
		sr->ALT[2] = (abs(info.gga.altitude) >> 16) & 0xff;
	}
#else
	sr->ALTE = 1;
	sr->ALTS   = 0;
#if 0
	sr->ALT[0] = abs(plState.transmitOK) & 0xff;
	sr->ALT[1] = (abs(plState.transmitOK) >> 8) & 0xff;
	sr->ALT[2] = (abs(plState.transmitOK) >> 16) & 0xff;
#endif
#endif
#if 0
	sr->ODM[0] = abs(plState.transmitFAILED) & 0xff;
	sr->ODM[1] = (abs(plState.transmitFAILED) >> 8) & 0xff;
	sr->ODM[2] = (abs(plState.transmitFAILED) >> 16) & 0xff;
#endif
/*
	sr->DIRH   = 0;
	sr->DIR    = 0;
	sr->ODM[0] = 0xE0;
	sr->ODM[1] = 0xDE;
	sr->ODM[2] = 0x04;
	sr->DIN    = 0;
	sr->SRC    = 0;
*/
#if 1
  /* subrec.subrecord.SRL - auto */
  subrecord->subrecord.SRT = EGTS_SR_POS_DATA;
  subrecord->SRD           = (void*)sr;
  record->psubrecords = subrecord;
  record->nsubrecords = 1;
#else
  egts_subrecord_t            subrecord2;
  egts_TELEDATA_EXT_POS_DATA_t   sr2;
  memset(&sr2, 0, sizeof(sr2));

  sr2.SFE    = 1;
  /*sr2.NSFE   = 1;*/
  sr2.SAT    = 4;
  /*sr2.NS     = 1;*/

  /* subrec.subrecord.SRL - auto */
  subrecord.subrecord.SRT = EGTS_SR_POS_DATA;
  subrecord.SRD           = (void*)&sr;
  subrecord2.subrecord.SRT = EGTS_SR_EXT_POS_DATA;
  subrecord2.SRD           = (void*)&sr2;
  egts_subrecord_t* subrs[2];
  subrs[0] = &subrecord;
  subrs[1] = &subrecord2;
  record.psubrecords = &subrs;
  record.nsubrecords = 2;

#endif
  probes_ctx.presponce = presponce;
  probes_ctx.precords  = record;
  probes_ctx.nrecords  = 1;
  probes_ctx.last_result = 0;
  probes_ctx.PIDsent = PID;
  egts_probe_printf("TELEDATA.EGTS_SR_POS_DATA (simple) ... ");
  egts_reset_errors( estate );
  egts_tx_packet( estate , pprofile , PR , proute , PID, presponce ,
    record , 1 ,
    ptemp_buf ,temp_buf_sz );
  egts_probe_printf( ( ( estate->rx_error_count > 0 ) ||
                     ( estate->tx_error_count > 0 ) || 
                     ( probes_ctx.last_result != 0 ) ) ? "FAILED\n" : "PASSED\n" );
/*
  probes_ctx.precords = NULL;
  probes_ctx.nrecords = 0;
  probes_ctx.last_result = 0;
*/

}
void send_test_TELEDATA_EXT_POS_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  )
{
	egts_record_t               *record = (egts_record_t *)memalloc_func( sizeof(egts_record_t) );
	egts_subrecord_t            *subrecord = (egts_subrecord_t *)memalloc_func( sizeof(egts_subrecord_t) );
	egts_TELEDATA_EXT_POS_DATA_t   *sr = (egts_TELEDATA_EXT_POS_DATA_t *)memalloc_func( sizeof(egts_TELEDATA_EXT_POS_DATA_t) );

	record->record.RL   = 0;  /* calculated automaticaly */
	record->record.RN   = globalRecordNumber++;
	record->record.SSOD = 0;
	record->record.RSOD = 0;
	record->record.GRP  = 0;
	record->record.RPP  = 0;
	record->record.TMFE = 0;   record->record.TM   = 0;
	record->record.EVFE = 0;   record->record.EVID = 0;
	record->record.OBFE = 0;   record->record.OID  = 0;
	record->record.SST  = EGTS_TELEDATA_SERVICE;
	record->record.RST  = EGTS_TELEDATA_SERVICE;
	memset(sr, 0, sizeof(egts_TELEDATA_EXT_POS_DATA_t));

	sr->SFE    = 1;
	sr->NSFE   = 1;
	sr->SAT    = 4;
	sr->NS     = 1;

	/* subrec.subrecord.SRL - auto */
	subrecord->subrecord.SRT = EGTS_SR_EXT_POS_DATA;
	subrecord->SRD           = (void*)sr;
	record->psubrecords = subrecord;
	record->nsubrecords = 1;

	probes_ctx.presponce = presponce;
	probes_ctx.precords  = record;
	probes_ctx.nrecords  = 1;
	probes_ctx.last_result = 0;
	probes_ctx.PIDsent = PID;
	egts_probe_printf("TELEDATA.EGTS_SR_EXT_POS_DATA (simple) ... ");
	egts_reset_errors( estate );
	egts_tx_packet( estate , pprofile , PR , proute , PID, presponce ,
			record , 1 ,
			ptemp_buf ,temp_buf_sz );
	egts_probe_printf( ( ( estate->rx_error_count > 0 ) ||
	                     ( estate->tx_error_count > 0 ) ||
	                     ( probes_ctx.last_result != 0 ) ) ? "FAILED\n" : "PASSED\n" );

/*
  probes_ctx.precords = NULL;
  probes_ctx.nrecords = 0;
  probes_ctx.last_result = 0;
*/

}

void send_test_TELEDATA_SR_STATE_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  )
{
	egts_record_t               *record = (egts_record_t *)memalloc_func( sizeof(egts_record_t) );
	egts_subrecord_t            *subrecord = (egts_subrecord_t *)memalloc_func( sizeof(egts_subrecord_t) );
	egts_TELEDATA_SR_STATE_DATA_t   *sr = (egts_TELEDATA_SR_STATE_DATA_t *)memalloc_func( sizeof(egts_TELEDATA_SR_STATE_DATA_t) );

	record->record.RL   = 0;  /* calculated automaticaly */
	record->record.RN   = globalRecordNumber++;
	record->record.SSOD = 0;
	record->record.RSOD = 0;
	record->record.GRP  = 0;
	record->record.RPP  = 0;
	record->record.TMFE = 0;   record->record.TM   = 0;
	record->record.EVFE = 0;   record->record.EVID = 0;
	record->record.OBFE = 0;   record->record.OID  = 0;
	record->record.SST  = EGTS_TELEDATA_SERVICE;
	record->record.RST  = EGTS_TELEDATA_SERVICE;
	memset(sr, 0, sizeof(egts_TELEDATA_SR_STATE_DATA_t));
#if 0
#define EGTS_MODE_IDLE            0U
#define EGTS_MODE_ERA             1U
#define EGTS_MODE_ACIVE           2U
#define EGTS_MODE_EMERGENCY_CALL  3U
#define EGTS_MODE_EMERGENCY_MON   4U
#define EGTS_MODE_TEST            5U
#define EGTS_MODE_SERVICE         6U
#define EGTS_MODE_FIRMWARE_LOAD   7U

	u8   ST;     /* State */
	  u8   MPSV;   /* Main Power Source Voltage, in 0.1V */
	  u8   BBV;    /* Back Up Battery Voltage, in 0.1V */
	  u8   IBV;    /* Internal Battery Voltage, in 0.1V */

	  /*spare :5 */
	  u8   NMS;  /*:1 Navigation Module State */
	  u8   IBU;  /*:1 Internal Battery Used */
	  u8   BBU;  /*:1 Back Up Battery Used */
#endif
	sr->ST		= EGTS_MODE_ACIVE;
	sr->IBV		= batValue / 0.1;
	sr->IBU		= 1;
	sr->MPSV	= batValue / 0.1;
	sr->NMS		= EGTS_MODE_ACIVE;
	sr->BBV		= 0;
	sr->BBU		= 0;

	/* subrec.subrecord.SRL - auto */
	subrecord->subrecord.SRT = EGTS_SR_STATE_DATA;
	subrecord->SRD           = (void*)sr;
	record->psubrecords = subrecord;
	record->nsubrecords = 1;

	probes_ctx.presponce = presponce;
	probes_ctx.precords  = record;
	probes_ctx.nrecords  = 1;
	probes_ctx.last_result = 0;
	probes_ctx.PIDsent = PID;
	egts_probe_printf("TELEDATA.EGTS_SR_STATE_DATA (simple) ... ");
	egts_reset_errors( estate );
	egts_tx_packet( estate , pprofile , PR , proute , PID, presponce ,
			record , 1 ,
			ptemp_buf ,temp_buf_sz );
	egts_probe_printf( ( ( estate->rx_error_count > 0 ) ||
	                     ( estate->tx_error_count > 0 ) ||
	                     ( probes_ctx.last_result != 0 ) ) ? "FAILED\n" : "PASSED\n" );

/*
  probes_ctx.precords = NULL;
  probes_ctx.nrecords = 0;
  probes_ctx.last_result = 0;
*/

}

void send_test_TELEDATA_SR_AD_SENSORS_DATA_t(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  )
{
	egts_record_t               *record = (egts_record_t *)memalloc_func( sizeof(egts_record_t) );
	egts_subrecord_t            *subrecord = (egts_subrecord_t *)memalloc_func( sizeof(egts_subrecord_t) );
	egts_TELEDATA_SR_AD_SENSORS_DATA_t   *sr = (egts_TELEDATA_SR_AD_SENSORS_DATA_t *)memalloc_func( sizeof(egts_TELEDATA_SR_AD_SENSORS_DATA_t) );

	record->record.RL   = 0;  /* calculated automaticaly */
	record->record.RN   = globalRecordNumber++;
	record->record.SSOD = 0;
	record->record.RSOD = 0;
	record->record.GRP  = 0;
	record->record.RPP  = 0;
	record->record.TMFE = 0;   record->record.TM   = 0;
	record->record.EVFE = 0;   record->record.EVID = 0;
	record->record.OBFE = 0;   record->record.OID  = 0;
	record->record.SST  = EGTS_TELEDATA_SERVICE;
	record->record.RST  = EGTS_TELEDATA_SERVICE;
	memset(sr, 0, sizeof(egts_TELEDATA_SR_AD_SENSORS_DATA_t));

	sr->DIOE		= 1;
	sr->DOUT		= 0;
	if (info.alarmInOpto > 0)
		sr->ADIO[0] |= 1;
	if (info.alarmInBtn > 0)
		sr->ADIO[0] |= 2;
	sr->ASFE		= 1;
	//sr->ANS[0][0]	= plState.nowState;

	/* subrec.subrecord.SRL - auto */
	subrecord->subrecord.SRT = EGTS_SR_AD_SENSORS_DATA;
	subrecord->SRD           = (void*)sr;
	record->psubrecords = subrecord;
	record->nsubrecords = 1;

	probes_ctx.presponce = presponce;
	probes_ctx.precords  = record;
	probes_ctx.nrecords  = 1;
	probes_ctx.last_result = 0;
	probes_ctx.PIDsent = PID;
	egts_probe_printf("TELEDATA.EGTS_SR_AD_SENSORS_DATA (simple) ... ");
	egts_reset_errors( estate );
	egts_tx_packet( estate , pprofile , PR , proute , PID, presponce ,
			record , 1 ,
			ptemp_buf ,temp_buf_sz );
	egts_probe_printf( ( ( estate->rx_error_count > 0 ) ||
	                     ( estate->tx_error_count > 0 ) ||
	                     ( probes_ctx.last_result != 0 ) ) ? "FAILED\n" : "PASSED\n" );

/*
  probes_ctx.precords = NULL;
  probes_ctx.nrecords = 0;
  probes_ctx.last_result = 0;
*/

}

/******************************************************************************
*
*/
