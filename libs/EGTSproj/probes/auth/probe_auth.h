/*****************************************************************************/
/*                                                                           */
/* File: probe_auth.h                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: EGTS Test Suite                                           */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Test routines for AUTH service                               */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: send_test_AUTH_TERM_IDENTITY                                   */
/*            send_test_AUTH_TERM_IDENTITY1                                  */
/*            send_test_AUTH_TERM_IDENTITY2                                  */
/*            send_test_AUTH_TERM_IDENTITY3                                  */
/*            send_test_AUTH_VEHICLE_DATA                                    */
/*            send_test_AUTH_AUTH_PARAMS                                     */
/*            send_test_AUTH_AUTH_PARAMS1                                    */
/*            send_test_AUTH_AUTH_PARAMS2                                    */
/*            send_test_AUTH_TERM_MODULE_DATA                                */
/*            send_test_AUTH_AUTH_INFO                                       */
/*            send_test_AUTH_SERVICE_INFO                                    */
/*            send_test_AUTH_RESULT_CODE                                     */
/*            send_test_AUTH_INIT                                            */
/*                                                                           */
/*****************************************************************************/

#ifndef probe_auth_h
#define probe_auth_h

/******************************************************************************
*
*/

#ifdef  __cplusplus
extern "C"
{
#endif

extern void send_test_AUTH_SR_RESPONSE(
		  egts_state_t*   estate ,
		  egts_profile_t* pprofile ,
		  u8              PR ,
		  egts_route_t*   proute ,
		  u16             PID,
		  egts_responce_header_t* presponce ,
		  void*           ptemp_buf ,
		  u16             temp_buf_sz
		  );


/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_TERM_IDENTITY()                                            */
/*                                                                           */
/* Description: Test function for AUTH_TERM_IDENTITY packet                  */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_TERM_IDENTITY(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_TERM_IDENTITY()                                            */
/*                                                                           */
/* Description: Test function for AUTH_TERM_IDENTITY packet                  */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_TERM_IDENTITY1(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_TERM_IDENTITY2()                                           */
/*                                                                           */
/* Description: Test function for AUTH_TERM_IDENTITY packet                  */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_TERM_IDENTITY2(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_TERM_IDENTITY3()                                           */
/*                                                                           */
/* Description: Test function for AUTH_TERM_IDENTITY packet                  */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_TERM_IDENTITY3(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_VEHICLE_DATA()                                             */
/*                                                                           */
/* Description: Test function for AUTH_VEHICLE_DATA packet                   */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_VEHICLE_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_AUTH_PARAMS()                                              */
/*                                                                           */
/* Description: Test function for AUTH_AUTH_PARAMS packet                    */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_AUTH_PARAMS(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_AUTH_PARAMS1()                                             */
/*                                                                           */
/* Description: Test function for AUTH_AUTH_PARAMS packet                    */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_AUTH_PARAMS1(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_AUTH_PARAMS2()                                             */
/*                                                                           */
/* Description: Test function for AUTH_AUTH_PARAMS packet                    */ 
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_AUTH_PARAMS2(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_TERM_MODULE_DATA()                                         */
/*                                                                           */
/* Description: Test function for AUTH_TERM_MODULE_DATA packet               */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_TERM_MODULE_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_AUTH_INFO()                                                */
/*                                                                           */
/* Description: Test function for AUTH_AUTH_INFO packet                      */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_AUTH_INFO(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_SERVICE_INFO()                                             */
/*                                                                           */
/* Description: Test function for AUTH_SERVICE_INFO packet                   */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_SERVICE_INFO(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_RESULT_CODE()                                              */
/*                                                                           */
/* Description: Test function for AUTH_RESULT_CODE packet                    */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_RESULT_CODE(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_AUTH_INIT()                                                     */
/*                                                                           */
/* Description: Test function for AUTH_INIT packet                           */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_AUTH_INIT(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/******************************************************************************
*
*/

/*****************************************************************************/
/*                                                                           */
/* send_test_TELEDATA_POS_DATA()                                           */
/*                                                                           */
/* Description: Test function for TELEDATA_POS_DATA packet                  */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_TELEDATA_POS_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

/*****************************************************************************/
/*                                                                           */
/* send_test_TELEDATA_EXT_POS_DATA()                                           */
/*                                                                           */
/* Description: Test function for TELEDATA_EXT_POS_DATA packet                  */
/*                                                                           */
/* Arguments: estate - protocol instance                                     */
/*            pprofile - transmit settings                                   */
/*            PR - packet priority                                           */
/*            proute - packet routing settings                               */
/*            PID - packet identifier                                        */
/*            presponce - optional response data                             */
/*            ptemp_buf - temporary buffer 64KB for operation                */
/*            temp_buf_sz - temporary buffer size                            */
/*                                                                           */
/* Return:    nothing                                                        */
/*                                                                           */
/* Other:     Not a part of mandatory code. For debug only.                  */
/*                                                                           */
/*****************************************************************************/

extern void send_test_TELEDATA_EXT_POS_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

extern void send_test_TELEDATA_SR_STATE_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  );

extern void send_test_TELEDATA_SR_AD_SENSORS_DATA_t(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  );


#ifdef  __cplusplus
}
#endif

/******************************************************************************
*
*/

#endif


