/*****************************************************************************/
/*                                                                           */
/* File: probe_teledata.h                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: EGTS Test Suite                                           */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Test routines for AUTH service                               */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions: send_test_AUTH_TERM_IDENTITY                                   */
/*            send_test_AUTH_TERM_IDENTITY1                                  */
/*            send_test_AUTH_TERM_IDENTITY2                                  */
/*            send_test_AUTH_TERM_IDENTITY3                                  */
/*            send_test_AUTH_VEHICLE_DATA                                    */
/*            send_test_AUTH_AUTH_PARAMS                                     */
/*            send_test_AUTH_AUTH_PARAMS1                                    */
/*            send_test_AUTH_AUTH_PARAMS2                                    */
/*            send_test_AUTH_TERM_MODULE_DATA                                */
/*            send_test_AUTH_AUTH_INFO                                       */
/*            send_test_AUTH_SERVICE_INFO                                    */
/*            send_test_AUTH_RESULT_CODE                                     */
/*            send_test_AUTH_INIT                                            */
/*                                                                           */
/*****************************************************************************/

#ifndef probe_auth_h
#define probe_auth_h

/******************************************************************************
*
*/

#ifdef  __cplusplus
extern "C"
{
#endif

void send_test_TELEDATA_POS_DATA(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR ,
  egts_route_t*   proute ,
  u16             PID,
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz  );


#ifdef  __cplusplus
}
#endif

/******************************************************************************
*
*/

#endif


