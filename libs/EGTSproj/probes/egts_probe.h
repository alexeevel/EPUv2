/*****************************************************************************/
/*                                                                           */
/* File: egts_probe.h                                                        */
/*                                                                           */
/* System: ERA GLONASS terminal communication protocol reference impl.       */
/*                                                                           */
/* Component Name: Tests                                                     */
/*                                                                           */
/* Status: Version 1.1                                                       */
/*                                                                           */
/* Language: C                                                               */
/*                                                                           */
/* (c) Copyright JSC �Navigation-information systems�, 2011                  */
/*                                                                           */
/* Address:                                                                  */
/*     24, Mishina Str., bld.1                                               */
/*     Moscow, Russia                                                        */
/*                                                                           */
/* Description: Misceleniouse declarations for autotests. It is not a part   */
/*   for mandatory EGTS sources.                                             */
/*                                                                           */
/* Additional information: -                                                 */
/*                                                                           */
/* Functions:                                                                */
/*                                                                           */
/*****************************************************************************/

#ifndef egts_probe_h
#define egts_probe_h

/******************************************************************************
*
*/

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
*
*/

typedef void (*tf_probe_send)(
  egts_state_t*   estate ,
  egts_profile_t* pprofile ,
  u8              PR , 
  egts_route_t*   proute ,
  u16             PID, 
  egts_responce_header_t* presponce ,
  void*           ptemp_buf ,
  u16             temp_buf_sz
  );

typedef struct
{
  egts_responce_header_t*   presponce;
  egts_record_t*            precords;
  u16                       nrecords;
  int                       nerr;
  int                       last_result;
  u16                       PIDsent;
} egts_probe_ctx_t;

/*****************************************************************************/
/*                                                                           */
/* estate_rx_packet_probe()                                                  */
/*                                                                           */
/* Description: Probe analizer. For special probes only.                     */
/*                                                                           */
/* Arguments: ctx - call context                                             */
/*            pheader - transport header                                     */
/*            signed_up - signature mark, 1/0                                */
/*            presponce - responce data                                      */
/*            precords - array of records                                    */
/*            nrecords - record count                                        */
/*            FDL - frame data length                                        */
/*                                                                           */
/* Return:    zero on success, nonzero on error                              */
/*                                                                           */
/* Other:                                                                    */
/*                                                                           */
/*****************************************************************************/

extern int  estate_rx_packet_probe( void* ctx , 
  egts_header_t*            pheader , 
  u8                        signed_up ,
  egts_responce_header_t*   presponce ,
  egts_record_t*            precords ,
  u16                       nrecords ,
  u16                       FDL );

extern egts_probe_ctx_t       probes_ctx;

extern const tf_probe_send  egts_probes_vector[];

#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
#else
#define GPRS_NOT_VALID_COORDS 30

/* Following are used for GPS data status */
typedef enum
{
      GPS_DATA_INVALID = 'V',
      GPS_DATA_VALID = 'A'
} GPS_DATA_STATUS;

typedef enum
{
	GPS_LAT_N = 'N',
	GPS_LAT_S = 'S'
} GPS_LAT_DIRECTION;

typedef enum
{
	GPS_LONG_W = 'W',
	GPS_LONG_E = 'E'
} GPS_LONG_DIRECTION;

/* RMC packet structure */
typedef struct
{
	int utc_h;
	int utc_m;
	int utc_s;
	int dt_d;
	int dt_m;
	int dt_y;
	char dt[7];                            /* date */
	GPS_DATA_STATUS ds;                    /* status */
	char lat[10];                          /* latitude */
	float lat_float;
	char lg[11];                           /* longitude */
	float long_float;
	unsigned int speed;
	GPS_LAT_DIRECTION lat_dir;
	GPS_LONG_DIRECTION long_dir;
} GPS_RMC;


/* GGA packet structure */
typedef struct
{
      char sat;                              /* satillites used */
      char alt[10];                           /* altitude */
      unsigned int altitude;                           /* altitude */
      GPS_DATA_STATUS ds;                    /* status */

} GPS_GGA;



/* INFO structure */
typedef struct
{
    GPS_RMC rmc;                     /* RMC data */
    GPS_GGA gga;                     /* GGA data */

} GPS_INFO;

extern GPS_INFO info;

#endif
/*****************************************************************************/
/*                                                                           */
/* egts_probe_printf()                                                       */
/*                                                                           */
/* Description: Probe debug callback. For special probes only.               */
/*                                                                           */
/* Arguments: fmt - format specifier                                         */
/*            ... - some arguments                                           */
/*                                                                           */
/* Return:    zero on success, nonzero on error                              */
/*                                                                           */
/* Other:                                                                    */
/*                                                                           */
/*****************************************************************************/

extern void egts_probe_printf( const char* fmt , ... );


/******************************************************************************
*
*/

#ifdef  __cplusplus
}
#endif

/*****************************************************************************
*
*/

#endif



