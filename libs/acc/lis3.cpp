/*
 * lis3ch.c
 *
 *  Created on: Apr 22, 2021
 *      Author: ironcaterpillar
 */

#include <acc/lis3.hpp>
#include "main.h"
#include "string.h"
#include "main.h"
#include "cmsis_os2.h"
#include "debug/debug.hpp"
using namespace debug;

bool LIS3::test()
{

	uint32_t info1 = ReadReg(INFO1,1);
	if(info1==INFO1_DEF_VAL)
		{
		DBG::PRINT(LVL::INFO, "LIS3SH ACC", "" , "TEST OK");
		return true;}
	else
	{
		DBG::PRINT(LVL::EMCY, "LIS3SH ACC", "" , "TEST FAILED ", "INFO = %d", info1);
		return false;
	}
}
 LIS3::LIS3(SPI_HandleTypeDef *spi, GPIO_TypeDef *port,	uint16_t pin):spi(spi), cs_port(port), cs_pin(pin) {

	HAL_GPIO_WritePin(cs_port, cs_pin, GPIO_PIN_SET);

	WriteReg(CTRL_REG4, 0x5F, 1);
	// 200 Hz antialias filter, +/- 2g FS range
	WriteReg(CTRL_REG5, 0x80, 1);
	// configure FIFO for bypass mode
	WriteReg(FIFO_CTRL_REG, 0, 1);
	// disable FIFO, enable register address auto-increment
	WriteReg(CTRL_REG6, 0x10, 1);
}

uint32_t LIS3::ReadReg(uint32_t reg, uint8_t len) {
	uint32_t data = 0;
	uint8_t tx[] = { form_request_byte(1, reg), 0, 0, 0 };
	HAL_GPIO_WritePin(cs_port, cs_pin, GPIO_PIN_RESET);
	osDelay(1);
	HAL_SPI_TransmitReceive(spi, tx, (uint8_t*) &data, len + 1, 1000);
	HAL_GPIO_WritePin(cs_port, cs_pin, GPIO_PIN_SET);
	return data >> 8;
}

void LIS3::WriteReg(uint32_t reg, uint32_t val, uint8_t len) {

	uint8_t tx[] = { form_request_byte(0, reg), 0, 0, 0 };
	memcpy(tx + 1, (uint8_t*)&val, len);
	HAL_GPIO_WritePin(cs_port, cs_pin, GPIO_PIN_RESET);
	osDelay(1);
	HAL_SPI_Transmit(spi, tx, len + 1, 1000);
	HAL_GPIO_WritePin(cs_port, cs_pin, GPIO_PIN_SET);


}
void LIS3::SetupWakeup(uint8_t threshold)
{
	WriteReg(CTRL_REG1, 0x01, 1);
	WriteReg(CTRL_REG3, 0b1101000, 1);
	WriteReg(CTRL_REG4, 0x67, 1);
	WriteReg(CTRL_REG5, 0x00, 1);
	WriteReg(THRS1_1, threshold, 1);
	WriteReg(ST1_1, 0x05, 1);
	WriteReg(ST1_2, 0x11, 1);
	WriteReg(MASK1_B, 0xFC, 1);
	WriteReg(MASK1_A, 0xFC, 1);
	WriteReg(SETT1, 0x1, 1);

}
