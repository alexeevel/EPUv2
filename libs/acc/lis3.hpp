/*
 * lis3.h
 *
 *  Created on: Apr 22, 2021
 *      Author: ironcaterpillar
 */

#ifndef ACC_LIS3_HPP_
#define ACC_LIS3_HPP_
#include "stdint.h"
#include "stm32l4xx_hal.h"
#include "hwtesting/hwtesting.hpp"


class LIS3: public AbstractHWTest {
	public:
	static constexpr auto INFO1 = 0x0D;
	static constexpr auto INFO2 = 0x0E;
	static constexpr auto OUT_T = 0x0C;
	static constexpr auto WHO_AM_I = 0xF;

	static constexpr auto CTRL_REG1 = 0x21;
	static constexpr auto CTRL_REG3 = 0x23;
	static constexpr auto CTRL_REG4 = 0x20;
	static constexpr auto CTRL_REG5 = 0x24;
	static constexpr auto CTRL_REG6 = 0x25;
	static constexpr auto THRS1_1 = 0x57;
	static constexpr auto ST1_1 = 0x40;
	static constexpr auto ST1_2 = 0x41;
	static constexpr auto MASK1_B = 0x59;
	static constexpr auto MASK1_A = 0x5A;
	static constexpr auto SETT1 = 0x5B;
	static constexpr auto FIFO_CTRL_REG = 0x2E;
	static constexpr auto TIM4_1 = 0x50;

	static constexpr auto INFO1_DEF_VAL = 33;

	bool test()override;



	LIS3(SPI_HandleTypeDef *spi, GPIO_TypeDef *port, uint16_t cs_pin);
	uint32_t ReadReg(uint32_t reg, uint8_t len);
	void WriteReg(uint32_t reg, uint32_t val, uint8_t len);
	void SetupWakeup(uint8_t threshold);

private:
	SPI_HandleTypeDef *spi;
		GPIO_TypeDef *cs_port;
		uint16_t cs_pin;

		 uint8_t form_request_byte(uint8_t read, uint8_t address) {
			return ((read ? 1 << 7 : 0) | (address & 0b1111111));
		}
};


#endif /* ACC_LIS3_HPP_ */
