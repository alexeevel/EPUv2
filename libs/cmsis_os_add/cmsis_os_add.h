/*
 * cmsis_os_add.h
 *
 *  Created on: Jun 14, 2021
 *      Author: ironcaterpillar
 */

#ifndef CMSIS_OS_ADD_CMSIS_OS_ADD_H_
#define CMSIS_OS_ADD_CMSIS_OS_ADD_H_
#include "cmsis_os2.h"

typedef void* osMessageStreamId_t;

typedef struct
{
	size_t size;
}osMessageStreamDef_t;

osMessageStreamDef(name, size)\
		extern const osMessageStreamDef_t osMessageBuffer_Def_##name = { .size=size };




#endif /* CMSIS_OS_ADD_CMSIS_OS_ADD_H_ */
