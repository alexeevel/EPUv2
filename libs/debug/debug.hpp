/*
 * debug.hpp
 *
 *  Created on: Jun 4, 2021
 *      Author: ironcaterpillar
 */

#ifndef DEBUG_DEBUG_HPP_
#define DEBUG_DEBUG_HPP_
#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_rtc.h"
#include "RTT/SEGGER_RTT.h"

namespace debug{
enum class LVL {
	NONE, EMCY, WARN, INFO, DBG, VERBOSE,
};
class DBG {

	static LVL level;

public:
	static void SETLEVEL(LVL lvl) {
		level = lvl;
	}

	static const char* get_level_str(LVL lev)
	{
		switch(lev)
		{
		case LVL::EMCY: return "EMCY";break;
		case LVL::WARN: return "WARN";break;
		case LVL::INFO: return "INFO";break;
		case LVL::DBG: return "DBG";break;
		case LVL::VERBOSE: return "VERBOSE";break;
		default: return "UNKNOWN";
		}
	}
	template<typename ... Params>
	static void PRINT(LVL lvl, const char *block_name, const char *subblock, const char *message,
			Params... msgs) {
		if (lvl > level)
			return;
		constexpr auto param_count = sizeof...(Params);
		uint32_t subsec = LL_RTC_TIME_GetSubSecond(RTC);
		uint32_t time = LL_RTC_TIME_Get(RTC);
		uint32_t date = LL_RTC_DATE_Get(RTC);

		SEGGER_RTT_printf(0, "%s %X\t%03X\t%03d\t%s\t%s\t%s\t", get_level_str(lvl), date, time,subsec, block_name,
				subblock, message);
		if constexpr (param_count)
			SEGGER_RTT_printf(0, msgs...);
		SEGGER_RTT_PutChar(0, '\n');
	}

};
}
#endif /* DEBUG_DEBUG_HPP_ */
