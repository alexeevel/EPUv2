/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

#include "stm32l4xx_ll_rtc.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_cortex.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_dma.h"

#include "stm32l4xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "RTT/SEGGER_RTT.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GNSS_FORCE_ON_Pin GPIO_PIN_2
#define GNSS_FORCE_ON_GPIO_Port GPIOE
#define GSM_PWR_ON_Pin GPIO_PIN_3
#define GSM_PWR_ON_GPIO_Port GPIOE
#define SIM_SEL_Pin GPIO_PIN_4
#define SIM_SEL_GPIO_Port GPIOE
#define SIM_DET_Pin GPIO_PIN_5
#define SIM_DET_GPIO_Port GPIOE
#define LED1_Pin GPIO_PIN_0
#define LED1_GPIO_Port GPIOF
#define LED2_Pin GPIO_PIN_1
#define LED2_GPIO_Port GPIOF
#define LED3_Pin GPIO_PIN_2
#define LED3_GPIO_Port GPIOF
#define LED4_Pin GPIO_PIN_3
#define LED4_GPIO_Port GPIOF
#define LED5_Pin GPIO_PIN_4
#define LED5_GPIO_Port GPIOF
#define LED6_Pin GPIO_PIN_5
#define LED6_GPIO_Port GPIOF
#define V33_EN_Pin GPIO_PIN_6
#define V33_EN_GPIO_Port GPIOF
#define V40_EN_Pin GPIO_PIN_7
#define V40_EN_GPIO_Port GPIOF
#define V50_EN_Pin GPIO_PIN_8
#define V50_EN_GPIO_Port GPIOF
#define V33_OK_Pin GPIO_PIN_9
#define V33_OK_GPIO_Port GPIOF
#define V40_OK_Pin GPIO_PIN_10
#define V40_OK_GPIO_Port GPIOF
#define VBAT_MON_Pin GPIO_PIN_2
#define VBAT_MON_GPIO_Port GPIOC
#define VBAT_REF_ON_Pin GPIO_PIN_3
#define VBAT_REF_ON_GPIO_Port GPIOC
#define I2C3_PULLUP_Pin GPIO_PIN_3
#define I2C3_PULLUP_GPIO_Port GPIOA
#define RFID_CS_Pin GPIO_PIN_4
#define RFID_CS_GPIO_Port GPIOA
#define RFID_CLK_Pin GPIO_PIN_5
#define RFID_CLK_GPIO_Port GPIOA
#define RFID_MISO_Pin GPIO_PIN_6
#define RFID_MISO_GPIO_Port GPIOA
#define RFID_MOSI_Pin GPIO_PIN_7
#define RFID_MOSI_GPIO_Port GPIOA
#define ACC_INT_Pin GPIO_PIN_5
#define ACC_INT_GPIO_Port GPIOC
#define ACC_INT_EXTI_IRQn EXTI9_5_IRQn
#define RFID_EN0_Pin GPIO_PIN_0
#define RFID_EN0_GPIO_Port GPIOB
#define RFID_EN2_Pin GPIO_PIN_1
#define RFID_EN2_GPIO_Port GPIOB
#define VBAT_DIV_EN_Pin GPIO_PIN_2
#define VBAT_DIV_EN_GPIO_Port GPIOB
#define V50_OK_Pin GPIO_PIN_11
#define V50_OK_GPIO_Port GPIOF
#define FL_RESET_Pin GPIO_PIN_9
#define FL_RESET_GPIO_Port GPIOE
#define ACC_CS_Pin GPIO_PIN_12
#define ACC_CS_GPIO_Port GPIOB
#define SCM_PWR_ON_Pin GPIO_PIN_2
#define SCM_PWR_ON_GPIO_Port GPIOG
#define SCM_OFF_Pin GPIO_PIN_3
#define SCM_OFF_GPIO_Port GPIOG
#define TIM_RST_Pin GPIO_PIN_6
#define TIM_RST_GPIO_Port GPIOC
#define OPTO_TX_Pin GPIO_PIN_7
#define OPTO_TX_GPIO_Port GPIOC
#define BTN_RX_Pin GPIO_PIN_9
#define BTN_RX_GPIO_Port GPIOC
#define BTN_RX_EXTI_IRQn EXTI9_5_IRQn
#define ESP32_IO0_Pin GPIO_PIN_1
#define ESP32_IO0_GPIO_Port GPIOD
#define GNSS_TX_Pin GPIO_PIN_6
#define GNSS_TX_GPIO_Port GPIOB
#define GNSS_RX_Pin GPIO_PIN_7
#define GNSS_RX_GPIO_Port GPIOB
#define GNSS_STNDBY_Pin GPIO_PIN_0
#define GNSS_STNDBY_GPIO_Port GPIOE
#define GNSS_RST_Pin GPIO_PIN_1
#define GNSS_RST_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#if (DEBUG==1)
# define DEBUG_PRINTF(...) SEGGER_RTT_printf(0,__VA_ARGS__);SEGGER_RTT_printf(0,"\n")
#else
#define DEBUG_PRINTF(...)
#endif
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
