/*
 * indication.h
 *
 *  Created on: Jun 1, 2021
 *      Author: ironcaterpillar
 */

#ifndef INC_INDICATION_H_
#define INC_INDICATION_H_

enum LEDState
{
	LED_OFF,LED_BLINK,LED_FLICKER,LED_FLASH
};
struct LED
{
	LEDState state;
};
class Indication
{

};



#endif /* INC_INDICATION_H_ */
