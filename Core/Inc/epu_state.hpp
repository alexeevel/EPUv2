/*
 * epu_state.h
 *
 *  Created on: May 31, 2021
 *      Author: ironcaterpillar
 */

#ifndef INC_EPU_STATE_HPP_
#define INC_EPU_STATE_HPP_

#include "cstdint"
#include "stm32l4xx_hal.h"
#include "err_type.hpp"
#include "stateful/stateful_function.h"

enum EPU_GLOBAL_STATES
{
	EPU_STATE_START,
	EPU_STATE_WAREHOUSE=1,
	EPU_WAIT_CLOSE,
	EPU_STATE_CLOSED,
	EPU_WAIT_OPEN,
	EPU_STATE_ALARM
};

struct epu_alarms {
	union {
		struct {
			bool wire:1;
			bool button:1;
			bool accel:1;
			bool temp:1;
		};
		uint32_t int_alarms;
	};
	void reset(){int_alarms=0;}
	bool access_alarm(){return wire|button;}
};

struct epu_hw_flags {
	union
	{ struct
		{
			bool nfc_field:1;
		};};
};


struct EPUSettings
{
	uint32_t wait_for_closing_ms=2*1000*60;
	uint32_t wait_for_opening_ms=2*1000*60;
};

struct GPSState
{
	float altitude;
	float longitude;
	float heigth;
	float speed_kph;
};

struct EPUState
{

		EPU_GLOBAL_STATES global_state;
		epu_alarms alarms;
		epu_hw_flags hw_flags;
		GPSState gps_state;
		void reset_time(){last_time_ms=0;time_passed_ms=0;}
		uint32_t last_time_ms;
		uint32_t time_passed_ms;
		void calc_time_passed()
		{
			if(this->time_passed_ms==0)
			{
				this->last_time_ms=1000*HAL_GetTick()/HAL_GetTickFreq();
			}
			else
			{
				uint32_t ms_local = 1000*HAL_GetTick()/HAL_GetTickFreq();
				this->time_passed_ms+=ms_local-this->last_time_ms;
				this->last_time_ms=ms_local;
			}
		}
};
class DummySender
{
	public:
	static void send_info(EPUState &state){}
};
template<class SENDER>
struct EPUStateFSM
{
	EPUSettings settings;
	EPUState state;


ERROR_CODE epu_fsm()
{

	STATEFUL_START(this->state.global_state)

	STATEFUL_END_STATE

	STATEFUL_STATE(EPU_STATE_WAREHOUSE)
	if(this->state.hw_flags.nfc_field){
		this->state.hw_flags.nfc_field=false;
		this->state.reset_time();
		SENDER::send_info(state);
		STATEFUL_MOVETO(EPU_WAIT_CLOSE, ERROR_BUSY)
	}
	else STATEFUL_MOVETO(EPU_STATE_WAREHOUSE, ERROR_BUSY)
	STATEFUL_END_STATE

	STATEFUL_STATE(EPU_WAIT_CLOSE)
	state.calc_time_passed();
	if(state.time_passed_ms>settings.wait_for_closing_ms&&!state.alarms.access_alarm())
	{state.reset_time();STATEFUL_MOVETO(EPU_STATE_CLOSED,ERROR_BUSY);}
	else if(state.alarms.access_alarm()){state.alarms.reset();state.reset_time();}
	STATEFUL_END_STATE

	STATEFUL_STATE(EPU_STATE_CLOSED)
	SENDER::send_info(state);
	if(state.alarms.access_alarm())
	{
		STATEFUL_MOVETO(EPU_STATE_ALARM, ERROR_BUSY);
	}
	if(this->state.hw_flags.nfc_field){
			this->state.hw_flags.nfc_field=false;
			this->state.reset_time();
			SENDER::send_info(state);
			STATEFUL_MOVETO(EPU_WAIT_OPEN,ERROR_BUSY);}
	STATEFUL_END_STATE

	STATEFUL_STATE(EPU_WAIT_OPEN)
	state.calc_time_passed();
		if(state.time_passed_ms>settings.wait_for_opening_ms&&state.alarms.access_alarm())
		{state.reset_time();STATEFUL_MOVETO(EPU_STATE_WAREHOUSE,ERROR_BUSY);}
		else if(!state.alarms.access_alarm()){state.alarms.reset();state.reset_time();}
	STATEFUL_END_STATE

	STATEFUL_STATE(EPU_STATE_ALARM)
	if(this->state.hw_flags.nfc_field){
		SENDER::send_info(state);
		this->state.hw_flags.nfc_field=false;
		STATEFUL_MOVETO(EPU_STATE_CLOSED,ERROR_BUSY);
	}
	STATEFUL_END_STATE

	STATEFUL_END;

	return ERROR_OK;
}

};
#endif /* INC_EPU_STATE_HPP_ */
