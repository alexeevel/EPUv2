/*
 * usbcomm.h
 *
 *  Created on: 23 июн. 2021 г.
 *      Author: abutko
 */

#ifndef INC_USBCOMM_H_
#define INC_USBCOMM_H_

#include "times.h"

#define ERR_NO_MEMORY -1
/* Following are used for GPS data status */
typedef enum
{
      GPS_DATA_INVALID = 'V',
      GPS_DATA_VALID = 'A'
} GPS_DATA_STATUS;

typedef enum
{
	GPS_LAT_N = 'N',
	GPS_LAT_S = 'S'
} GPS_LAT_DIRECTION;

typedef enum
{
	GPS_LONG_W = 'W',
	GPS_LONG_E = 'E'
} GPS_LONG_DIRECTION;

/* RMC packet structure */
typedef struct
{
	int utc_h;
	int utc_m;
	int utc_s;
	int dt_d;
	int dt_m;
	int dt_y;
	char dt[7];                            /* date */
	GPS_DATA_STATUS ds;                    /* status */
	char lat[10];                          /* latitude */
	float lat_float;
	char lg[11];                           /* longitude */
	float long_float;
	unsigned int speed;
	GPS_LAT_DIRECTION lat_dir;
	GPS_LONG_DIRECTION long_dir;
} GPS_RMC;


/* GGA packet structure */
typedef struct
{
      char sat;                              /* satillites used */
      char alt[10];                           /* altitude */
      unsigned int altitude;                           /* altitude */
      GPS_DATA_STATUS ds;                    /* status */

} GPS_GGA;



/* INFO structure */
typedef struct
{
    GPS_RMC rmc;                     /* RMC data */
    GPS_GGA gga;                     /* GGA data */
    char isEmul;
	unitime gm;
	char alarmInOpto;
	char alarmInBtn;
	char fileCreated;
	char gpsOKdone;
	char taskDone;
} GPS_INFO;

extern GPS_INFO info;




#endif /* INC_USBCOMM_H_ */
