/*
 * times.h
 *
 *  Created on: 14 июн. 2021 г.
 *      Author: abutko
 */

#ifndef INC_TIMES_H_
#define INC_TIMES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "time.h"
typedef struct unitime_
{
  int	tm_sec;
  int	tm_min;
  int	tm_hour;
  int	tm_mday;
  int	tm_mon;
  int	tm_year;
  int	tm_wday;
  int	tm_yday;
} unitime;
void convertTime(time_t tm_in, int timezone_in, unitime *unitm);
void convertTimeBack(unitime *unitm_in, int timezone_in, time_t *tm_out);

#ifdef __cplusplus
}
#endif

#endif /* INC_TIMES_H_ */
