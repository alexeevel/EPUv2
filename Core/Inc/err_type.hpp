/*
 * err_type.hpp
 *
 *  Created on: Jun 1, 2021
 *      Author: ironcaterpillar
 */

#ifndef INC_ERR_TYPE_HPP_
#define INC_ERR_TYPE_HPP_



enum ERROR_CODE
{
	ERROR_OK=0,
	ERROR_BUSY=1,
};


#endif /* INC_ERR_TYPE_HPP_ */
