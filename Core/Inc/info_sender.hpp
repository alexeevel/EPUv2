/*
 * info_sender.hpp
 *
 *  Created on: Jun 1, 2021
 *      Author: ironcaterpillar
 */

#ifndef INC_INFO_SENDER_HPP_
#define INC_INFO_SENDER_HPP_

#include "err_type.h"

class InfoSender
{
public:
	virtual ERROR_CODES send(EPUState& epu_state);
	virtual ~InfoSender()=default;
};


#endif /* INC_INFO_SENDER_HPP_ */
