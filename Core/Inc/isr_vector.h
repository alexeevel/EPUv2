/*
 * isr_vector.h
 *
 *  Created on: 15 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#ifndef INC_ISR_VECTOR_H_
#define INC_ISR_VECTOR_H_


#ifdef __cplusplus
#include <tuple>
template<typename T, typename Ret, int I>
struct UMemberPTRHolder;

template<typename T, typename Ret,typename ... Params, int I>
struct UMemberPTRHolder<T, Ret(Params...), I>
{
	typedef Ret(*cfptr)(Params...) ;
	typedef Ret(T::*method_ptr)(Params...) ;
	static cfptr init(T& tt, method_ptr ptr){t = &tt;mptr = ptr;return run;}
	static T* t;
	static method_ptr mptr;
	static Ret run(Params... params)
	{
		if(mptr&&t)
			return (t->*mptr)(params...);
		else return Ret();
	}

};

template<typename T, typename Ret,typename ... Params, int I>
typename UMemberPTRHolder<T, Ret(Params...), I>::method_ptr UMemberPTRHolder<T, Ret(Params...), I>::mptr=nullptr;

template<typename T, typename Ret,typename ... Params, int I>
T* UMemberPTRHolder<T, Ret(Params...), I>::t=nullptr;


extern "C" {
#endif

typedef void (*isr_vector_t)(void) ;
void enable_ram_isr_vector();
void isr_set_periph_callback(void * periph_address, isr_vector_t callback);

#ifdef __cplusplus
}
#endif
#endif /* INC_ISR_VECTOR_H_ */
