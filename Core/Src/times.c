/*
 * times.c
 *
 *  Created on: 14 июн. 2021 г.
 *      Author: abutko
 */

#include "times.h"

void convertTime(time_t tm_in, int timezone_in, unitime *unitm)
{
	struct tm gm;
	time_t tm = tm_in - 60*60*timezone_in - 60*3;
	gmtime_r(&tm, &gm);
	unitm->tm_sec = gm.tm_sec;
	unitm->tm_min = gm.tm_min;
	unitm->tm_hour = gm.tm_hour;
	unitm->tm_mday = gm.tm_mday;
	unitm->tm_mon = gm.tm_mon + 1;
	unitm->tm_year = gm.tm_year + 1900;
	unitm->tm_wday = gm.tm_wday;
	unitm->tm_yday = gm.tm_yday;
}

void convertTimeBack(unitime *unitm_in, int timezone_in, time_t *tm_out)
{
	struct tm gm;
	gm.tm_sec = unitm_in->tm_sec;
	gm.tm_min = unitm_in->tm_min;
	gm.tm_hour = unitm_in->tm_hour;
	gm.tm_mday = unitm_in->tm_mday;
	gm.tm_mon = unitm_in->tm_mon - 1;
	gm.tm_year = unitm_in->tm_year - 1900;
	gm.tm_wday = unitm_in->tm_wday;
	gm.tm_yday = unitm_in->tm_yday;
	time_t tm = mktime(&gm);
	*tm_out = tm + 60*60*timezone_in + 60*3;
}


