/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <gnss/l76.hpp>
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "epu_state.hpp"
#include "bsp/bsp.hpp"
#include "acc/lis3.hpp"
#include "spi.h"
#include "RTT/SEGGER_RTT.h"
#include <iostream>
#include "flash/s25_test.hpp"
#include "debug/debug.hpp"
#include "rfid/trf7970a.hpp"
#include "thermo/stlm75.hpp"
#include "i2c.h"
#include "gsm/m66_test.hpp"
#include "skzi/skzi.hpp"
#include "EGTSproj/probes/egts.hpp"
#include "usart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
using namespace debug;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
uint32_t defaultTaskBuffer[ 256 * 4 ];
osStaticThreadDef_t defaultTaskControlBlock;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .cb_mem = &defaultTaskControlBlock,
  .cb_size = sizeof(defaultTaskControlBlock),
  .stack_mem = &defaultTaskBuffer[0],
  .stack_size = sizeof(defaultTaskBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

extern "C" void MX_USB_HOST_Init(void);
extern "C" void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 2 */
void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
}
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
extern "C" void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/*
void HAL_Delay(uint32_t Delay)
{
	osDelay(Delay);
}
*/
EGTS egtsObj;
char egts_data_rcv[500];
unsigned int egts_data_len = 0;

static char strBuf[100];

/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_HOST */
  MX_USB_HOST_Init();
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */


	  EPUStateFSM<DummySender> statefsm;
  uint32_t cnt=0;

  S25QFlashTest s25test;
  power_ctrl v33(V33_EN_GPIO_Port, V33_EN_Pin, V33_OK_GPIO_Port,V33_OK_Pin,"V33");
  power_ctrl v40(V40_EN_GPIO_Port, V40_EN_Pin, V40_OK_GPIO_Port,V40_OK_Pin,"V40");
  power_ctrl v50(V50_EN_GPIO_Port, V50_EN_Pin, V50_OK_GPIO_Port,V50_OK_Pin,"V50");
  v33.test();
  v40.test();
  v50.test();

  v40.on();
  v33.on();
  LIS3 lis3(&hspi2,ACC_CS_GPIO_Port,ACC_CS_Pin);
  lis3.test();
  s25test.test();
  v50.on();
while(!v50.is_on())osDelay(1);
  TRF7970 trf(spi_port(&hspi1, RFID_CS_GPIO_Port, RFID_CS_Pin)
		  , gpio_out_pin(RFID_EN0_GPIO_Port, RFID_EN0_Pin, false)
		  , gpio_out_pin(RFID_EN2_GPIO_Port, RFID_EN2_Pin, false));
  trf.test();

  STLM75 stlm75(&hi2c3, gpio_out_pin(I2C3_PULLUP_GPIO_Port, I2C3_PULLUP_Pin, false));
  stlm75.test();

  setDefValue();

  SKZI scm;

  L76 l76(&huart1
		  , gpio_out_pin(GNSS_FORCE_ON_GPIO_Port, GNSS_FORCE_ON_Pin, false)
		  , gpio_out_pin(GNSS_RST_GPIO_Port, GNSS_RST_Pin, false)
		  , gpio_out_pin(GNSS_STNDBY_GPIO_Port, GNSS_STNDBY_Pin, false));

  //isr_set_periph_callback(l76.get_uart()->Instance, UMemberPTRHolder<L76,void(),__COUNTER__>
  //::init(l76, &L76::interrupt_handler));

  HAL_UART_RegisterCallback(l76.get_uart(),HAL_UART_RX_COMPLETE_CB_ID,
		 UMemberPTRHolder<L76,void(UART_HandleTypeDef *),__COUNTER__>
		    ::init(l76, &L76::rx_cplt_callback));

  HAL_UART_RegisterCallback(l76.get_uart(),HAL_UART_ERROR_CB_ID,
  		 UMemberPTRHolder<L76,void(UART_HandleTypeDef *),__COUNTER__>
  		    ::init(l76, &L76::error_callback));
  l76.start();
  l76.test();
  M66Test m66gsm(&v40);
  //m66gsm.test();
  //m66gsm.test();
  //m66gsm.test();
  //m66gsm.test();

  char *sendBuf = NULL;
  unsigned int outSize = 0;
  char *receivedBufPtr = NULL;
  unsigned int receivedBufSize = 0;

  if (scm.initSKZI())
  {
	  if (m66gsm.init())
	  {
		  egtsObj.EGTS_Init();
		  if (scm.connectStage1((char **)&sendBuf, (unsigned int *)&outSize))
		  {
			  snprintf((char *)&strBuf, sizeof(strBuf), "TLS ClientHello [out: %04d] ............................... OK", outSize);
			  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", (char *)&strBuf);
			  if (m66gsm.sendSocket((const char *)sendBuf, (const unsigned int)outSize, (char **)&receivedBufPtr, &receivedBufSize))
			  {
				  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [out: %04d] and receive [in: %04d] ............ OK", outSize, receivedBufSize);
				  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
				  if (scm.connectStage2((const char *)receivedBufPtr, (const unsigned int)receivedBufSize, (char **)&sendBuf, (unsigned int *)&outSize))
				  {
					  snprintf((char *)&strBuf, sizeof(strBuf), "TLS ServerHello [in: %04d] ChangeCipherSpec [out: %04d] ... OK", outSize, receivedBufSize);
					  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", (char *)&strBuf);
					  receivedBufPtr = NULL;
					  receivedBufSize = 0;
					  if (m66gsm.sendSocket((const char *)sendBuf, (const unsigned int)outSize, (char **)&receivedBufPtr, &receivedBufSize))
					  {
						  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [out: %04d] and receive [in: %04d] ............ OK", outSize, receivedBufSize);
						  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
						  if (scm.connectStage3((const char *)receivedBufPtr, (const unsigned int)receivedBufSize))
						  {
							  snprintf((char *)&strBuf, sizeof(strBuf), "TLS Finished [in: %04d] ................................... OK", receivedBufSize);
							  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", (char *)&strBuf);
							  while (egtsObj.nextPacket((char **)&receivedBufPtr, &receivedBufSize))
							  {
								  snprintf((char *)&strBuf, sizeof(strBuf), "EGTS packet ready to send [out: %04d] ..................... OK", receivedBufSize);
								  DBG::PRINT(LVL::EMCY, "MAIN", "" , "EGTS", (char *)&strBuf);
								  if (scm.encrypt((const char *)receivedBufPtr, (const unsigned int)receivedBufSize, (char **)&sendBuf, (unsigned int *)&outSize))
								  {
									  snprintf((char *)&strBuf, sizeof(strBuf), "TLS Encrypt [in: %04d, out: %04d] ......................... OK", receivedBufSize, outSize);
									  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", (char *)&strBuf);
									  receivedBufPtr = NULL;
									  receivedBufSize = 0;
									  if (m66gsm.sendSocket((const char *)sendBuf, (const unsigned int)outSize, (char **)&receivedBufPtr, &receivedBufSize))
									  {
										  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [out: %04d] and receive [in: %04d] ............ OK", outSize, receivedBufSize);
										  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
										  if (scm.decrypt((const char *)receivedBufPtr, (const unsigned int)receivedBufSize, (char *)&egts_data_rcv, sizeof(egts_data_rcv), &egts_data_len))
										  {
											  snprintf((char *)&strBuf, sizeof(strBuf), "TLS Decrypt [in: %04d, out: %04d] ......................... OK", receivedBufSize, egts_data_len);
											  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", (char *)&strBuf);
											  egtsObj.analyzePacket(egts_data_rcv, egts_data_len);
										  }
										  else
										  {
											  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", "TLS Decrypt ............................................... FAILED");
											  continue;
										  }
									  }
									  else
									  {
										  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [state: %s] ............................ FAILED", m66gsm.getStateStr());
										  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
										  continue;
									  }
								  }
								  else
								  {
									  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", "TLS Encrypt ............................................... FAILED");
									  continue;
								  }
							  }
							  DBG::PRINT(LVL::EMCY, "MAIN", "" , "Finish Transfer ................................................. OK\r\n\r\n\r\n", "");
							  scm.disconnect();
							  m66gsm.disconnect();
							  egtsObj.EGTS_Deinit();
						  }
						  else
							  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", "TLS Finished .............................................. FAILED");
					  }
					  else
					  {
						  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [state: %s] ............................ FAILED", m66gsm.getStateStr());
						  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
					  }
				  }
				  else
					  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", "TLS ServerHello ........................................... FAILED");
			  }
			  else
			  {
				  snprintf((char *)&strBuf, sizeof(strBuf), "Socket send [state: %s] ............................ FAILED", m66gsm.getStateStr());
				  DBG::PRINT(LVL::EMCY, "MAIN", "" , "GSM ", (char *)&strBuf);
			  }
		  }
		  else
			  DBG::PRINT(LVL::EMCY, "MAIN", "" , "SCM ", "TLS ClientHello ........................................... FAILED");
	  }
  }
#if 0
  L76 m66(&huart1
		  , gpio_out_pin(GNSS_FORCE_ON_GPIO_Port, GNSS_FORCE_ON_Pin, false)
		  , gpio_out_pin(GNSS_RST_GPIO_Port, GNSS_RST_Pin, false)
		  , gpio_out_pin(GNSS_STNDBY_GPIO_Port, GNSS_STNDBY_Pin, false));
#endif
	  for(;;)
  {

	EPU_BSP::led_toggle(cnt%EPU_BSP::LED_COUNT);
	statefsm.epu_fsm();
    osDelay(111);


    cnt++;
    if (cnt == 100)
		NVIC_SystemReset();
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
