/*
 * new_replacement.cpp
 *
 *  Created on: Jun 14, 2021
 *      Author: ironcaterpillar
 */
#include <new>
#include "FreeRTOS.h"

void* operator new (std::size_t size, const std::nothrow_t& nothrow_value) noexcept
//void* operator new(std::size_t sz) // no inline, required by [replacement.functions]/3
{

    if (void *ptr = pvPortMalloc(size))
        return ptr;


#if __cpp_exceptions
    throw std::bad_alloc{}; // required by [new.delete.single]/3
#else
    return nullptr;
#endif
}
void operator delete(void* ptr) noexcept
{
    vPortFree(ptr);
}


