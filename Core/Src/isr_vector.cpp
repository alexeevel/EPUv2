/*
 * isr_vector.c
 * TODO: Add memory protection here
 *
 *  Created on: 15 июн. 2021 г.
 *      Author: ironcaterpillar
 */

#include "string.h"
#include "isr_vector.h"
#include "cmsis_os2.h"
#include "FreeRTOS.h"
#include "stm32l4xx.h"


void* rom_start = (void*)0x0;
isr_vector_t isr_vector_arr[128] __attribute__((section (".ram_isr_vector")))={0};
extern "C" void enable_ram_isr_vector()
{
	__disable_irq();
	memset(isr_vector_arr, 0, sizeof(isr_vector_arr));
	memcpy(isr_vector_arr, rom_start, sizeof(isr_vector_arr));
	SCB->VTOR = (uint32_t)isr_vector_arr;
	__enable_irq();
}

extern "C" void isr_set_periph_callback(void * periph_address, isr_vector_t callback)
{
	IRQn_Type irq;
	switch((uint32_t)periph_address)
	{
	case (uint32_t)USART1_BASE: irq=USART1_IRQn;break;
	default: return;

	}
	__NVIC_SetVector(irq, (uint32_t)callback);
}




